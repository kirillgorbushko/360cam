//
//  AppHelper.m
//  
//
//  Created by Kirill Gorbushko on 22.06.15.
//
//

#import "AppHelper.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "MBProgressHUD.h"
#import <MessageUI/MessageUI.h>

@implementation AppHelper

#pragma mark - Public

+ (RootViewController *)rootViewControllerSideMenu
{
    UIViewController *viewController = [((AppDelegate *)[UIApplication sharedApplication].delegate).window rootViewController];
    if ([viewController isKindOfClass:[RootViewController class]]) {
        return  (RootViewController *)[((AppDelegate *)[UIApplication sharedApplication].delegate).window rootViewController];
    } else {
        return nil;
    }
}

+ (NSString *)appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey];
}

+ (void)alertViewWithMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    });
}

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:alertViewDelegate cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    });
}

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:[AppHelper appName] message:message delegate:alertViewDelegate cancelButtonTitle:@"OK" otherButtonTitles:otherButtonTitles, nil] show];
    });
}

+ (void)showLoader
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:[AppHelper topView] animated:YES].dimBackground = YES;
    });
}

+ (void)showLoaderWithText:(NSString *)text
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[AppHelper topView]];
        hud.removeFromSuperViewOnHide = YES;
        hud.labelText = text;
        [[AppHelper topView] addSubview:hud];
        [hud show:YES];
    });
}

+ (void)hideLoader
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:[AppHelper topView] animated:YES];
    });
}

+ (BOOL)isIOS_8
{
    return [[UIDevice currentDevice].systemVersion floatValue] >= 8.0;
}

+ (UIView *)topView
{
    return [UIApplication sharedApplication].keyWindow;
}

#pragma mark - Temp method for control tutorialView

+ (void)setTutorialViewedForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isTutorialViewedForKey:(NSString *)key
{
    BOOL isViewed = [[NSUserDefaults standardUserDefaults] boolForKey:key];
    return isViewed;
}

+ (void)resetTutorialViewStatus
{
    NSArray *viewKey = @[KeyIsBestOfTutorialShown, KeyIsMyGalleryTutorialShown, KeyIsPlaybackTutorialShown];
    for (int i = 0; i < viewKey.count; i++) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:viewKey[i]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
