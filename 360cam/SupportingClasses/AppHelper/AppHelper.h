//
//  AppHelper.h
//  
//
//  Created by Kirill Gorbushko on 22.06.15.
//
//

static NSString *const KeyIsBestOfTutorialShown = @"KeyIsBestOfTutorialShown";
static NSString *const KeyIsMyGalleryTutorialShown = @"KeyIsMyGalleryTutorialShown";
static NSString *const KeyIsPlaybackTutorialShown = @"KeyIsPlaybackTutorialShown";

@class RootViewController;
@class MFMailComposeViewController;

@interface AppHelper : NSObject

+ (RootViewController *)rootViewControllerSideMenu;

+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate otherButtonTitles:(NSString *)otherButtonTitles, ...;
+ (void)alertViewWithMessage:(NSString *)message delegate:(id /*<UIAlertViewDelegate>*/)alertViewDelegate;
+ (void)alertViewWithMessage:(NSString *)message;

+ (NSString *)appName;

+ (void)showLoader;
+ (void)showLoaderWithText:(NSString *)text;
+ (void)hideLoader;

+ (BOOL)isIOS_8;

+ (UIView *)topView;

#pragma mark - Temp method for control tutorialView

+ (void)setTutorialViewedForKey:(NSString *)key;
+ (void)resetTutorialViewStatus;
+ (BOOL)isTutorialViewedForKey:(NSString *)key;

@end
