//
//  LoadImageManager.m
//  360cam
//
//  Created by Victor on 09.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "LoadImageManager.h"

static NSString *const BaseURLString = @"http://api.360.tv/";
static NSURLSession *downloadSession;

@implementation LoadImageManager

#pragma mark - LifeCycle

+ (id)sharedManager
{
    static LoadImageManager *loadImageManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loadImageManager = [[self alloc] init];
    });
    return loadImageManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        downloadSession = [self prepareSessionForRequest];
        [self setSharedCacheForImages];
    }
    return self;
}

#pragma mark - Public

- (void)loadImageWithURL:(NSString *)imageURL complemention:(void (^)(NSData *imageData))processImage
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    [request setHTTPMethod:@"GET"];
    NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:request];
    if (cachedResponse.data) {
        dispatch_async(dispatch_get_main_queue(), ^{
            processImage(cachedResponse.data);
        });
    } else {
        NSURLSessionDataTask *dataTask = [downloadSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    processImage(data);
                });
            }
        }];
        [dataTask resume];
    }
}

- (NSURLSession *)urlSession
{
    return downloadSession;
}

- (NSString *)baseURLString
{
    return BaseURLString;
}

#pragma mark - Private

- (NSURLSession *)prepareSessionForRequest
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{@"Content-Type": @"application/json", @"Accept": @"application/json"}];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    return session;
}

- (void)setSharedCacheForImages
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSUInteger cashSize = 250 * 1024 * 1024;
        NSUInteger cashDiskSize = 250 * 1024 * 1024;
        NSURLCache *imageCache = [[NSURLCache alloc] initWithMemoryCapacity:cashSize diskCapacity:cashDiskSize diskPath:@"nsURLCashe"];
        [NSURLCache setSharedURLCache:imageCache];
    });
}

@end
