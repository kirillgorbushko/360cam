//
//  LoadImageManager.h
//  360cam
//
//  Created by Victor on 09.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoadImageManager : NSObject

+ (id)sharedManager;
- (void)loadImageWithURL:(NSString *)imageURL complemention:(void (^)(NSData *imageData))processImage;
- (NSURLSession *)urlSession;
- (NSString *)baseURLString;

@end
