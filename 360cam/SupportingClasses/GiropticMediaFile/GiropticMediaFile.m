//
//  FileEntries.m
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 19.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "GiropticMediaFile.h"
#import "GiropticEndPoints.h"

@implementation GiropticMediaFile

#pragma mark - Public

+ (GiropticMediaFile *)getFileFromDictionary:(NSDictionary *)inputDictionary
{
    NSString *path = [NSString stringWithFormat:@"%@", GiroImageFilePath ];
    GiropticMediaFile *newFile = [[GiropticMediaFile alloc] init];
    newFile.isLink = [[inputDictionary valueForKey:@"isLink"] boolValue];
    newFile.kind = [inputDictionary valueForKey:@"kind"];
    newFile.modificationTimeMs = [NSDate dateWithTimeIntervalSince1970:[[inputDictionary valueForKey:@"modificatioTimeMs"] integerValue]];
    newFile.fileName = [inputDictionary valueForKey:@"name"];
    newFile.sizeInBytes = [[inputDictionary valueForKey:@"sizeBytes"] longLongValue];
    newFile.fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", path, [inputDictionary valueForKey:@"name"]]];
    newFile.fileType = [GiropticMediaFile fileTypeForFileWithName:[inputDictionary valueForKey:@"name"]];
    return newFile;
}

+ (NSString *)getKeyForFile:(NSString *)fileName suffix:(NSString *)keySuffix
{
    NSString *key = [NSString stringWithFormat:@"%@_%@", fileName, keySuffix];
    return key;
}

#pragma mark - Private

+ (FileType)fileTypeForFileWithName:(NSString *)fileName
{
    FileType selectedType;
    if ([fileName hasSuffix:@".JPG"]) {
        selectedType = FileTypeJPG;
    } else if ([fileName hasSuffix:@"_B"]) {
        selectedType = FileTypeBURST;
    } else if ([fileName hasSuffix:@"_TL"]) {
        selectedType = FileTypeTIMELAPS;
    } else if ([fileName hasSuffix:@".MP4"]) {
        selectedType = FileTypeVIDEO;
    } else if ([fileName hasSuffix:@"_HDR"]) {
        selectedType = FileTypeHDR;
    } else {
        selectedType = FileTypeUNDEFINED;
    }

    return selectedType;
}

@end
