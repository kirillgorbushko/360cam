//
//  ResizingLabel.m
//  360cam
//
//  Created by Norbert Citrak on 7/7/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ResizingLabel.h"

@implementation ResizingLabel

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    if (self.numberOfLines == 0 && bounds.size.width != self.preferredMaxLayoutWidth) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
        [self setNeedsUpdateConstraints];
    }
}

@end
