//
//  ResizingLabel.h
//  360cam
//
//  Created by Norbert Citrak on 7/7/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResizingLabel : UILabel

@end
