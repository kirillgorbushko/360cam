//
//  AssetModel.m
//  360cam
//
//  Created by Victor on 12.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AssetModel.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation AssetModel

- (UIImage *)fullScreenImage
{
    return [UIImage imageWithCGImage:self.alAsset.defaultRepresentation.fullScreenImage];
}

- (UIImage *)fullResolutionImage
{
    return [UIImage imageWithCGImage:self.alAsset.defaultRepresentation.fullResolutionImage];
}

- (BOOL)isLittlePlanetPhoto
{
    return [self.group isEqualToString:AlbumNameGiropticLittlePlanetLocalFies];
}

@end
