//
//  AssetModel.h
//  360cam
//
//  Created by Victor on 12.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class CLLocation;
@class ALAsset;

@interface AssetModel : NSObject

@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) ALAsset *alAsset;
@property (strong, nonatomic) NSURL *sourceUrl;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSDictionary *metadata;
@property (strong, nonatomic) NSDate *createdDate;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSString *group;

- (UIImage *)fullScreenImage;
- (UIImage *)fullResolutionImage;

- (BOOL)isLittlePlanetPhoto;

@end
