//
//  MapLocation.m
//  360cam
//
//  Created by Victor on 02.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "MapLocation.h"

@implementation MapLocation

@synthesize coordinate;

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.coordinate = CLLocationCoordinate2DMake(0, 0);
    }
    return self;
}

@end
