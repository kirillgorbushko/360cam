//
//  MapLocation.h
//  360cam
//
//  Created by Victor on 02.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MapLocation : NSObject <MKAnnotation>

@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@end
