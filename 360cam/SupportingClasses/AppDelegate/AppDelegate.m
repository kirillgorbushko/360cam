//
//  AppDelegate.m
//  360cam
//
//  Created by Kirill Gorbushko on 22.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "WiFiFetchService.h"
@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    WiFiFetchService *sharedSerivce = [WiFiFetchService sharedService];
    [sharedSerivce startNotifications];
    
    [AppHelper resetTutorialViewStatus];
    
    return YES;
}

#pragma mark - Rotation

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    UIViewController *topViewController = [self topViewController];
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskAll;
    } else if (![topViewController isKindOfClass:[RootViewController class]]) {
        if ([topViewController respondsToSelector:NSSelectorFromString(@"canRotate")]) {
            return UIInterfaceOrientationMaskAll;
        } else {
            return UIInterfaceOrientationMaskPortrait;
        }
    } else if ([topViewController isKindOfClass:[RootViewController class]]) {
        UIViewController *currentViewController = ((UINavigationController *)((RootViewController *)topViewController).contentViewController).topViewController;
        if ([currentViewController respondsToSelector:NSSelectorFromString(@"canRotate")]) {
            return UIInterfaceOrientationMaskAll;
        }
        return UIInterfaceOrientationMaskPortrait;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController *presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

@end
