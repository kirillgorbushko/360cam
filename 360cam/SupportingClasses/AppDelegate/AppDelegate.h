//
//  AppDelegate.h
//  360cam
//
//  Created by Kirill Gorbushko on 22.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

