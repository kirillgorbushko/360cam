//
//  ALHelper.m
//  360cam
//
//  Created by Victor on 12.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "AssetsManager.h"
#import "AssetModel.h"

@interface AssetsManager ()

@property (strong, nonatomic) ALAssetsLibrary *assetLibrary;

@end

@implementation AssetsManager

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.assetLibrary = [[ALAssetsLibrary alloc] init];
    }
    return self;
}

#pragma mark - Singleton

+ (id)sharedInstance
{
    static AssetsManager *alHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        alHelper = [[self alloc] init];
    });
    return alHelper;
}

#pragma mark - Public

- (void)loadDataForType:(MediaFileType)type withCompletitionHandler:(LoadCompletionHandler)completionHandler
{
    __block NSMutableArray *tempAssetsArray = [[NSMutableArray alloc] init];
    [self.assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group setAssetsFilter:(type == MediaFileTypePhotos) ? [ALAssetsFilter allPhotos] : [ALAssetsFilter allVideos]];
            if ([self shouldLoadGroup:group]) {
                [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
                    if (asset) {
                        AssetModel *assetModel = [[AssetModel alloc] init];
                        assetModel.fileName = [[asset defaultRepresentation] filename];
                        assetModel.alAsset = asset;
                        assetModel.sourceUrl = [[asset defaultRepresentation] url];
                        assetModel.type = (type == MediaFileTypePhotos) ? @"photo" : @"video";
                        assetModel.metadata = [[asset defaultRepresentation] metadata];
                        assetModel.createdDate = [asset valueForProperty:ALAssetPropertyDate];
                        assetModel.location = [asset valueForProperty:ALAssetPropertyLocation];
                        assetModel.group = [group valueForProperty:ALAssetsGroupPropertyName];
                        [tempAssetsArray addObject:assetModel];
                    }
                }];
            }
        }
        if (group == nil) {
            _assetArray = [[NSMutableArray alloc] initWithArray:tempAssetsArray];
            completionHandler(self.assetArray, nil);
        }
    } failureBlock:^(NSError *error) {
        completionHandler(nil, error);
    }];
}

- (void)savePhoto:(UIImage *)photo toAlbum:(NSString *)albumName withCompletitionHandler:(SaveCompletionHandler)completionHandler
{
    __weak typeof(self) weakSelf = self;
    [self.assetLibrary writeImageToSavedPhotosAlbum:photo.CGImage orientation:(ALAssetOrientation)photo.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
        [weakSelf addAssetURL:assetURL toAlbum:albumName withCompletionBlock:completionHandler];
    }];
}

- (void)saveVideo:(NSURL *)videoURL toAlbum:(NSString *)albumName withCompletation:(SaveCompletionHandler)completionHandler
{
    __weak typeof(self) weakSelf = self;
    [self.assetLibrary writeVideoAtPathToSavedPhotosAlbum:videoURL completionBlock:^(NSURL *assetURL, NSError *error) {
        [weakSelf addAssetURL:assetURL toAlbum:albumName withCompletionBlock:completionHandler];
    }];
}

- (void)deleteAsset:(NSURL *)assetURL withCompletitionHandler:(DeleteCompletionHandler)completionHandler
{
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHFetchResult *asset = [PHAsset fetchAssetsWithALAssetURLs:@[assetURL] options:nil];
        [PHAssetChangeRequest deleteAssets:asset];
    } completionHandler:^(BOOL success, NSError *error) {
        completionHandler(error);
    }];
}

#pragma mark - Private

- (void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName withCompletionBlock:(SaveCompletionHandler)completionHandler
{
    if (!albumName) {
        NSError *error = [[NSError alloc] initWithDomain:@"Album name is nil" code:404 userInfo:nil];
        completionHandler(error);
        return;
    }
    
    __block BOOL isFindAlbum = NO;
    [self.assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if ([albumName compare:[group valueForProperty:ALAssetsGroupPropertyName]] == NSOrderedSame) {
            isFindAlbum = YES;
            [self.assetLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                [group addAsset: asset];
                completionHandler(nil);
                *stop = YES;
            } failureBlock:completionHandler];
        }
        if (!group && !isFindAlbum) {
            __weak ALAssetsLibrary* weakAssestsLibary = self.assetLibrary;
            [self.assetLibrary addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group) {
                [weakAssestsLibary assetForURL: assetURL resultBlock:^(ALAsset *asset) {
                    [group addAsset: asset];
                    completionHandler(nil);
                } failureBlock: completionHandler];
            } failureBlock: completionHandler];
        }
    } failureBlock:completionHandler];
}

- (BOOL)shouldLoadGroup:(ALAssetsGroup *)assetsGroup
{
    NSString *albumName = [assetsGroup valueForProperty:ALAssetsGroupPropertyName];
    return [albumName isEqualToString:AlbumNameGiropticLocalFies] || [albumName isEqualToString:AlbumNameGiropticLittlePlanetLocalFies];
}

@end