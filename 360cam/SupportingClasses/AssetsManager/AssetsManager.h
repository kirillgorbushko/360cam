//
//  ALHelper.h
//  360cam
//
//  Created by Victor on 12.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef void (^LoadCompletionHandler) (NSArray *result, NSError *error);
typedef void (^SaveCompletionHandler) (NSError *error);
typedef void (^DeleteCompletionHandler) (NSError *error);

@interface AssetsManager : NSObject

@property (strong, nonatomic, readonly) NSMutableArray *assetArray;

+ (id)sharedInstance;

- (void)loadDataForType:(MediaFileType)type withCompletitionHandler:(LoadCompletionHandler)completionHandler;
- (void)savePhoto:(UIImage *)photo toAlbum:(NSString *)albumName withCompletitionHandler:(SaveCompletionHandler)completionHandler;
- (void)saveVideo:(NSURL *)videoURL toAlbum:(NSString *)albumName withCompletation:(SaveCompletionHandler)completionHandler;
- (void)deleteAsset:(NSURL *)assetURL withCompletitionHandler:(DeleteCompletionHandler)completionHandler;

@end
