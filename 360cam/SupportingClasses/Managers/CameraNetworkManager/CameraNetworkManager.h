//
//  CameraNetworkManager.h
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 18.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiropticSettingsParameters.h"

@class GiropticMediaFile;

typedef NS_ENUM(NSUInteger, GiropticCameraMode) {
    GiropticCameraModeNone,
    GiropticCameraModeVideo,
    GiropticCameraModeImage,
    GiropticCameraModeBurst,
    GiropticCameraModeTimeLaps,
    GiropticCameraModeLive
};

typedef NS_ENUM(NSUInteger, GiropticStreamURLType) {
    GiropticStreamURLTypeMain,
    GiropticStreamURLTypePreview
};

typedef NS_ENUM(NSUInteger, GiropticFileWriteMode) {
    GiropticFileWriteModeAppend,
    GiropticFileWriteModeOverwrite,
};

typedef void(^Result)(BOOL success, id response, NSError *error);

@interface CameraNetworkManager : NSObject

@property (copy, nonatomic) __block NSString *sessionID;
@property (assign, nonatomic, readonly) __block BOOL isSessionActive;
@property (assign, nonatomic) __block GiropticCameraMode selectedMode;
@property (assign, nonatomic) __block NSInteger totalEntriesCount;
@property (copy, nonatomic) __block NSString *continuationToken;

+ (instancetype)sharedManager;

- (BOOL)isCameraConnected;
- (void)setStartPointForFileListToZero;

- (void)giropticStartSessionWithDurationInSec:(NSInteger)duration result:(Result)startSessionResult;
- (void)giropticUpdateSessionWithTimeOut:(NSInteger)timeout sessionId:(NSString *)sessionId operationResult:(Result)sessionUpdateResult;
- (void)giropticCloseSessionWithSessionId:(NSString *)sessionId operationResult:(Result)closeSessionResult;

- (void)giropticSwitchToMode:(GiropticCameraMode)mode operationResult:(Result)switchModeResult;
- (void)giropticGetFileAttributesWithURL:(NSString *)fileUri operationresult:(Result)getFileResult;
- (void)giropticGetListOfFilesCount:(NSInteger)requestedFilesCount filePath:(NSString *)filePath continuationToken:(NSString *)continuationToken operationResult:(Result)operationResult;
- (void)giropticGetFullListOfFilesForPath:(NSString *)filesPath operationResult:(Result)operationResult;
- (void)giropticGetState:(Result)oprationResult;

- (void)giropticTakePicture:(Result)takePictureResult;
- (void)giropticStartRecording:(Result)startRecordingResult;
- (void)giropticStopRecording:(Result)stopRecordingResult;
- (void)giropticGetStreamURLWithType:(GiropticStreamURLType)streamURLType operationResult:(Result)getStreamURLResult;
- (void)giropticGetImagePreview:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height operationResult:(Result)getImagePreviewResult;
- (void)giropticReadFile:(NSString *)fileUri bytesToread:(long long int)bytesLength fromValue:(long long int)readFromBytes operationResult:(Result)readFileResult;
- (void)giropticWriteFile:(NSString *)fileUri fileContent:(NSDate *)content writingMode:(GiropticFileWriteMode)mode ovverwriteIfExist:(BOOL)mustOverride operationResult:(Result)writeFileResult;
- (void)giropticDeleteFileWithURL:(NSString *)fileUri isDirectory:(BOOL)isDirectory operationResult:(Result)deleteFileResult;
- (void)giropticGetImage:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height operationResult:(Result)getImageResult;
- (void)giropticGetCameraInfoWithResult:(Result)getResult;

- (void)giropticSetMetadataOperationForFile:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height GPSLatitude:(CGFloat)latitude GPSLongitude:(CGFloat)longitude Result:(Result)setMetadataResult;
- (void)giropticPatchMetadataForfileUri:(NSString *)fileUri GPSLatitude:(CGFloat)latitude GPSLongitude:(CGFloat)longitude operationResult:(Result)patchMetadataResult;
- (void)giropticGetMetadataForfileUri:(NSString *)fileUri operationResult:(Result)getMetadataResult;
- (void)giropticSetOptions:(NSDictionary *)optionsWithSettings operationResult:(Result)setOptionResult;
- (void)giropticGetOptions:(NSArray *)optionsWithSettings operationResult:(Result)getOptionResult;




@end
