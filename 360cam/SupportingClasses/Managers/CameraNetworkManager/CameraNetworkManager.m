//
//  CameraNetworkManager.m
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 18.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "CameraNetworkManager.h"
#import "AFNetworking.h"
#import "GiropticEndPoints.h"
#import "NSString+Addition.h"
#import "GiropticMediaFile.h"
#import "WiFiFetchService.h"

@interface CameraNetworkManager () 

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

@property (copy, nonatomic) __block NSString *internalContinuationToken;
@property (strong, nonatomic) __block NSMutableArray *filesList;

@end

@implementation CameraNetworkManager

#pragma mark - Public

#pragma mark - Singleton

+ (instancetype)sharedManager
{
    static CameraNetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[CameraNetworkManager alloc] init];
    });
    return sharedManager;
}

#pragma mark - Connection Command

- (BOOL)isCameraConnected
{
//    return YES;
    if ([WiFiFetchService sharedService].currentStatus == FetchServiceStatusConnected) {
        return YES;
    } else {
        return NO;
    }
}

- (void)giropticStartSessionWithDurationInSec:(NSInteger)duration result:(Result)startSessionResult
{
    NSDictionary *startSessionParameters = @{
                                             @"name" : @"camera.startSession",
                                             @"parameters" : @{
                                                     @"timeout" : @(duration)
                                                     }
                                             };
    [self performBasePOSTWithParameters:startSessionParameters operationResult:startSessionResult];
}

- (void)giropticUpdateSessionWithTimeOut:(NSInteger)timeout sessionId:(NSString *)sessionId operationResult:(Result)sessionUpdateResult
{
    if (!self.sessionID.length) {
        sessionUpdateResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"Cant Update session"]);
        return;
    }
    NSDictionary *updateParameters = @{
                                       @"name" : @"camera.updateSession",
                                       @"parameters" : @{
                                               @"sessionId" : sessionId,
                                               @"timeout" : @(timeout)
                                               }
                                       };
    [self performBasePOSTWithParameters:updateParameters operationResult:sessionUpdateResult];
}

- (void)giropticCloseSessionWithSessionId:(NSString *)sessionId operationResult:(Result)closeSessionResult
{
    if (!sessionId.length) {
        if (!self.isSessionActive || !self.sessionID) {
            closeSessionResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"Cant close nil session"]);
            return;
        } else {
            sessionId = self.sessionID;
        }
    }
    NSLog(@"Current session ID to close -%@", sessionId);
    
    NSDictionary *closeSessionParameters = @{
                                             @"name" : @"camera.closeSession",
                                             @"parameters" : @{
                                                     @"sessionId" : sessionId
                                                     }
                                             };
    [self.manager POST:GiroBaseURL parameters:closeSessionParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            closeSessionResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            closeSessionResult(YES, responseDictionary, nil);
            _isSessionActive = NO;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        closeSessionResult(NO, nil, error);
    }];
}

#pragma mark - Control Command

- (void)giropticSwitchToMode:(GiropticCameraMode)mode operationResult:(Result)switchModeResult
{
    if (!self.sessionID.length) {
        switchModeResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    NSString *selectedMode;
    switch (mode) {
        case GiropticCameraModeImage: {
            selectedMode = GiroCaptureModeImage;
            break;
        }
        case GiropticCameraModeBurst: {
            selectedMode = GiroCaptureModeBurst;
            break;
        }
        case GiropticCameraModeLive: {
            selectedMode = GiroCaptureModeLive;
            break;
        }
        case GiropticCameraModeTimeLaps: {
            selectedMode = GiroCaptureModeTimeLapse;
            break;
        }
        case GiropticCameraModeVideo: {
            selectedMode = GiroCaptureModeVideo;
            break;
        }
        default:
            break;
    }
    _selectedMode = mode;
    
    NSDictionary *parametersDict = @{
                                     @"name" : @"camera.setOptions",
                                     @"parameters" : @{
                                             @"sessionId" : self.sessionID,
                                             @"options" : @{
                                                     @"captureMode": selectedMode,
                                                     }
                                             }
                                     };
    [self.manager POST:GiroBaseURL parameters:parametersDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            switchModeResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            switchModeResult(YES, responseDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        switchModeResult(NO, nil, error);
    }];
}

- (void)giropticGetFileAttributesWithURL:(NSString *)fileUri operationresult:(Result)getFileResult
{
    NSDictionary *parameters = @{
                                 @"name" : @"camera._getAttributes",
                                 @"parameters" : @{
                                         @"fileUri" :fileUri
                                         }
                                 };
    [self.manager POST:GiroBaseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            getFileResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            getFileResult(YES, responseDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        getFileResult(NO, nil, error);
    }];

    [self performPOSTWithParameters:parameters operationResult:getFileResult];
}

- (void)giropticGetListOfFilesCount:(NSInteger)requestedFilesCount filePath:(NSString *)filePath continuationToken:(NSString *)continuationToken operationResult:(Result)operationResult
{
    NSString *path = [NSString stringWithFormat:@"%@", GiroImageFilePath];
    if (filePath.length) {
        path = filePath;
    }
    if (!continuationToken.length) {
        continuationToken = @"";
    }
    
    NSDictionary *parameters = @{
                                 @"name" : @"camera._listDirectory",
                                 @"parameters" : @{
                                         @"fileUri" : path,
                                         @"entryCount" : @(requestedFilesCount),
                                         @"continuationToken" : continuationToken
                                         }
                                 };
    __weak CameraNetworkManager *weakSelf = self;
    [self.manager POST:GiroBaseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
        } else {
            
            NSDictionary *result = [responseDictionary valueForKey:@"results"];
            weakSelf.continuationToken = [result valueForKey:@"continuationToken"];
            weakSelf.totalEntriesCount = [[result valueForKey:@"totalEntries"] integerValue];
            NSArray *entries = [result valueForKey:@"entries"];
            
            NSMutableArray *requestResult = [[NSMutableArray alloc] init];
            for (NSDictionary *file in entries) {
                [requestResult addObject:[GiropticMediaFile getFileFromDictionary:file]];
            }
            
            operationResult(YES, requestResult, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        operationResult(NO, nil, error);
    }];
}

- (void)giropticGetFullListOfFilesForPath:(NSString *)filesPath operationResult:(Result)operationResult
{
    if (!self.internalContinuationToken.length) {
        self.internalContinuationToken = @"";
    }
    if (!self.filesList) {
        self.filesList = [[NSMutableArray alloc] init];
    }
    
    __weak typeof(self) weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetListOfFilesCount:10 filePath:filesPath continuationToken:self.internalContinuationToken operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf.filesList addObjectsFromArray:response];
            NSLog(@"Files fetched - %ld", (long)weakSelf.filesList.count);
            if (weakSelf.filesList.count != weakSelf.totalEntriesCount) {
                [[CameraNetworkManager sharedManager] giropticGetFullListOfFilesForPath:filesPath operationResult:operationResult];
            } else {
                operationResult(YES,[[CameraNetworkManager sharedManager] generatePathListForDir:filesPath fromArrayWithMediaObjects:response], nil);
            }
        } else {
            operationResult(NO, nil, error);
        }
    }];
}

- (void)giropticGetState:(Result)oprationResult
{
    [self.manager GET:GiroCameraState parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
        } else {
            oprationResult(YES, responseDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        oprationResult(NO, nil, error);
    }];
    
}

- (NSArray *)generatePathListForDir:(NSString *)dirPath fromArrayWithMediaObjects:(NSArray *)mediaObjects
{
    NSMutableArray *paths = [[NSMutableArray alloc] init];
    for (GiropticMediaFile *mediaFile in mediaObjects) {
         [paths addObject:[NSString stringWithFormat:@"%@/%@", dirPath, mediaFile.fileName]];
    }
    return paths;
}

- (void)giropticSetMetadataOperationForFile:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height GPSLatitude:(CGFloat)latitude GPSLongitude:(CGFloat)longitude Result:(Result)setMetadataResult
{
    NSDictionary *parameters = @{
                                 @"name" : @"camera._setMetadata",
                                 @"parameters" : @{
                                         @"fileUri" : fileUri,
                                         @"exif" : @{
                                                 @"I​mageWidth" : @(width),
                                                 @"I​mageHeight": @(height),
                                                 @"G​PSLatitude": @(latitude),
                                                 @"G​PSLongitude" : @(longitude)
                                                 }
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:setMetadataResult];
}

- (void)giropticPatchMetadataForfileUri:(NSString *)fileUri GPSLatitude:(CGFloat)latitude GPSLongitude:(CGFloat)longitude operationResult:(Result)patchMetadataResult
{
    NSString *path = [NSString stringWithFormat:@"%@/%@", GiroImageFilePath , fileUri];

    NSDictionary *parameters = @{
                                 @"name" : @"camera._patchMetadata",
                                 @"parameters" :@{
                                         @"fileUri" : path,
                                         @"exif" : @{
                                                 @"G​PSLatitude": @(latitude),
                                                 @"G​PSLongitude" : @(longitude)
                                                 }
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:patchMetadataResult];
}

- (void)giropticGetMetadataForfileUri:(NSString *)fileUri operationResult:(Result)getMetadataResult
{
    NSString *path = [NSString stringWithFormat:@"%@/%@", GiroImageFilePath , fileUri];

    NSDictionary *parameters = @{
                                 @"name" : @"camera.getMetadata",
                                 @"parameters" :@{
                                         @"fileUri" : path,
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:getMetadataResult];
}

#pragma mark - Options Command

- (void)giropticSetOptions:(NSDictionary *)optionsWithSettings operationResult:(Result)setOptionResult
{
    if (!self.sessionID.length) {
        setOptionResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    NSDictionary *parameters = @{
                                 @"name" : @"camera.setOptions",
                                 @"parameters" : @{
                                         @"sessionId" : self.sessionID,
                                         @"options" : optionsWithSettings
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:setOptionResult];
}

- (void)giropticGetOptions:(NSArray *)optionsWithSettings operationResult:(Result)getOptionResult
{
    if (!self.sessionID.length) {
        getOptionResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    NSDictionary *parameters = @{
                                 @"name" : @"camera.getOptions",
                                 @"parameters" : @{
                                         @"sessionId" : self.sessionID,
                                         @"optionNames" : optionsWithSettings
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:getOptionResult];
}

#pragma mark - Operation Command

- (void)giropticTakePicture:(Result)takePictureResult
{
    if (!self.sessionID.length) {
        takePictureResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    if (self.selectedMode == GiropticCameraModeImage || self.selectedMode == GiropticCameraModeBurst) {
        NSDictionary *parameters = @{
                                     @"name" : @"camera.takePicture",
                                     @"parameters" : @{
                                             @"sessionId" : self.sessionID
                                             }
                                     };
        
        [self performPOSTWithParameters:parameters operationResult:takePictureResult];
    } else {
        takePictureResult (NO, nil, [self prepareErrorForIncorrectModeWithCode:@"Please change mode"]);
        return;
    }
}

- (void)giropticStartRecording:(Result)startRecordingResult
{
    if (!self.sessionID.length) {
        startRecordingResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    if (self.selectedMode == GiropticCameraModeVideo || self.selectedMode == GiropticCameraModeTimeLaps) {
        
        NSDictionary *parameters = @{
                                     @"name" : @"camera._startRecording",
                                     @"parameters" : @{
                                             @"sessionId" : self.sessionID
                                             }
                                     };
        
        [self performPOSTWithParameters:parameters operationResult:startRecordingResult];
    } else {
        startRecordingResult (NO, nil, [self prepareErrorForIncorrectModeWithCode:@"Please change mode"]);
    }
}

- (void)giropticStopRecording:(Result)stopRecordingResult
{
    if (!self.sessionID.length) {
        stopRecordingResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    NSDictionary *parameters = @{
                                 @"name" : @"camera._stopRecording",
                                 @"parameters" : @{
                                         @"sessionId" : self.sessionID
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:stopRecordingResult];
}

- (void)giropticGetStreamURLWithType:(GiropticStreamURLType)streamURLType operationResult:(Result)getStreamURLResult
{
    if (!self.sessionID.length) {
        getStreamURLResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:@"No session ID"]);
        return;
    }

    NSString *selectedStreamType;
    switch (streamURLType) {
        case GiropticStreamURLTypeMain: {
            selectedStreamType = GiroStreamTypeURLMain;
            break;
        }
        case GiropticStreamURLTypePreview: {
            selectedStreamType = GiroStreamTypeURLPreview;
            break;
        }
        default:
            break;
    }
    
    NSDictionary *parameters = @{
                                 @"name" : @"camera._getStreamUrl",
                                 @"parameters" : @{
                                         @"sessionId" : self.sessionID,
                                         @"type" : selectedStreamType
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:getStreamURLResult];
}

- (void)giropticGetImagePreview:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height operationResult:(Result)getImagePreviewResult
{
    if (!fileUri.length) {
        return;
    }
    
    if (!width) {
        width = 512;
    }
    if (!height) {
        height = 256;
    }
    
    NSDictionary *parameters = @{
                                 @"name" : @"camera._getImagePreview",
                                 @"parameters" : @{
                                         @"fileUri" : fileUri,
                                         @"width" : @(width),
                                         @"height" : @(height),
                                         @"format" : @"base64"
                                         }
                                 };
    
    [self.manager POST:GiroBaseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Cant get preview for file - %@", [fileUri lastPathComponent]);
            getImagePreviewResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            NSData *responseData = (NSData *)responseObject;
            NSError *readingError;
            
            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
            responseString = [[responseString componentsSeparatedByString:@"\n"] componentsJoinedByString:@""];
            responseString = [responseString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
            responseString = [responseString stringByRemovingControlCharacters];
            responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&readingError];
            NSDictionary *resultedDataDictionary = [[responseDictionary valueForKey:@"result"] valueForKey:@"content"];
            
            getImagePreviewResult(YES, resultedDataDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        getImagePreviewResult(NO, nil, error);
    }];
}

- (void)giropticGetCameraInfoWithResult:(Result)getResult
{
    [self.manager GET:GiroCameraInfoURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
        } else {
            getResult(YES, responseDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        getResult(NO, nil, error);
    }];
}

- (void)giropticReadFile:(NSString *)fileUri bytesToread:(long long int)bytesLength fromValue:(long long int)readFromBytes operationResult:(Result)readFileResult
{
    NSDictionary *parameters;
    
    if (bytesLength && readFromBytes >= 0) {
        parameters = @{
                       @"name" : @"camera._readFile",
                       @"parameters" : @{
                               @"fileUri" : fileUri,
                               @"range" : @{
                                       @"firstByte" : @(readFromBytes),
                                       @"lastByte" : @(bytesLength)
                                       },
                               @"format" : @"base64"
                               }
                       };
    } else {
        parameters = @{
                       @"name" : @"camera._readFile",
                       @"parameters" : @{
                               @"fileUri" : fileUri,
                               @"format" : @"base64"
                               }
                       };
    }
    
    [self.manager POST:GiroBaseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
            readFileResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            NSData *responseData = (NSData *)responseObject;
            NSError *readingError;
            
            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            responseString = [[responseString componentsSeparatedByString:@"\n"] componentsJoinedByString:@""];
            responseString = [responseString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
            responseString = [responseString stringByRemovingControlCharacters];
            responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&readingError];
            NSDictionary *resultedDataDictionary = [[responseDictionary valueForKey:@"result"] valueForKey:@"content"];
                        
            readFileResult(YES, resultedDataDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        readFileResult(NO, nil, error);
    }];
}

- (void)giropticWriteFile:(NSString *)fileUri fileContent:(NSDate *)content writingMode:(GiropticFileWriteMode)mode ovverwriteIfExist:(BOOL)mustOverride operationResult:(Result)writeFileResult
{
    NSString *selectedWritingMode;
    switch (mode) {
        case GiropticFileWriteModeAppend: {
            selectedWritingMode = GiroWriteModeAppend;
            break;
        }
        case GiropticFileWriteModeOverwrite: {
            selectedWritingMode = GiroWriteModeOverwrite;
            break;
        }
        default:
            break;
    }
    
    NSDictionary *parameters = @{
                                 @"name" : @"camera._writeFile",
                                 @"parameters" : @{
                                         @"fileUri" : fileUri,
                                         @"content" : content,
                                         @"writeMode" : selectedWritingMode,
                                         @"mustNotExist" : @(mustOverride)
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:writeFileResult];
}

- (void)giropticDeleteFileWithURL:(NSString *)fileUri isDirectory:(BOOL)isDirectory operationResult:(Result)deleteFileResult
{
    NSDictionary *parameters = @{
                                 @"name" : @"camera.delete",
                                 @"parameters" : @{
                                         @"fileUri" : fileUri,
                                         @"recursive" : @(isDirectory)
                                         }
                                 };
    
    [self performPOSTWithParameters:parameters operationResult:deleteFileResult];
}

- (void)giropticGetImage:(NSString *)fileUri imageWidth:(NSInteger)width imageHeight:(NSInteger)height operationResult:(Result)getImageResult
{
    NSMutableDictionary *innerParameters = [ @{ @"fileUri" : fileUri } mutableCopy];
    
    if (width && height) {
        [innerParameters setValue: @{
                                     @"scaledimage_width" : @(width),
                                     @"scaledimage_height" : @(height)
                                     }
                           forKey:@"maxSize"];
    }
    
    NSDictionary *parameters = @{
                                 @"name" : @"camera.getImage",
                                 @"parameters" : innerParameters
                                 };
    
    [self.manager POST:GiroBaseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
            getImageResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            NSData *responseData = (NSData *)responseObject;
            UIImage *image = [UIImage imageWithData:responseData];
            getImageResult(YES, image, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        getImageResult(NO, nil, error);
    }];
}

#pragma mark - Management

- (void)setStartPointForFileListToZero
{
    self.continuationToken = @"noToken";
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self managerSetup];
    }
    return self;
}

#pragma mark - Private

#pragma mark - Setups

- (void)managerSetup
{
    _selectedMode = GiropticCameraModeVideo;
    
    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:GiroBaseURL]];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;
}

#pragma mark - ErrorHandling

- (NSError *)prepareErrorForIncorrectModeWithCode:(NSString *)code
{
    if (!code.length) {
        code = @"Operation cannot be completed. Incorrect response from camera";
    }
    NSDictionary *errorDescripionDic = @{
                                         NSLocalizedDescriptionKey : code,
                                         NSLocalizedRecoverySuggestionErrorKey : @"No suggestion"
                                         };
    NSError *error = [NSError errorWithDomain:@"" code:GiroErrorIncorrectOperationRequest userInfo:errorDescripionDic];
    return error;
}

#pragma mark - POST

- (void)performBasePOSTWithParameters:(NSDictionary *)parameter operationResult:(Result)postOperationResult
{
    [self.manager POST:GiroBaseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            NSLog(@"Error");
            postOperationResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            NSDictionary *paramDict = [responseDictionary objectForKey:@"results"];
            
            _sessionID = [paramDict objectForKey:@"sessionId"];
            
            [[NSUserDefaults standardUserDefaults] setValue:_sessionID forKey:GiroCurrentSessionID];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            _isSessionActive = YES;
            postOperationResult(YES, paramDict, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        postOperationResult(NO, nil, error);
    }];
}

- (void)performPOSTWithParameters:(NSDictionary *)parameter operationResult:(Result)postOperationResult
{
    [self.manager POST:GiroBaseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *err;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&err];
        if ([[responseDictionary valueForKey:@"error"] valueForKey:@"code"]) {
            postOperationResult(NO, nil, [self prepareErrorForIncorrectModeWithCode:[[responseDictionary valueForKey:@"error"] valueForKey:@"code"]]);
        } else {
            postOperationResult(YES, responseDictionary, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        postOperationResult(NO, nil, error);
    }];
}

@end