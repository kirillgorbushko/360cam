//
//  WiFiFetchService
//  360cam
//
//  Created by Kirill Gorbushko on 10.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "WiFiFetchService.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "GiropticEndPoints.h"

@interface WiFiFetchService()

@property (strong, nonatomic) NSTimer *fetchDeviceConnectionTimer;
@property (assign, nonatomic) FetchServiceStatus previousStatus;

@end

@implementation WiFiFetchService

#pragma mark - Public

+ (instancetype)sharedService
{
    static WiFiFetchService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[WiFiFetchService alloc] init];
    });
    return sharedService;
}

- (void)startNotifications
{
    self.fetchDeviceConnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1. target:self selector:@selector(fetchCurrentDeviceConnectionDetails) userInfo:nil repeats:YES];
}

- (void)stopNotifications
{
    [self.fetchDeviceConnectionTimer invalidate];
    self.fetchDeviceConnectionTimer = nil;
}

#pragma mark - Custom Accessors

- (void)setCurrentStatus:(FetchServiceStatus)currentStatus
{
    if (currentStatus == self.previousStatus) {
        return;
    } else {
        self.previousStatus = currentStatus;
        _currentStatus = currentStatus;
        [self postNotificationWithStatus:currentStatus];
        if (self.delegate && [self.delegate respondsToSelector:@selector(currentFetchServiceStatus:)]) {
            [self.delegate currentFetchServiceStatus:currentStatus];
        }
    }
}

#pragma mark - Private

- (void)postNotificationWithStatus:(FetchServiceStatus)newStatus
{
    switch (newStatus) {
        case FetchServiceStatusDisconnected: {
            [[NSNotificationCenter defaultCenter] postNotificationName:DeviceDidDisconnectedToCameraNotification object:nil];
            break;
        }
        case FetchServiceStatusConnected: {
            [[NSNotificationCenter defaultCenter] postNotificationName:DeviceDidConnectedToCameraNotification object:nil];
            break;
        }
        default:
            break;
    }
}

- (void)fetchCurrentDeviceConnectionDetails
{
    NSDictionary *wifiSettings = [[WiFiFetchService sharedService] getConnectionDetails];
    NSString *currentBSSID = [wifiSettings valueForKey:@"BSSID"];
    _cameraName = [wifiSettings valueForKey:@"SSID"];
    _macAddress = [wifiSettings valueForKey:@"BSSID"];
    BOOL isCameraConnected = NO;
    if (!currentBSSID.length) {
        isCameraConnected = NO;
    } else {
        isCameraConnected = [currentBSSID rangeOfString:GiroBSSIDMACPart].location != NSNotFound;
    }
    if (isCameraConnected) {
        [self setCurrentStatus:FetchServiceStatusConnected];
    } else {
        [self setCurrentStatus:FetchServiceStatusDisconnected];
    }
}

- (NSDictionary *)getConnectionDetails
{
    NSDictionary *connectionDetails = [NSDictionary dictionary];
    CFArrayRef myArray = CNCopySupportedInterfaces();
    if (myArray) {
        CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        connectionDetails = (__bridge_transfer NSDictionary*)myDict;
    }
    return connectionDetails;
}


@end
