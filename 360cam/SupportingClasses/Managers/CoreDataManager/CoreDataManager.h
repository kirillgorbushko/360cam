//
//  CoreDataManager.h
//  NSFetchedResultsController
//
//  Created by Mike on 7/13/15.
//  Copyright (c) 2015 Mike. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@protocol CoreDataManagerDelegate <NSObject>

@required
- (void) getErrorFromCoreDataManager:(NSError *)error;

@end

@interface CoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (weak, nonatomic) id<CoreDataManagerDelegate> delegate;

+ (instancetype) sharedManager;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
