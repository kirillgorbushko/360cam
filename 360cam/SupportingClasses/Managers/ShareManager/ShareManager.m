//
//  ShareManager.m
//  ShareTwitter
//
//  Created by Mike on 7/7/15.
//  Copyright (c) 2015 Mike. All rights reserved.
//

#import "ShareManager.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <linkedin-sdk/LISDK.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

static NSString *const FacebookAPIKey = @"842577732492976";
static NSString *const GoogleClientID = @"1081137465269-nigpqe2i2lmrklg5qb6elfv8qtn3b0v7.apps.googleusercontent.com";

@interface ShareManager() <GPPSignInDelegate, GPPShareDelegate>

@property(strong ,nonatomic) LISDKSession *linkedInSession;
@property(strong, nonatomic) NSString *messageBuffer;
@property(strong, nonatomic) UIImage *imageBuffer;
@end

@implementation ShareManager

#pragma maek - Lifecycle

+ (instancetype)sharedManager
{
    static dispatch_once_t onceToken;
    static id sharedManager = nil;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

#pragma mark - Public

- (void)shareImage:(UIImage *)image onGooglePlusWithMessage:(NSString *)message
{
    self.messageBuffer = message;
    self.imageBuffer = image;
    [self authenticateGooglePlusUser];
}

- (void)shareImage:(UIImage *)image onTwitterWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion
{
    if (![self checkImage:image andUpdateMessage:message]) {
        NSError *error = [[NSError alloc] initWithDomain:@"Image not found" code:3 userInfo:nil];
        completion(error);
        return;
    }
    __weak typeof(self) weakSelf = self;
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier: ACAccountTypeIdentifierTwitter];
    [account requestAccessToAccountsWithType: accountType
                                     options: nil
                                  completion: ^(BOOL granted, NSError *error){
        if (!granted) {
             completion([weakSelf generareErrorFromGrantedError:error]);
             return;
         }
        NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
        if (!arrayOfAccounts.count) {
            error = [[NSError alloc] initWithDomain:@"Accounts not found" code:2 userInfo:nil];
            completion(error);
            return;
        }
        ACAccount *twitterAccount = [arrayOfAccounts firstObject];
        NSDictionary *messageDictionary = @{@"status": message};
        NSURL *requestURL = [NSURL URLWithString:@"https://upload.twitter.com/1/statuses/update_with_media.json"];
        SLRequest *postRequest = [SLRequest requestForServiceType: SLServiceTypeTwitter requestMethod: SLRequestMethodPOST URL: requestURL parameters: messageDictionary];
        postRequest.account = twitterAccount;
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        [postRequest addMultipartData:imageData withName:@"media" type:@"image/jpeg" filename:@"imge"];
        [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error){
            completion([weakSelf generateErrorFromStatusCode:urlResponse.statusCode]);
        }];
    }];
}

- (void)shareImage:(UIImage *)image onFacebookWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion
{
    if (![self checkImage:image andUpdateMessage:message]) {
        NSError *error = [[NSError alloc] initWithDomain:@"Image not found" code:3 userInfo:nil];
        completion(error);
        return;
    }
    __weak typeof(self) weakSelf = self;
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSDictionary *options = @{ACFacebookAppIdKey: FacebookAPIKey, ACFacebookPermissionsKey: @[@"email"], ACFacebookAudienceKey: ACFacebookAudienceEveryone};
    [account requestAccessToAccountsWithType:accountType options:options completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            completion([weakSelf generareErrorFromGrantedError:error]);
            return;
        }
        NSDictionary *optionsPublish = @{ACFacebookAppIdKey: FacebookAPIKey, ACFacebookPermissionsKey: @[@"publish_actions"], ACFacebookAudienceKey: ACFacebookAudienceFriends};
        [account requestAccessToAccountsWithType:accountType options:optionsPublish completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            completion([weakSelf generareErrorFromGrantedError:error]);
            return;
        }
        NSArray *accounts = [account accountsWithAccountType:accountType];
        if (!accounts.count) {
            error = [[NSError alloc] initWithDomain:@"Accounts not found" code:2 userInfo:nil];
            completion(error);
            return;
        }
        ACAccount *facebookAccount = [accounts firstObject];
        NSDictionary *parameters = @{@"message": message};
        SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:[NSURL URLWithString:@"https://graph.facebook.com/me/photos"] parameters:parameters];
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        [facebookRequest addMultipartData: imageData withName:@"source" type:@"multipart/form-data" filename:@"image"];
        [facebookRequest setAccount:facebookAccount];
        [facebookRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse,NSError *error){
            completion([weakSelf generateErrorFromStatusCode:urlResponse.statusCode]);
            }];
        }];
    }];
}

- (void)shareImageURL:(NSString *)imageURL onLinkedInWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion
{
    if (!message) {
        message = @"";
    }
    __weak typeof(self) weakSelf = self;
    if (!self.linkedInSession) {
        [LISDKSessionManager
         createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_W_SHARE_PERMISSION, nil]state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *returnState) {
             weakSelf.linkedInSession = [[LISDKSessionManager sharedInstance] session];
             [weakSelf postImageWithURL:imageURL andMessage:message andCompletionBlock:completion];
         }
         errorBlock:^(NSError *error) {
             completion(error);
         }];
    } else {
        if ([LISDKSessionManager hasValidSession]) {
            [weakSelf postImageWithURL:imageURL andMessage:message andCompletionBlock:completion];
        } else {
            NSError *error = [[NSError alloc] initWithDomain:@"Session not valid" code:2 userInfo:nil];
            completion(error);
        }
    }
}

- (void)shareOnLinkedInMessage:(NSString *)message withCompletionBlock:(SharedCompletionBlock)completion
{
    [self shareImageURL:nil onLinkedInWithMessage:message andCompletionBlock:completion];
}

#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(getErrorMessageFromGooglePlus:)]) {
            [self.delegate getErrorMessageFromGooglePlus:error];
        }
    } else {
        [GPPShare sharedInstance].delegate = self;
        id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
        [shareBuilder setPrefillText:self.messageBuffer];
        [shareBuilder attachImage:self.imageBuffer];
        [shareBuilder open];
    }
}

#pragma mark - GPPShareDelegate

- (void)finishedSharingWithError:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(getErrorMessageFromGooglePlus:)]) {
        [self.delegate getErrorMessageFromGooglePlus:error];
    }
}

#pragma mark - Private

 - (void)authenticateGooglePlusUser
{
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = GoogleClientID;
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.delegate = self;
    [signIn authenticate];
}

- (NSError *)generateLinkedInErrorFromStatusCode:(LISDKErrorCode)code
{
    switch (code) {
        case NONE:
            return [[NSError alloc] initWithDomain:@"There is something wrong with the request" code:NONE userInfo:nil];
        case INVALID_REQUEST:
            return [[NSError alloc] initWithDomain:@"Unable to complete request due to a network error" code:NONE userInfo:nil];
        case NETWORK_UNAVAILABLE:
            return [[NSError alloc] initWithDomain:@"The user cancelled the operation" code:NONE userInfo:nil];
        case USER_CANCELLED:
            return [[NSError alloc] initWithDomain:@"User canceled operation" code:NONE userInfo:nil];
        case UNKNOWN_ERROR:
            return [[NSError alloc] initWithDomain:@"There is something wrong with the request" code:NONE userInfo:nil];
        case SERVER_ERROR:
            return [[NSError alloc] initWithDomain:@"An error occurred within LinkedIn's" code:NONE userInfo:nil];
        case LINKEDIN_APP_NOT_FOUND:
            return [[NSError alloc] initWithDomain:@"Linkedin App not found on device or the version of the Linkedin App installed does not support the sdk" code:NONE userInfo:nil];
        default:
            return [[NSError alloc] initWithDomain:@"The user is not properly authenticated on this device" code:NONE userInfo:nil];
    }
}

- (void)postImageWithURL:(NSString *)imageUrl andMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion
{
    NSString *url = @"https://api.linkedin.com/v1/people/~/shares";
    NSDictionary *postData = [self getJsonDictionartFromImageURLForLinkedIn:imageUrl andMessage:message];
   // __weak typeof(self) weakSelf = self;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postData options:0 error:&error];
    if (error) {
        completion(error);
        return;
    }
    if ([LISDKSessionManager hasValidSession]) {
        [[LISDKAPIHelper sharedInstance] postRequest:url body:jsonData success:^(LISDKAPIResponse *response) {
            completion(nil);
        } error:^(LISDKAPIError *apiError) {
            completion(error);
        }];
    } else {
        NSError *error = [[NSError alloc] initWithDomain:@"Sesion  not started" code:4 userInfo:nil];
        completion(error);
    }
}

- (NSDictionary*)getJsonDictionartFromImageURLForLinkedIn:(NSString *)imageUrl andMessage:(NSString *)message
{
    NSDictionary *postData;
    if (imageUrl) {
        postData = @{@"content":@{@"title":@"360cam", @"description":message, @"submitted-url":imageUrl, @"submitted-image-url":imageUrl}, @"visibility":@{@"code":@"anyone"}};
    } else {
        postData = @{@"comment":message, @"visibility":@{@"code":@"anyone"}};
    }
    return postData;
}

- (SLRequest *)generateSLRequestFromMessage:(NSString *)message andUrlString:(NSString *)urlString
{
    NSDictionary *parameters = @{@"message": message};
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:[NSURL URLWithString:urlString] parameters:parameters];
    return request;
}

- (BOOL)checkImage:(UIImage *)image andUpdateMessage:(NSString *)message
{
    if (!message) {
        message = @"";
    }
    return (BOOL)image;
}

- (NSError *)generareErrorFromGrantedError:(NSError *)error
{
    if (!error) {
        error = [[NSError alloc] initWithDomain:@"Access denied" code:1 userInfo: nil];
    }
    return error;
}

- (NSError *)generateErrorFromStatusCode:(NSInteger )statusCode
{
    if (statusCode == 200) {
        return nil;
    } else {
        return [NSError errorWithDomain:[NSHTTPURLResponse localizedStringForStatusCode:statusCode] code:statusCode userInfo:nil];
    }
}

@end
