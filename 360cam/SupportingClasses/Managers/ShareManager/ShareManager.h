//
//  ShareManager.h
//  ShareTwitter
//
//  Created by Mike on 7/7/15.
//  Copyright (c) 2015 Mike. All rights reserved.
//



@protocol ShareManagerDelegate <NSObject>

@required
- (void) getErrorMessageFromGooglePlus:(NSError *)error;
@end


@interface ShareManager : NSObject

typedef void (^SharedCompletionBlock)(NSError *error);

@property(weak, nonatomic) id<ShareManagerDelegate> delegate;

+ (instancetype) sharedManager;
- (void)shareImage:(UIImage *)image onTwitterWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion;
- (void)shareImage:(UIImage *)image onFacebookWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion;
- (void)shareImageURL:(NSString *)imageURL onLinkedInWithMessage:(NSString *)message andCompletionBlock:(SharedCompletionBlock)completion;
- (void)shareImage:(UIImage *)image onGooglePlusWithMessage:(NSString *)message;
- (void)shareOnLinkedInMessage:(NSString *)message withCompletionBlock:(SharedCompletionBlock)completion;

@end
