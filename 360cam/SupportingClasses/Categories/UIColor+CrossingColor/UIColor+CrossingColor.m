//
//  UIColor+CrossingColor.m
//  360cam
//
//  Created by Kirill Gorbushko on 13.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIColor+CrossingColor.h"

@implementation UIColor (CrossingColor)

#pragma mark - Public

- (UIColor *)crossingToColor:(UIColor *)toColor percent:(float)percent
{
    float dec = percent / 100.f;
    CGFloat fRed, fBlue, fGreen, fAlpha;
    CGFloat tRed, tBlue, tGreen, tAlpha;
    CGFloat red, green, blue, alpha;
    
    if(CGColorGetNumberOfComponents(self.CGColor) == 2) {
        [self getWhite:&fRed alpha:&fAlpha];
        fGreen = fRed;
        fBlue = fRed;
    } else {
        [self getRed:&fRed green:&fGreen blue:&fBlue alpha:&fAlpha];
    }
    
    if(CGColorGetNumberOfComponents(toColor.CGColor) == 2) {
        [toColor getWhite:&tRed alpha:&tAlpha];
        tGreen = tRed;
        tBlue = tRed;
    } else {
        [toColor getRed:&tRed green:&tGreen blue:&tBlue alpha:&tAlpha];
    }
    
    red = (dec * (tRed - fRed)) + fRed;
    green = (dec * (tGreen - fGreen)) + fGreen;
    blue = (dec * (tBlue - fBlue)) + fBlue;
    alpha = (dec * (tAlpha - fAlpha)) + fAlpha;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
