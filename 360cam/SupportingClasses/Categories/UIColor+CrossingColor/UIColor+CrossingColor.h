//
//  UIColor+CrossingColor.h
//  360cam
//
//  Created by Kirill Gorbushko on 13.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface UIColor (CrossingColor)

- (UIColor *)crossingToColor:(UIColor *)toColor percent:(float)percent;

@end
