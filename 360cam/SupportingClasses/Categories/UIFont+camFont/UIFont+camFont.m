//
//  UIFont+camFont.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "UIFont+camFont.h"

@implementation UIFont (camFont)

#pragma mark - Public

#pragma mark - ZonaPro

+ (UIFont *)camFontZonaProRegularWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro" size:size];
}

+ (UIFont *)camFontZonaProBoldWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro-Bold" size:size];
}

+ (UIFont *)camFontZonaProLightWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro-Light" size:size];
}

+ (UIFont *)camFontZonaProThinWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro-Thin" size:size];
}

+ (UIFont *)camFontZonaProHairLineWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro-Hairline" size:size];
}

+ (UIFont *)camFontZonaProExtraBoldWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"ZonaPro-Black" size:size];
}

@end