//
//  UIFont+camFont.h
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

@interface UIFont (camFont)

#pragma mark - ZonaPro

+ (UIFont *)camFontZonaProThinWithSize:(CGFloat)size;
+ (UIFont *)camFontZonaProLightWithSize:(CGFloat)size;
+ (UIFont *)camFontZonaProBoldWithSize:(CGFloat)size;
+ (UIFont *)camFontZonaProRegularWithSize:(CGFloat)size;
+ (UIFont *)camFontZonaProHairLineWithSize:(CGFloat)size;
+ (UIFont *)camFontZonaProExtraBoldWithSize:(CGFloat)size;

@end
