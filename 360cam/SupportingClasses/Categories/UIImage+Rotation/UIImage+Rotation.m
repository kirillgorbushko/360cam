//
//  UIImage+Rotation.m
//  360cam
//
//  Created by Stas Volskyi on 7/15/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIImage+Rotation.h"

@implementation UIImage (Rotation)

- (UIImage *)rotateToAngel:(CGFloat)angel
{
    if (!angel) {
        return self;
    }
    CGRect boundingRect = [self getBoundingRectAfterRotation:CGRectMake(0, 0, self.size.width, self.size.height) byAngle:angel];
    boundingRect.size.width--;
    boundingRect.size.height--;
    
    CGAffineTransform imageTransform = CGAffineTransformIdentity;
    imageTransform = CGAffineTransformTranslate(imageTransform, boundingRect.size.width / 2.0, boundingRect.size.height / 2.0);
    imageTransform = CGAffineTransformRotate(imageTransform, angel);
    imageTransform = CGAffineTransformScale(imageTransform, 1.0, -1.0);
    
    UIGraphicsBeginImageContext(boundingRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, imageTransform);
    CGContextDrawImage(context, CGRectMake(-self.size.width / 2.0, -self.size.height / 2.0, self.size.width, self.size.height), self.CGImage);
    CGImageRef rotatedImageRef = CGBitmapContextCreateImage(context);
    UIImage *rotatedImage = [UIImage imageWithCGImage:rotatedImageRef];
    UIGraphicsEndImageContext();
    CGImageRelease(rotatedImageRef);
    return rotatedImage;
}

- (UIImage *)flippedImageHorizontally:(BOOL)horizontally
{
    CGAffineTransform imageTransform = CGAffineTransformIdentity;
    if (horizontally) {
        imageTransform = CGAffineTransformTranslate(imageTransform, 0, self.size.height);
        imageTransform = CGAffineTransformScale(imageTransform, 1, -1);
        imageTransform = CGAffineTransformTranslate(imageTransform, self.size.width, 0);
        imageTransform = CGAffineTransformScale(imageTransform, -1, 1);
    }
    
    UIGraphicsBeginImageContext(self.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, imageTransform);
    CGContextDrawImage(context, CGRectMake(0, 0, self.size.width, self.size.height), self.CGImage);
    CGImageRef flippedImageRef = CGBitmapContextCreateImage(context);
    UIImage *flippedImage = [UIImage imageWithCGImage:flippedImageRef];
    UIGraphicsEndImageContext();
    CGImageRelease(flippedImageRef);

    return flippedImage;
}

#pragma mark - Private helpers

- (CGRect)getBoundingRectAfterRotation:(CGRect)rectangle byAngle:(CGFloat)angleOfRotation
{
    CGFloat newWidth = rectangle.size.width * fabs(cosf(angleOfRotation)) + rectangle.size.height * fabs(sinf(angleOfRotation));
    CGFloat newHeight = rectangle.size.height * fabs(cosf(angleOfRotation)) + rectangle.size.width * fabs(sinf(angleOfRotation));
    CGFloat newX = rectangle.origin.x + ((rectangle.size.width - newWidth) / 2);
    CGFloat newY = rectangle.origin.y + ((rectangle.size.height - newHeight) / 2);
    
    return CGRectMake(newX, newY, newWidth, newHeight);
}

@end
