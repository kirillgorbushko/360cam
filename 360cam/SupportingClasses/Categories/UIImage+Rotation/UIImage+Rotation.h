//
//  UIImage+Rotation.h
//  360cam
//
//  Created by Stas Volskyi on 7/15/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

/** Angel in radians */
- (UIImage *)rotateToAngel:(CGFloat)angel;
/** Horizontally: YES - horizontal flip, NO - vertical flip*/
- (UIImage *)flippedImageHorizontally:(BOOL)horizontally;

@end
