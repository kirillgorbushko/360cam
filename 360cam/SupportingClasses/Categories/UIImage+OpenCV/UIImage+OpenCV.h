 //
//  UIImage+OpenCV.h
//  360cam
//
//  Created by Kirill Gorbushko on 05.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LittlePlanet.h"

@interface UIImage (OpenCV)

+ (cv::Mat)cvMatFromUIImage:(UIImage *)image;
+ (UIImage *)UIImageFromCVMat:(cv::Mat)cvMat;

@end
