//
//  UIImage+Metadata.h
//  360cam
//
//  Created by Kirill Gorbushko on 03.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//
#import <ImageIO/ImageIO.h>

@interface UIImage (Metadata)

- (NSDictionary *)metadata;

- (NSString *)dateFromExifMetadata:(NSDictionary *)commonMetadata;
- (NSString *)ownerNameFromExifMetadata:(NSDictionary *)commonMetadata;
- (NSDictionary *)locationDataFromMetadata:(NSDictionary *)commonMetadata;

@end
