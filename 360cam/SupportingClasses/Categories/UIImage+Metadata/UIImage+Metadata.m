//
//  UIImage+Metadata.m
//  360cam
//
//  Created by Kirill Gorbushko on 03.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIImage+Metadata.h"

@implementation UIImage (Metadata)

- (NSDictionary *)metadata
{    
    NSData *imgData = UIImageJPEGRepresentation(self, 1.0);
    
    CGImageSourceRef mySourceRef = CGImageSourceCreateWithData((CFDataRef)imgData, NULL);
    NSDictionary *allMetaData;
    if (mySourceRef != NULL) {
        allMetaData = (__bridge NSDictionary *)CGImageSourceCopyPropertiesAtIndex(mySourceRef, 0, NULL);
    }
    return allMetaData;
}

- (NSString *)dateFromExifMetadata:(NSDictionary *)commonMetadata
{
    NSDictionary *exifData = [commonMetadata objectForKey:(NSString *)kCGImagePropertyExifDictionary];
    NSString *EXIFDate = [exifData objectForKey:(NSString*)kCGImagePropertyExifDateTimeOriginal];

    return EXIFDate;
}

- (NSString *)ownerNameFromExifMetadata:(NSDictionary *)commonMetadata
{
    NSDictionary *tiffDic = [commonMetadata objectForKey:(NSString *)kCGImagePropertyTIFFDictionary];
    NSString *EXIFOwnerName = [tiffDic objectForKey:(NSString*)kCGImagePropertyTIFFArtist];
    return EXIFOwnerName;
}

- (NSDictionary *)locationDataFromMetadata:(NSDictionary *)commonMetadata
{
    NSDictionary *locationDictionary = [commonMetadata objectForKey:(NSString *)kCGImagePropertyGPSDictionary];
    return locationDictionary;
}


@end