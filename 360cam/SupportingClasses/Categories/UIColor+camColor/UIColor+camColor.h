//
//  UIColor+camColor.h
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

@interface UIColor (camColor)

#pragma mark - CameraHomeColors

+ (UIColor *)lightGrayBorderColor;
+ (UIColor *)lightBlueBorderColor;
+ (UIColor *)pinkRecordColor;

#pragma mark - HomeScreen

+ (UIColor *)darkGrayForHomePageWithAlpha:(CGFloat)alpha;

#pragma mark - DetailSettings

+ (UIColor *)grayForDetailSettingsNavigationBar;

#pragma mark - BestOff screen / PlaybackScreen

+ (UIColor *)greyBorderColor;
+ (UIColor *)lightBlueButtonBackgroundColor;
+ (UIColor *)yellowButtonBackgroundColor;

#pragma mark AddDeviceScreen

+ (UIColor *)grayColorForText;
+ (UIColor *)grayColorForIndicatorBorder;
+ (UIColor *)darkGrayColorForText;

#pragma mark - Choose Camera Screen

+ (UIColor *)chooseCamDarkNavBar;
+ (UIColor *)chooseCamLightBlue;
+ (UIColor *)chooseCamDarkBlack;
+ (UIColor *)chooseCamLightBlack;

#pragma mark - ViewMode screen

+ (UIColor *) lightBlueViewModeColor;

#pragma mark - Article My Gallerie Screen

+ (UIColor *)colorGalleryLightBlue;
+ (UIColor *)colorGallerySilverTextColor;
+ (UIColor *)colorGalleryBlackTextColor;
+ (UIColor *)colorGalleryToolbarTextColor;
+ (UIColor *)colorGalleryToolbarUnactiveTextColor;
+ (UIColor *)colorGalleryDescriptionText;

#pragma mark - Player

+ (UIColor *)blackTransparentColor;

#pragma mark - Detail Share/Import

+ (UIColor *)colorLightBlueShareImport;
+ (UIColor *)lightGrayShareImport;
+ (UIColor *)mailShareImport;
+ (UIColor *)facebookGrayShareImport;
+ (UIColor *)twitterShareImport;
+ (UIColor *)linkedinShareImport;
+ (UIColor *)googlePlusShareImport;

#pragma mark - NotificationScreen

+ (UIColor *)lightGrayColorForNotificationImageBorder;
+ (UIColor *)lightGrayColorForNotificationText;

@end
