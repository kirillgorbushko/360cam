//
//  UIColor+camColor.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "UIColor+camColor.h"

@implementation UIColor (camColor)

#pragma mark - CameraHomeColors

+ (UIColor *)lightGrayBorderColor
{
    return [UIColor colorWithRealRed:38 green:38 blue:38 alpha:1.];
}

+ (UIColor *)lightBlueBorderColor
{
    return [UIColor colorWithRealRed:79 green:185 blue:255 alpha:1.];
}

+ (UIColor *)pinkRecordColor
{
    return [UIColor colorWithRealRed:234 green:23 blue:56 alpha:1.];
}

#pragma mark - HomeScreen

+ (UIColor *)darkGrayForHomePageWithAlpha:(CGFloat)alpha
{
    return [UIColor colorWithRealRed:70 green:70 blue:70 alpha:alpha];
}

#pragma mark - DetailSettings

+ (UIColor *)grayForDetailSettingsNavigationBar
{
    return [UIColor colorWithRealRed:74 green:75 blue:76 alpha:1.f];
}

#pragma mark - BestOfScreen / PlaybackScreen

+ (UIColor *)greyBorderColor
{
    return [UIColor colorWithRealRed:164 green:202 blue:238 alpha:1.f];
}

+ (UIColor *)lightBlueButtonBackgroundColor
{
    return [UIColor colorWithRealRed:68 green:187 blue:255 alpha:1.f];
}
#pragma mark - ViewMode screen

+ (UIColor *)lightBlueViewModeColor
{
    return [UIColor colorWithRealRed:60 green:183 blue:249 alpha:1.0f];
}

+ (UIColor *)yellowButtonBackgroundColor
{
    return [UIColor colorWithRealRed:251 green:67 blue:3 alpha:1.f];
}

#pragma mark - AddNewDeviceScreen

+ (UIColor *)grayColorForText
{
    return [UIColor colorWithRealRed:219 green:219 blue:219 alpha:1.f];
}

+ (UIColor *)darkGrayColorForText
{
    return [UIColor colorWithRealRed:88 green:88 blue:88 alpha:1.f];
}

+ (UIColor *)grayColorForIndicatorBorder
{
    return [UIColor colorWithRealRed:243 green:243 blue:243 alpha:1.f];
}

#pragma mark - Article My Gallerie Screen

+ (UIColor *)colorGalleryLightBlue
{
    return [UIColor colorWithRealRed:68 green:187 blue:255 alpha:1.f];
}

+ (UIColor *)colorGallerySilverTextColor
{
    return [UIColor colorWithRealRed:101 green:107 blue:110 alpha:1.f];
}

+ (UIColor *)colorGalleryBlackTextColor
{
    return [UIColor colorWithRealRed:23 green:22 blue:22 alpha:1.f];
}

+ (UIColor *)colorGalleryToolbarTextColor
{
    return [UIColor colorWithRealRed:149 green:149 blue:149 alpha:1.f];
}

+ (UIColor *)colorGalleryToolbarUnactiveTextColor
{
    return [UIColor colorWithRealRed:215 green:215 blue:215 alpha:1.f];
}

+ (UIColor *)colorGalleryDescriptionText
{
    return [UIColor colorWithRealRed:119 green:119 blue:119 alpha:1];
}

#pragma mark - Player

+ (UIColor *)blackTransparentColor
{
    return [UIColor colorWithRealRed:0 green:0 blue:0 alpha:0.6f];
}

#pragma mark - Choose Camera Screen

+ (UIColor *)chooseCamDarkNavBar
{
    return [UIColor colorWithRealRed:75.f green:75.f blue:75.f alpha:1.f];
}

+ (UIColor *)chooseCamLightBlue
{
    return [UIColor colorWithRealRed:68.f green:187.f blue:255.f alpha:1.f];
}

+ (UIColor *)chooseCamDarkBlack
{
    return [UIColor colorWithRealRed:74.f green:74.f blue:74.f alpha:1.f];
}

+ (UIColor *)chooseCamLightBlack
{
    return [UIColor colorWithRealRed:101.f green:107.f blue:111.f alpha:1.f];
}

#pragma mark - Detail Share/Import

+ (UIColor *)colorLightBlueShareImport
{
    return [UIColor colorWithRed:0.314f green:0.631f blue:0.906f alpha:1.00f];
}

+ (UIColor *)lightGrayShareImport
{
    return [UIColor colorWithRealRed:74 green:74 blue:72 alpha:1.0f];
}

+ (UIColor *)mailShareImport
{
    return [UIColor colorWithRealRed:74 green:74 blue:72 alpha:1.0f];
}

+ (UIColor *)facebookGrayShareImport
{
    return [UIColor colorWithRealRed:68 green:95 blue:152 alpha:1.0f];
}

+ (UIColor *)twitterShareImport
{
    return [UIColor colorWithRealRed:88 green:156 blue:233 alpha:1.0f];
}

+ (UIColor *)linkedinShareImport
{
    return [UIColor colorWithRealRed:51 green:126 blue:185 alpha:1.0f];
}

+ (UIColor *)googlePlusShareImport
{
    return [UIColor colorWithRealRed:172 green:43 blue:13 alpha:1.0f];
}

#pragma mark - NotificationScreen

+ (UIColor *)lightGrayColorForNotificationImageBorder
{
    return [UIColor colorWithRealRed:237 green:237 blue:237 alpha:1.f];
}

+ (UIColor *)lightGrayColorForNotificationText
{
    return [UIColor colorWithRealRed:119 green:119 blue:119 alpha:1.f];
}

#pragma mark - Private

+ (UIColor *)colorWithRealRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(float)alpha
{
    return [UIColor colorWithRed:((float)red)/255.f green:((float)green)/255.f blue:((float)blue)/255.f alpha:((float)alpha)/1.f];
}

@end
