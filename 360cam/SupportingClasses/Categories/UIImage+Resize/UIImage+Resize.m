//
//  UIImage+Resize.m
//  360cam
//
//  Created by Stas Volskyi on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (Resize)

- (UIImage *)fullScreenImage
{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    CGFloat scale = [[UIScreen mainScreen] respondsToSelector:@selector(nativeScale)] ? [UIScreen mainScreen].nativeScale : 2.0f;
    CGFloat screenDismension = MIN(CGRectGetWidth(screenBounds), CGRectGetHeight(screenBounds)) * scale;
    CGFloat imageDismension = MIN(self.size.width, self.size.height);
    CGFloat coef = screenDismension / imageDismension;
    if (coef >= 1) {
        return self;
    }
    CGSize newSize = CGSizeMake(self.size.width * coef, self.size.height * coef);
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *fullScreenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return fullScreenImage;
}

@end
