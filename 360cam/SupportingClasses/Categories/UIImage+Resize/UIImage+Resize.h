//
//  UIImage+Resize.h
//  360cam
//
//  Created by Stas Volskyi on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)fullScreenImage;

@end
