
//  Created by Kirill on 9/27/14.
//  Copyright (c) 2014 Thinkmobiles. All rights reserved.
//

#import "RoundButton.h"

@implementation RoundButton

#pragma mark - TouchAction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nextResponder touchesBegan:touches withEvent:event];
    
    if ([RoundButton distanceFromCenterView:self fromTouches:touches withEvent:event] < self.frame.size.width / 2) {
        [self setHighlighted:YES];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nextResponder touchesEnded:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nextResponder touchesMoved:touches withEvent:event];
}

#pragma mark - Private

+ (CGFloat)distanceBetweenTwoPoints:(CGPoint)point1 toPoint:(CGPoint)point2
{
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx * dx + dy * dy );
}

+ (CGFloat)distanceFromCenterView:(UIButton *)buttonView fromTouches:(NSSet *)touches withEvent:(UIEvent *)event
{
    touches = [event touchesForView:buttonView];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:buttonView];
    CGPoint center = CGPointMake(buttonView.bounds.size.height / 2, buttonView.bounds.size.width / 2);
    CGFloat distance = [RoundButton distanceBetweenTwoPoints:center toPoint:touchPoint];
    return distance;
}

@end
