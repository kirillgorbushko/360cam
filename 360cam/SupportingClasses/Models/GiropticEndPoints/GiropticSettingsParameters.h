//
//  GiropticSettingsParameters.h
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 19.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

static NSString *const GiroCurrentSessionID = @"giroCurrentSessionID";

#pragma mark - Setters

/**
 List of shutter speeds currently available, e.g. [1/15, 1/30, 1/60, …...] or [] when it is in “auto” mode.
 Parameter type - String
 */
static NSString *const GiroOptionKeyCaptureMode = @"captureMode";
/**
 List of shutter speeds currently available, e.g. [1/15, 1/30, 1/60, …...] or [] when it is in “auto” mode.
 Parameter type - Number
 */
static NSString *const GiroOptionKeyShutterSpeed = @"shutterSpeed";
/**
 List of white balance settings currently available, which can be a subset of our pre-defined list:
 [ “_auto_wide”, “_auto_normal”, “_sunny”, “_shadow”, “_indoor”]
 Parameter type - String
 */
static NSString *const GiroOptionKeyWhiteBalance = @"whiteBalance";
/**
 List of exposure delays currently available, in seconds, e.g. [0, 1, 2, 5, 10, 30, 60]
 Parameter type - Number
 */
static NSString *const GiroOptionKeyExplosureDelay = @"exposureDelay";
/**
 Current image type and resolution, e.g.
 {
 “type”: “jpeg”,
 “width”: 4096,
 “height”: 2048
 }
 Parameter type - Object
 */
static NSString *const GiroOptionKeyFileFormat = @"fileFormat";
/**
 Turn HDR capture mode current status on/off. Value must be ‘true’ or ‘false’. Default value is ‘false’. It can be set to true only when "hdrSupport" is ‘true’.
 Parameter type - Boolean
 */
static NSString *const GiroOptionKeyHDR = @"hdr";
/**
 Current exposure bracket setting, with two entries, "shots", an integer containing the number of shots to be taken, and "increment", a number containing f-stop increment between shots. Relevant if "hdr" option is enabled.
 For example,
 {
 "shots": 3,
 "increment": [0, -4, 4]
 }
 Default to empty "{}" when"hdr" is false.
 Parameter type - Object
 */
static NSString *const GiroOptionKeyExposureBracket = @"exposureBracket";
/**
 Current interval between two consecutive capturing pictures on “_timelapse” captureMode in seconds
 Parameter type - Number
 */
static NSString *const GiroOptionKeyTimeLapsInterval = @"_timelapseInterval";
/**
 List of “_burst” mode duration delays in seconds.
 [5, 10, 15, 30]
 Parameter type - Number
 */
static NSString *const GiroOptionKeyBurstDuration = @"_burstDuration";
/**
 Min an max encoding bitrate supported by the camera in bit/s
 
 {
 "min": 500000,
 "max”: 20000000
 }
 Parameter type - Object
 */
static NSString *const GiroOptionKeyVideoBitrate = @"_videoBitrate";
/**
 Min an max encoding bitrate supported by the camera in bit/s
 
 {
 "min": 5,
 "max”: 30
 }
 Parameter type - Number
 */
static NSString *const GiroOptionKeyVideoFrameRate = @"_videoFramerate";
/**
 Turn camera Gyroscope module on/off. Value must be ‘true’ or ‘false’. Default value is ‘true’ It can be set to true only when "gyroSupport" is ‘true’.
 Parameter type - Boolean
 */
static NSString *const GiroOptionKeyGYRO = @"gyro";
/**
 Turn camera GPS module on/off. Value must be ‘true’ or ‘false’. It can be set to true only when "gpsSupport" is ‘true’.
 Parameter type - Boolean
 */
static NSString *const GiroOptionKeyGPS = @"gps";
/**
 Image stabilization operations currently available. Our pre-defined list is :
 [ "off", "_horizontal_stabilization", "_vibration_correction" ].
 Parameter type - String
 */
static NSString *const GiroOptionKeyImageStabilisation = @"imageStabilization";
/**
 Current date and time information. May be set by setOptions using phone’s date and time. The format is "YYYY:MM:DD HH:MM:SS" with time shown in 24-hour format, and the date and time separated by one blank character.
 Parameter type - Staring
 */
static NSString *const GiroOptionKeyDateTimeZone = @"dateTimeZone";
/**
 List of exposure modes currently available “ExposureMode”.
 0 = Auto exposure
 1 = Manual exposure
 Parameter type - NSNumber
 */
static NSString *const GiroOptionKeyExplosureMode = @"exposureMode";

#pragma mark - Getters

/**
 READONLY
 Value must be ‘true’ or ‘false’ depending on if the camera supports this.
 */
static NSString *const GiroOptionKeyHDRSupport = @"hdrSupport";
/**
 READONLY
 Number of bytes of total storage. Cannot be set.
 */
static NSString *const GiroOptionKeyTotalSpace = @"totalSpace";
/**
 READONLY
 Number of bytes of total storage. Cannot be set.
 */
static NSString *const GiroOptionKeyRemainingSpace = @"remainingSpace";
/**
 READONLY
 Estimated number of remaining pictures based on current capture setting. Cannot be set.
 */
static NSString *const GiroOptionKeyRemaingPictures = @"remainingPictures";
/**
 READONLY
 Expressed as a percentage of the battery remaining.
 */
static NSString *const GiroOptionKeyButteryLevel = @"batteryLevel";
/**
 READONLY
 Exposure bracket settings currently available, e.g.
 {
 "shotsSupport": [1, 3, 5, 7],
 "incrementSupport": [-2, -1.67, -1.33, -1, -0.67, -0.33, 0, ……, 2]
 }
 
 Default to empty "{}" if "hdr" is false.
 */
static NSString *const GiroOptionKeyExposureBracketSupport = @"exposureBracketSupport";
/**
 READONLY
 List of exposure delays currently available, in seconds, e.g. [0, 1, 2, 5, 10, 30, 60]
 */
static NSString *const GiroOptionKeyExplosureDelaySupport = @"exposureDelaySupport";
/**
 READONLY
 List of the file formats currently available, e.g.
 [
 {
 “type”: “mp4”,
 “width”: 2048,
 “height”: 1024
 },
 ……
 {
 “type”: “jpeg”,
 “width”: 4096,
 “height”: 2048
 }
 ]
 */
static NSString *const GiroOptionKeyFileFormatSupport = @"fileFormatSupport";
/**
 READONLY
 List of white balance settings currently available, which can be a subset of our pre-defined list:
 [ “_auto_wide”, “_auto_normal”, “_sunny”, “_shadow”, “_indoor”]
 */
static NSString *const GiroOptionKeyWhiteBalanceSupport = @"whiteBalanceSupport";
/**
 READONLY
 List of shutter speeds currently available, e.g. [1/15, 1/30, 1/60, …...] or [] when it is in “auto” mode.
 */
static NSString *const GiroOptionKeyShutterSpeedSupport = @"shutterSpeedSupport";
/**
 READONLY
 List of shutter speeds currently available, e.g. [1/15, 1/30, 1/60, …...] or [] when it is in “auto” mode.
 */
static NSString *const GiroOptionKeyCaptureModeSupport = @"captureMode";
/**
 READONLY
 List of exposure modes currently available “ExposureMode”.
 0 = Auto exposure
 1 = Manual exposure
 */
static NSString *const GiroOptionKeyExplosureModeSupport = @"exposureModeSupport";
/**
 READONLY
 Current interval between two consecutive capturing pictures on “_timelapse” captureMode in seconds
 */
static NSString *const GiroOptionKeyTimeLapsIntervalSupport = @"_timelapseIntervalSupport";
/**
 READONLY
 List of “_burst” mode duration delays in seconds.
 [5, 10, 15, 30]
 */
static NSString *const GiroOptionKeyBurstDurationSupport = @"_burstDurationSupport";
/**
 READONLY
 Min an max encoding bitrate supported by the camera in bit/s
 
 {
 "min": 500000,
 "max”: 20000000
 }
 */
static NSString *const GiroOptionKeyVideoBitrateSupport = @"_videoBitrateSupport";
/**
 READONLY
 Min an max encoding bitrate supported by the camera in bit/s
 
 {
 "min": 5,
 "max”: 30
 }
 */
static NSString *const GiroOptionKeyVideoFrameRateSupport = @"_videoFramerateSupport";
/**
 READONLY
 Image stabilization operations currently available. Our pre-defined list is :
 [ "off", "_horizontal_stabilization", "_vibration_correction" ].
 */
static NSString *const GiroOptionKeyImageStabilisationSupport = @"imageStabilizationSupport";

