//
//  Network.m
//  360cam
//
//  Created by Mike on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Network.h"
#import "Camera.h"


@implementation Network

@dynamic cameraId;
@dynamic networkId;
@dynamic networkType;
@dynamic ssId;
@dynamic networkCamera;

@end
