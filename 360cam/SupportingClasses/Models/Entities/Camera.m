//
//  Camera.m
//  360cam
//
//  Created by Mike on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "Camera.h"
#import "Network.h"


@implementation Camera

@dynamic addedDate;
@dynamic cameraId;
@dynamic modelType;
@dynamic name;
@dynamic cameraNetwork;

@end
