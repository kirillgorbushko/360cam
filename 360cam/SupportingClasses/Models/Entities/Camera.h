//
//  Camera.h
//  360cam
//
//  Created by Mike on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Network;

@interface Camera : NSManagedObject

@property (nonatomic, retain) NSDate * addedDate;
@property (nonatomic, retain) NSString * cameraId;
@property (nonatomic, retain) NSString * modelType;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Network *cameraNetwork;

@end
