//
//  Network.h
//  360cam
//
//  Created by Mike on 7/14/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Camera;

@interface Network : NSManagedObject

@property (nonatomic, retain) NSString * cameraId;
@property (nonatomic, retain) NSString * networkId;
@property (nonatomic, retain) NSString * networkType;
@property (nonatomic, retain) NSString * ssId;
@property (nonatomic, retain) Camera *networkCamera;

@end
