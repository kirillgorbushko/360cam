//
//  RemoteCamaraHomeViewController.h
//  
//
//  Created by Kirill Gorbushko on 25.06.15.
//
//
#import "CameraNavigationController.h"

@interface RemoteCameraHomeViewController : UIViewController <CameraNavigationControllerDelegate>

@property (copy, nonatomic) NSString *sessionId;

@end
