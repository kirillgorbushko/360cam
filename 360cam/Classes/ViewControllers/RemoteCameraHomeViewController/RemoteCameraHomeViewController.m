//
//  RemoteCamaraHomeViewController.m
//
//
//  Created by Kirill Gorbushko on 25.06.15.
//
//

// Controllers
#import "RemoteCameraHomeViewController.h"
#import "RemoteCameraSDGalleryViewController.h"
#import "CameraSettingsViewController.h"
#import "RootViewController.h"
#import "AppHelper.h"
#import "UIFont+camFont.h"
#import "PhotoPlayerViewController.h"

// Categories
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"

// Managers
#import "CameraNetworkManager.h"
#import "WiFiFetchService.h"

// Models
#import "RoundButton.h"
#import "Animation.h"
#import "GiropticEndPoints.h"

typedef NS_ENUM(NSUInteger, CameraSignalLevel) {
    CameraSignalMinimum,
    CameraSignalMedium,
    CameraSignalMaximum,
};

typedef NS_ENUM(NSUInteger, CameraBatteryLevel) {
    CameraBatteryLevelZero,
    CameraBatteryLevelMinimum,
    CameraBatteryLevelLow,
    CameraBatteryLevelMedium,
    CameraBatteryLevelMaximum
};

typedef NS_ENUM(NSUInteger, CameraControlState) {
    CameraControlStateNone,
    CameraControlStatePlay,
    CameraControlStatePause
};

static NSString *const DetailsButtonImageName = @"ic_blocks";
static NSString *const BrakerButtonImageName = @"ic_power_active";

static NSString *const ActiveVideoButtonImageName = @"ic_video_active";
static NSString *const ActivePhotoButtonImageName = @"ic_photo_active";
static NSString *const ActiveBurstButtonImageName = @"ic_burst_active";
static NSString *const ActiveTimeLapseButtonImageName = @"ic_timelapse_active";

@interface RemoteCameraHomeViewController()

@property (weak, nonatomic) IBOutlet UIView *topInformationView;
@property (weak, nonatomic) IBOutlet UIView *bottomInformationView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *roundButtonContainerView;
@property (weak, nonatomic) IBOutlet UIView *modeSelectionView;
@property (weak, nonatomic) IBOutlet UIView *controlElementsView;
@property (weak, nonatomic) IBOutlet UIView *resultPreviewView;
@property (weak, nonatomic) IBOutlet UIView *indicatorView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widtButtonRoundConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightButtonRoundConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *signalImageView;
@property (weak, nonatomic) IBOutlet UIImageView *batteryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *resultPreviewThumbnailsImageView;

@property (weak, nonatomic) IBOutlet UILabel *videoDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultPreviewAssetNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultPreviewAssetDurationLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeSelectionViewVerticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentVIewVerticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomVerticalSpace;

@property (weak, nonatomic) IBOutlet UIButton *settingButton;
@property (weak, nonatomic) IBOutlet UIButton *selectedModeButton;
@property (weak, nonatomic) IBOutlet RoundButton *actionButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYRoundButtonsConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topYConstraint;

@property (strong, nonatomic) UIView *takePictureTimerView;
@property (strong, nonatomic) UILabel *takePictureTimerLabel;
@property (assign, nonatomic) NSInteger takePictureTimerValue;

@property (strong, nonatomic) NSTimer *internalTimer;
@property (strong, nonatomic) NSTimer *batteryTimer;
@property (strong, nonatomic) NSTimer *photoCountingTimer;
@property (strong, nonatomic) NSTimer *previewPhotoTimer;

@property (strong, nonatomic) NSDate *startDate;
@property (assign, nonatomic) BOOL isModeSelectionViewShown;
@property (assign, nonatomic) NSInteger currentMode;

@property (assign, nonatomic) BOOL videOrTimelapseStarted;
@property (assign, nonatomic) NSInteger explosureDelay;
@property (strong, nonatomic) NSString *filePath;
@end

@implementation RemoteCameraHomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setBorders];
    [self prepareNavigationBarButtons];
    [self setStartParameters];
    [self initTakePictureTimerView];
    [self updateUIIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController presentTransparentNavigationBarAnimated:YES];
    
    [self addWiFiObservers];
    [self startWifiSignalAnimation];
    [self initSelectedModeButton];
    [self startBatteryTimer];
    [self pauseButtonDesign:NO];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yy";
    self.dateLabel.text = [formatter stringFromDate:[NSDate date]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getCurrentOptionFromCamera];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.batteryTimer invalidate];
    self.batteryTimer = nil;
    [self removeAllObservers];
    [self stopVideoRecordTimer];
    [self.signalImageView stopAnimating];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self setInsentsForButtons];
}

#pragma mark - IBActions

- (void)detailsButonPressed
{
    RemoteCameraSDGalleryViewController *sdCardVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RemoteCameraSDGalleryViewController"];
    [self.navigationController pushViewController:sdCardVC animated:YES];
}

- (void)brakerButtonPress
{
    
}

- (IBAction)selectModeButtonPressed:(id)sender
{
    if (self.videOrTimelapseStarted) {
        return;
    }
    if (self.isModeSelectionViewShown) {
        [self shouldShowModeSelectionView:NO];
    } else {
        [self shouldShowModeSelectionView:YES];
    }
    self.isModeSelectionViewShown = !self.isModeSelectionViewShown;
}

- (IBAction)settingsButtonPressed:(id)sender
{
    CameraSettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    [self.navigationController pushViewController:settingsVC animated:YES];
}

- (IBAction)videoModeSelectionButtonPress:(id)sender
{
    [self modeButtonPress:sender];
}

- (IBAction)photoModeSelectionButtonPress:(id)sender
{
    [self modeButtonPress:sender];
}

- (IBAction)burstModeSelectionButtonPress:(id)sender
{
    [self modeButtonPress:sender];
}

- (IBAction)timeLapseModeSelectionButtonPress:(id)sender
{
    [self modeButtonPress:sender];
}

- (void)performAction:(UIButton *)sender
{
    
    if (self.currentMode == GiropticCameraModeVideo || self.currentMode == GiropticCameraModeTimeLaps) {
        if (self.videOrTimelapseStarted) {
            [self stopVideoOrTimelapse];
        } else {
            [self startVideoOrTimelapse];
        }
    } else if (self.currentMode == GiropticCameraModeBurst) {
        [self createBrust];
    } else if (self.currentMode == GiropticCameraModeImage) {
        [self createPhoto];
    }
}

- (IBAction)viewAssetButtonPressed:(id)sender
{
    
}

- (IBAction)viewAssetDetailsButtonPressed:(id)sender
{
    [AppHelper showLoaderWithText:@"Preparing image"];
    __weak typeof(self) weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetImage:self.filePath imageWidth:0 imageHeight:0 operationResult:^(BOOL success, id response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [AppHelper hideLoader];
            if (success) {
                PhotoPlayerViewController *photoController = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"photoPlayer"];
                photoController.sourceImage = response;
                [weakSelf.navigationController pushViewController:photoController animated:YES];
            } else {
                NSLog(@"cant get file - %@", self.filePath);
            }
        });
    }];
}

#pragma mark - RoundedButtonClick

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([RoundButton distanceFromCenterView:self.actionButton fromTouches:touches withEvent:event] < self.actionButton.frame.size.width / 2) {
        [self.actionButton setHighlighted:NO];
        [self performSelector:@selector(performAction:) withObject:self.actionButton];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([RoundButton distanceFromCenterView:self.actionButton fromTouches:touches withEvent:event] < self.actionButton.frame.size.width / 2) {
        [self.actionButton setHighlighted:NO];
        [self performSelector:@selector(performAction:) withObject:self.actionButton];
    }
}

#pragma mark - Animation

- (void)startWifiSignalAnimation
{
    self.signalImageView.animationImages = @[[UIImage imageNamed:@"ic_signal_3"], [UIImage imageNamed:@"ic_signal_2"]];
    self.signalImageView.animationDuration = 20 + arc4random_uniform(20);
    self.signalImageView.animationRepeatCount = MAXFLOAT;
    [self.signalImageView startAnimating];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.contentView.layer animationForKey:@"moveContentView"]) {
        [self.contentView.layer removeAllAnimations];
    }
    if (anim == [self.modeSelectionView.layer animationForKey:@"moveModeSelectionView"]) {
        [self.modeSelectionView.layer removeAllAnimations];
    }
    if (anim == [self.resultPreviewView.layer animationForKey:@"hidePreview"]) {
        [self.resultPreviewView.layer removeAllAnimations];
        self.resultPreviewView.hidden = YES;
    }
}

#pragma mark - CameraNavigationControllerDelegate

- (void)menuWillShown
{
    if (self.contentViewBottomVerticalSpace.constant) {
        [self shouldShowModeSelectionView:NO];
        self.isModeSelectionViewShown = NO;
    }
}

#pragma mark - Observer

- (void)addWiFiObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraDisconnected) name:DeviceDidDisconnectedToCameraNotification object:nil];
}

- (void)removeAllObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)isCameraConnected
{
    CameraNetworkManager *shaerdManager = [CameraNetworkManager sharedManager];
    if (![shaerdManager isCameraConnected]) {
        [self cameraDisconnected];
    }
}

- (void)cameraDisconnected
{
    [AppHelper alertViewWithMessage:@"Camera disconnected"];
    if (self.navigationController.viewControllers.count == 1) {
        [[AppHelper rootViewControllerSideMenu] navigateToViewControllerAtIndex:3];//choseViewController
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Photo

- (void)startPhotoPreivewTimer
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.previewPhotoTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(stopTimer) userInfo:nil repeats:NO];
    });
}

- (void)stopTimer
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.previewPhotoTimer invalidate];
        weakSelf.previewPhotoTimer = nil;
        [weakSelf showHidePreview];
    });
}

- (void)createPhoto
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    [self startCountingPhotoViewTimer];
    __weak typeof(self) weakSelf = self;
    __block CGFloat screenScale = 2;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(nativeScale)]) {
        screenScale = [UIScreen mainScreen].nativeScale;
    }
    [manager giropticTakePicture:^(BOOL success, id response, NSError *error) {
        if (success) {
            NSString *fileUri = [[response valueForKey:@"results"] valueForKey:@"fileUri"];
            self.filePath = fileUri;
            [manager giropticGetImagePreview:fileUri imageWidth:90 * screenScale   imageHeight:50 * screenScale operationResult:^(BOOL success, id response, NSError *error) {
                if (success) {
                    if ([[response valueForKey:@"type"] isEqualToString:@"base64"]) {
                        NSData *data =[[NSData alloc] initWithBase64EncodedString:[response valueForKey:@"data"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            [weakSelf setPreviewAssetName:[fileUri lastPathComponent] assetDuration:@"" assetThumbnail:image];
                            [weakSelf showHidePreview];
                            [weakSelf startPhotoPreivewTimer];
                        }
                    } else {
                        NSLog(@"error: %@", error);
                    }
                }
            }];
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
    }];
}

#pragma mark - TakePictureAnimation

- (void)initTakePictureTimerView
{
    self.takePictureTimerView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.takePictureTimerView.backgroundColor = [UIColor clearColor];
    self.takePictureTimerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 120.0)];
    self.takePictureTimerLabel.font = [UIFont camFontZonaProBoldWithSize:100.0];
    self.takePictureTimerLabel.textColor = [UIColor whiteColor];
    self.takePictureTimerLabel.textAlignment = NSTextAlignmentCenter;
    [self.takePictureTimerView addSubview:self.takePictureTimerLabel];
    self.takePictureTimerLabel.center = self.takePictureTimerView.center;
    [self.view addSubview:self.takePictureTimerView];
    self.takePictureTimerView.hidden = YES;
}

- (void)startTakePictureTimerAnimationWithTime:(NSNumber *)time
{
    self.takePictureTimerView.hidden = NO;
    self.takePictureTimerLabel.text = [NSString stringWithFormat:@"%li", [time integerValue]];
    self.takePictureTimerValue = [time integerValue];
    [self animateTakePictureLabel];
}

- (void)stopTakePictureTimerAnimation
{
    self.takePictureTimerView.hidden = YES;
    [self.photoCountingTimer  invalidate];
    self.photoCountingTimer = nil;
}

- (void)startCountingPhotoViewTimer
{
    [self performSelector:@selector(startTakePictureTimerAnimationWithTime:) withObject:@(self.explosureDelay) afterDelay:1.0f];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.photoCountingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTakePictureTimerLabel) userInfo:nil repeats:YES];
    });
}

- (void)updateTakePictureTimerLabel
{
    if (self.takePictureTimerValue > 1) {
        self.takePictureTimerValue --;
        self.takePictureTimerLabel.text = [NSString stringWithFormat:@"%li", self.takePictureTimerValue];
        [self animateTakePictureLabel];
    } else if (self.takePictureTimerValue == 1) {
        self.takePictureTimerValue --;
        self.takePictureTimerLabel.text = @"😀";
        [self animateTakePictureLabel];
    } else {
        [self stopTakePictureTimerAnimation];
    }
}

- (void)animateTakePictureLabel
{
    CABasicAnimation *fadeAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnim.fromValue = @0;
    fadeAnim.toValue = @0.8;
    
    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnim.fromValue = @0;
    scaleAnim.toValue = @1;
    
    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    animGroup.duration = 0.9;
    animGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animGroup.animations = @[fadeAnim, scaleAnim];
    animGroup.removedOnCompletion = YES;
    [self.takePictureTimerLabel.layer addAnimation:animGroup forKey:nil];
}

#pragma mark - Video

#pragma mark - Camera options

- (void)getCurrentOptionFromCamera
{
    NSArray *requestedOptions = @[
                                  GiroOptionKeyCaptureMode,
                                  GiroOptionKeyGPS,
                                  GiroOptionKeyGYRO,
                                  GiroOptionKeyHDR,
                                  GiroOptionKeyRemainingSpace,
                                  GiroOptionKeyTimeLapsInterval,
                                  GiroOptionKeyVideoBitrate,
                                  GiroOptionKeyVideoFrameRate,
                                  GiroOptionKeyWhiteBalance,
                                  GiroOptionKeyTotalSpace,
                                  GiroOptionKeyFileFormat,
                                  GiroOptionKeyButteryLevel,
                                  GiroOptionKeyBurstDuration,
                                  GiroOptionKeyExplosureDelay
                                  ];
    __block NSDictionary *responseWithOptions = [NSDictionary dictionary];
    __weak typeof(self) weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetOptions:requestedOptions operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            responseWithOptions = [[response valueForKey:@"results"] valueForKey:@"options"];
            weakSelf.explosureDelay = [responseWithOptions[GiroOptionKeyExplosureDelay] integerValue];
            [weakSelf setBatteryLevel:[responseWithOptions[GiroOptionKeyButteryLevel] integerValue]];
            [weakSelf setDefaultMode:responseWithOptions[GiroOptionKeyCaptureMode]];
        }
    }];
}

#pragma mark - Camera mode

- (void)setCameraMode:(NSInteger)mode WithPreviousMode:(NSInteger)previousMode
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    __weak typeof(self) weakSelf = self;
    [AppHelper showLoaderWithText:@"Switching"];
    [manager giropticSwitchToMode:mode operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setModeButtonImageForSelectedMode];
            [weakSelf getCurrentOptionFromCamera];
        } else {
            weakSelf.currentMode = previousMode;
            [AppHelper alertViewWithMessage:error.description];
        }
        [AppHelper hideLoader];
        [weakSelf setActiveModeVisible];
    }];
}

- (void)initSelectedModeButton
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    self.currentMode = manager.selectedMode;
    [self setModeButtonImageForSelectedMode];
}

- (void)createBrust
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    [AppHelper showLoader];
    
    __block CGFloat screenScale = 2;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(nativeScale)]) {
        screenScale = [UIScreen mainScreen].nativeScale;
    }
    
    [manager giropticTakePicture:^(BOOL success, id response, NSError *error) {
        if (success) {
            
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
        [AppHelper hideLoader];
    }];
}

- (void)startVideoOrTimelapse
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    __weak typeof(self) weakSelf = self;
    [AppHelper showLoader];
    [manager giropticStartRecording:^(BOOL success, id response, NSError *error) {
        if (success) {
            weakSelf.videOrTimelapseStarted = YES;
            [weakSelf startVideoRecordTimer];
            [weakSelf pauseButtonDesign:YES];
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
        [AppHelper hideLoader];
    }];
}

- (void)stopVideo
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    [weakSelf stopVideoRecordTimer];
    [manager giropticStopRecording:^(BOOL success, id response, NSError *error) {
        if (success) {
            weakSelf.videOrTimelapseStarted = NO;
            [weakSelf pauseButtonDesign:NO];
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
        [AppHelper hideLoader];
    }];
}

- (void)stopVideoOrTimelapse
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    [AppHelper showLoader];
    __weak typeof(self) weakSelf = self;
    [weakSelf stopVideoRecordTimer];
    [manager giropticStopRecording:^(BOOL success, id response, NSError *error) {
        if (success) {
            weakSelf.videOrTimelapseStarted = NO;
            [weakSelf pauseButtonDesign:NO];
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
        [AppHelper hideLoader];
    }];
}

- (void)pauseButtonDesign:(BOOL)isPause
{
    [UIView animateWithDuration:0.2 animations:^{
        if (isPause) {
            [self.roundButtonContainerView layoutIfNeeded];
            [self.actionButton setImage:[UIImage imageNamed:@"ic_pause"] forState:UIControlStateNormal];
            [self updateBackgroundActionButtonImageConstraint:90];
        } else {
            [self.roundButtonContainerView layoutIfNeeded];
            [self.actionButton setImage:nil forState:UIControlStateNormal];
            [self updateBackgroundActionButtonImageConstraint:80];
        }
    }];
}

- (void)updateBackgroundActionButtonImageConstraint:(NSInteger)newValue
{
    self.widtButtonRoundConstraint.constant = newValue;
    self.heightButtonRoundConstraint.constant = newValue;
    self.roundButtonContainerView.layer.cornerRadius = newValue / 2;
}

#pragma mark - Battery

- (void)startBatteryTimer
{
    self.batteryTimer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(getBatteryLevelFromCamera) userInfo:nil repeats:YES];
    [self.batteryTimer fire];
}

- (void)getBatteryLevelFromCamera
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    __weak typeof(self) weakSelf = self;
    [manager giropticGetState:^(BOOL success, id response, NSError *error) {
        if (success) {
            NSInteger batteryLevel = [response[@"batteryLevel"] integerValue];
            [weakSelf setBatteryLevel:batteryLevel];
        } else {
            [AppHelper alertViewWithMessage:error.description];
        }
    }];
}

- (void)setBatteryLevel:(NSInteger)level
{
    if (level < 10) {
        [self updateCameraBatteryLevel:CameraBatteryLevelZero];
    } else if (level < 30) {
        [self updateCameraBatteryLevel:CameraBatteryLevelMinimum];
    } else if (level < 60) {
        [self updateCameraBatteryLevel:CameraBatteryLevelLow];
    } else if (level < 85){
        [self updateCameraBatteryLevel:CameraBatteryLevelMedium];
    } else {
        [self updateCameraBatteryLevel:CameraBatteryLevelMaximum];
    }
}

- (void)startBatteryAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    animation.duration = 2.0f;
    animation.repeatCount = MAXFLOAT;
    animation.keyTimes = @[@0, @0.3, @0.5, @0.8, @1];
    animation.values = @[@1, @0.5, @0, @0.5, @1];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.batteryImageView.layer addAnimation:animation forKey:@"fade"];
}

#pragma mark - Private

- (void)changeModeDesign
{
    if (self.currentMode == GiropticCameraModeVideo || self.currentMode == GiropticCameraModeTimeLaps) {
        self.timerLabel.hidden = NO;
        self.indicatorView.hidden = NO;
    } else {
        self.timerLabel.hidden = YES;
        self.indicatorView.hidden = YES;
    }
}

- (void)setDefaultMode:(NSString *)mode
{
    if ([mode isEqualToString:GiroCaptureModeVideo]) {
        self.currentMode = GiropticCameraModeVideo;
    } else if ([mode isEqualToString:GiroCaptureModeImage]) {
        self.currentMode = GiropticCameraModeImage;
    } else if ([mode isEqualToString:GiroCaptureModeBurst]) {
        self.currentMode = GiropticCameraModeBurst;
    } else if ([mode isEqualToString:GiroCaptureModeTimeLapse]) {
        self.currentMode = GiropticCameraModeTimeLaps;
    }
    [CameraNetworkManager sharedManager].selectedMode = self.currentMode;
    [self setModeButtonImageForSelectedMode];
    [self setActiveModeVisible];
}

- (void)shouldShowModeSelectionView:(BOOL)shouldShow
{
    if (self.internalTimer) {
        [self stopVideoRecordTimer];
    }
    if (shouldShow) {
        self.selectedModeButton.layer.borderColor = [UIColor lightBlueBorderColor].CGColor;
        self.selectedModeButton.layer.borderWidth = 1.f;
    } else {
        self.selectedModeButton.layer.borderColor = [UIColor lightGrayBorderColor].CGColor;
        self.selectedModeButton.layer.borderWidth = 1.f;
    }
    
    CGFloat height = self.modeSelectionView.bounds.size.height;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [NSValue valueWithCGPoint:self.contentView.center];
    CGPoint toValue = self.contentView.center;
    toValue.y = shouldShow ? toValue.y - height : toValue.y + height;
    animation.toValue = [NSValue valueWithCGPoint:toValue];
    animation.removedOnCompletion = NO;
    animation.delegate = self;
    animation.duration = 0.25;
    [self.contentView.layer addAnimation:animation forKey:@"moveContentView"];
    self.contentView.center = toValue;
    
    CABasicAnimation *anim = [animation mutableCopy];
    anim.fromValue = [NSValue valueWithCGPoint:self.modeSelectionView.center];
    CGPoint endValue = self.modeSelectionView.center;
    endValue.y = shouldShow ? endValue.y - height : endValue.y + height;
    anim.toValue = [NSValue valueWithCGPoint:endValue];
    [self.modeSelectionView.layer addAnimation:anim forKey:@"moveModeSelectionView"];
    self.modeSelectionView.center = endValue;
    
    if (!shouldShow) {
        self.contentViewBottomVerticalSpace.constant = 0;
        self.contentVIewVerticalSpaceConstraint.constant = 0;
        self.modeSelectionViewVerticalSpaceConstraint.constant = 0;
    } else {
        self.contentViewBottomVerticalSpace.constant = height;
        self.contentVIewVerticalSpaceConstraint.constant = -height;
        self.modeSelectionViewVerticalSpaceConstraint.constant = -height;
    }
}

- (void)prepareNavigationBarButtons
{
    ((CameraNavigationController *)self.navigationController).subDelegate = self;
    
    [self.navigationController setButtonsWithImageNamed:@[BrakerButtonImageName, DetailsButtonImageName]
                                      andActionDelegate:self
                                             tintColors:@[[UIColor lightBlueBorderColor], [UIColor grayColor]]
                                               position:ButtonPositionModeRight
                          selectorsStringRepresentation:@[NSStringFromSelector(@selector(brakerButtonPress)), NSStringFromSelector(@selector(detailsButonPressed))]
                                            buttonWidth:20.f];
    self.navigationItem.title = @" ";
}

- (void)setBorderStyleForView:(UIView *)view
{
    view.layer.borderColor = [UIColor lightGrayBorderColor].CGColor;
    view.layer.borderWidth = 1.f;
}

- (void)setBorders
{
    for (UIView *subView in self.topInformationView.subviews) {
        [self setBorderStyleForView:subView];
    }
    [self setBorderStyleForView:self.bottomInformationView];
    self.roundButtonContainerView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.roundButtonContainerView.layer.borderWidth = 1.f;
    self.roundButtonContainerView.layer.cornerRadius = self.roundButtonContainerView.bounds.size.height / 2;
    
    for (UIView *button in self.controlElementsView.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            button.layer.cornerRadius = button.bounds.size.height / 2;
            [self setBorderStyleForView:button];
        }
    }
    self.indicatorView.layer.cornerRadius = self.indicatorView.bounds.size.height / 2;
}

- (void)setInsentsForButtons
{
    for (UIButton *button in self.modeSelectionView.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            
            CGSize buttonSize = button.frame.size;
            NSString *buttonTitle = button.titleLabel.text;
            CGSize titleSize = [buttonTitle sizeWithAttributes:@{ NSFontAttributeName : [UIFont camFontZonaProBoldWithSize:12.f] }];
            UIImage *buttonImage = button.imageView.image;
            CGSize buttonImageSize = buttonImage.size;
            
            CGFloat offsetBetweenImageAndText = 10;
            
            [button setImageEdgeInsets:UIEdgeInsetsMake((buttonSize.height - (titleSize.height + buttonImageSize.height)) / 2 - offsetBetweenImageAndText,
                                                        (buttonSize.width - buttonImageSize.width) / 2,
                                                        0,0)];
            [button setTitleEdgeInsets:UIEdgeInsetsMake((buttonSize.height - (titleSize.height + buttonImageSize.height)) / 2 + buttonImageSize.height + offsetBetweenImageAndText,
                                                        titleSize.width + [button imageEdgeInsets].left > buttonSize.width ? -buttonImage.size.width  +  (buttonSize.width - titleSize.width) / 2 : (buttonSize.width - titleSize.width) / 2 - buttonImage.size.width,
                                                        0,0)];
        }
    }
}

- (void)setStartParameters
{
    self.isModeSelectionViewShown = NO;
}

- (void)setActiveModeVisible
{
    NSMutableArray *inactiveButtons = [[NSMutableArray alloc] init];
    UIButton *activeButton;
    for (UIButton *button in self.modeSelectionView.subviews) {
        if (button.tag == self.currentMode) {
            activeButton = button;
        } else {
            [inactiveButtons addObject:button];
        }
    }
    [self deselectButtonsFromArray:inactiveButtons];
    if (activeButton) {
        [activeButton setSelected:YES];
    }
    [self changeModeDesign];
}

- (void)deselectButtonsFromArray:(NSArray *)buttons
{
    for (UIButton *buttonToDeselect in buttons) {
        [buttonToDeselect setSelected:NO];
    }
}

- (void)modeButtonPress:(UIButton *)button
{
    self.isModeSelectionViewShown = NO;
    NSInteger previousMode = self.currentMode;
    self.currentMode = button.tag;
    [self setCameraMode:self.currentMode WithPreviousMode:previousMode];
    if (IS_IPHONE_4_OR_LESS) {
       [self shouldShowModeSelectionView:NO];
    } else {
       [self performSelector:@selector(shouldShowModeSelectionView:) withObject:@(NO) afterDelay:0.1f];
    }
}

- (void)setModeButtonImageForSelectedMode
{
    NSInteger elementsToCut = @"_active".length;
    switch (self.currentMode) {
        case 1: {
            [self updateIMageForSelectedModeButton:[ActiveVideoButtonImageName substringToIndex:ActiveVideoButtonImageName.length - elementsToCut] hightlightedImage:ActiveVideoButtonImageName];
            break;
        }
        case 2: {
            [self updateIMageForSelectedModeButton:[ActivePhotoButtonImageName substringToIndex:ActivePhotoButtonImageName.length - elementsToCut] hightlightedImage:ActivePhotoButtonImageName];
            break;
        }
        case 3: {
            [self updateIMageForSelectedModeButton:[ActiveBurstButtonImageName substringToIndex:ActiveBurstButtonImageName.length - elementsToCut] hightlightedImage:ActiveBurstButtonImageName];
            break;
        }
        case 4: {
            [self updateIMageForSelectedModeButton:[ActiveTimeLapseButtonImageName substringToIndex:ActiveTimeLapseButtonImageName.length - elementsToCut] hightlightedImage:ActiveTimeLapseButtonImageName];
            break;
        }
    }
}

- (void)updateIMageForSelectedModeButton:(NSString *)normalStateImage hightlightedImage:(NSString *)imageForHightLightedState
{
    [self.selectedModeButton setImage:[UIImage imageNamed:normalStateImage] forState:UIControlStateNormal];
    [self.selectedModeButton setImage:[UIImage imageNamed:imageForHightLightedState] forState:UIControlStateHighlighted];
    [self.selectedModeButton setImage:[UIImage imageNamed:imageForHightLightedState] forState:UIControlStateSelected];
}

- (void)updateCameraBatteryLevel:(CameraBatteryLevel)chargeLevel
{
    [self.batteryImageView.layer removeAllAnimations];
    UIImage *image;
    switch (chargeLevel) {
        case CameraBatteryLevelZero: {
            image = [UIImage imageNamed:@"ic_battery_empty"];
            [self startBatteryAnimation];
            break;
        }
        case CameraBatteryLevelMinimum: {
            image = [UIImage imageNamed:@"ic_battery_1"];
            break;
        }
        case CameraBatteryLevelLow: {
            image = [UIImage imageNamed:@"ic_battery_2"];
            break;
        }
        case CameraBatteryLevelMedium: {
            image = [UIImage imageNamed:@"ic_battery_3"];
            break;
        }
        case CameraBatteryLevelMaximum: {
            image = [UIImage imageNamed:@"ic_battery_4"];
            break;
        }
    }
    self.batteryImageView.image = image;
}

- (void)updateCameraSignalLevel:(CameraSignalLevel)signalLevel
{
    NSString *assetName;
    switch (signalLevel) {
        case CameraSignalMinimum: {
            assetName = @"ic_signal_1";
            break;
        }
        case CameraSignalMedium: {
            assetName = @"ic_signal_2";
            break;
        }
        case CameraSignalMaximum: {
            assetName = @"ic_signal_3";
            break;
        }
    }
    self.signalImageView.image = [UIImage imageNamed:assetName];
}

- (void)updateCameraControlState:(CameraControlState)state
{
    NSString *assetName;
    switch (state) {
        case CameraControlStateNone: {
            assetName = @"";
            break;
        }
        case CameraControlStatePause: {
            assetName = @"ic_pause";
            break;
        }
        case CameraControlStatePlay: {
            assetName = @"ic_play";
            break;
        }
    }
    if (assetName.length) {
        [self.actionButton setImage:[UIImage imageNamed:assetName] forState:UIControlStateNormal];
    } else {
        [self.actionButton setImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    }
}

- (void)showHidePreview
{
    __weak typeof(self) weakSelf = self;
    CGFloat constant;
    if (self.resultPreviewView.hidden) {
        self.resultPreviewView.hidden = NO;
        self.resultPreviewView.layer.opacity = 1.f;
        [self.resultPreviewView.layer addAnimation:[Animation fadeAnimFromValue:0.f to:1.f delegate:nil] forKey:nil];
        constant = 25;
    } else {
        [self.resultPreviewView.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.f delegate:self] forKey:@"hidePreview"];
        self.resultPreviewView.layer.opacity = 0.f;
        constant = 0;
    }
    [UIView animateWithDuration:0.25 animations:^{
        weakSelf.centerYRoundButtonsConstraint.constant = constant;
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)updateUIIfNeeded
{
    if (IS_IPHONE_4_OR_LESS) {
        self.topYConstraint.constant = 100;
    }
}

- (void)setPreviewAssetName:(NSString *)fileName assetDuration:(NSString *)assetDuration assetThumbnail:(UIImage *)thumbnail
{
    self.resultPreviewAssetNameLabel.text = fileName;
    self.resultPreviewAssetDurationLabel.text = assetDuration;
    self.resultPreviewThumbnailsImageView.image = thumbnail;
}

- (void)startVideoRecordTimer
{
    self.startDate = [NSDate date];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.internalTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:weakSelf selector:@selector(updateTimerLabel) userInfo:nil repeats:YES];
    });
    [self.internalTimer fire];
    self.indicatorView.backgroundColor = [UIColor pinkRecordColor];
    [Animation blinkAnimationForView:self.indicatorView];
}

- (void)stopVideoRecordTimer
{
    [self.indicatorView.layer removeAllAnimations];
    self.indicatorView.backgroundColor = [UIColor lightGrayBorderColor];
    self.indicatorView.layer.opacity = 1.f;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.internalTimer invalidate];
        weakSelf.internalTimer = nil;
    });
    
    self.timerLabel.text = @"00:00";
}

- (void)updateTimerLabel
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:self.startDate];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *timeString = [dateFormatter stringFromDate:timerDate];
    self.timerLabel.text = timeString;
}

@end