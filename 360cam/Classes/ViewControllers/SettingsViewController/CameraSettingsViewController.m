//
//  CameraSettingsViewController.m
//  360cam
//
//  Created by Misha Gajdan on 15.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "CameraSettingsViewController.h"
#import "DetailedSettingViewController.h"
#import "RootViewController.h"
#import "UINavigationController+Transparent.h"

//cells
#import "SettingsHeaderTableViewCell.h"
#import "SettingsDescriptionTableViewCell.h"
#import "SettingsDetailedTableViewCell.h"

static NSString *const TwoLabelCellIdentifier = @"TwoLabelCell";
static NSString *const SwitchLabelCellIdentifier = @"SwitchCell";
static NSString *const TitleCellIdentifier = @"TitleCell";
static NSString *const HeaderCell = @"HeaderCell";

static CGFloat const CellSize = 50.0;
static NSInteger const NumberOfSection = 5;

@interface CameraSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *sectionArray;
@property (strong, nonatomic) NSArray *composedDataSourceForDropDownList;
@property (strong, nonatomic) NSMutableDictionary *currecntSettings;

@property (assign, nonatomic) NSInteger openedSection;

@end

@implementation CameraSettingsViewController 

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSourceForDropDownList];
    [self prepereDataSourceForTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController presentTransparentNavigationBarAnimated:YES];
    self.navigationItem.title = @"";
    
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DetailSettings"]) {
        SettingsDescriptionTableViewCell *cell = (SettingsDescriptionTableViewCell *)sender;
        DetailedSettingViewController *controller = [segue destinationViewController];
        controller.title = cell.labelDesciption.text;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return NumberOfSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *data = [self.sectionArray[section] allKeys];
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (!indexPath.section) {
        cell = [self switchCellForIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    } else if (indexPath.row) {
            if (indexPath.section == 1) {
                cell = [self descriptionCellForIndexPath:indexPath];
            } else if (indexPath.section == 2) {
                switch (indexPath.row) {
                    case 1: {
                        cell = [self switchCellForIndexPath:indexPath];
                        break;
                    }
                    default: {
                        cell = [self descriptionCellForIndexPath:indexPath];
                        break;
                    }
                }
            } else {
                cell = [self descriptionCellForIndexPath:indexPath];
            }
    } else {
        cell = [self detailedCellForIndexPath:indexPath];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section <= 1) {
        return CellSize;
    }
    return 0;
}

#pragma  mark - Cell configuration

- (UITableViewCell *)switchCellForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *data = [self.sectionArray[indexPath.section] allKeys];
    
    SettingsSwitchTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:SwitchLabelCellIdentifier forIndexPath:indexPath];
    cell.descriptionLabel.text = data[indexPath.row];
    cell.delegate = (id)self;
    return cell;
}

- (UITableViewCell *)descriptionCellForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *data = [self.sectionArray[indexPath.section] allKeys];
    
    SettingsDescriptionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TwoLabelCellIdentifier forIndexPath:indexPath];
    cell.labelDesciption.text = data[indexPath.row];
    cell.labelMode.text = @"empty";
    return cell;
}

- (UITableViewCell *)detailedCellForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *data = [self.sectionArray[indexPath.section] allKeys];
    
    SettingsDetailedTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:TitleCellIdentifier forIndexPath:indexPath];
    cell.descriptionLabel.text = data[indexPath.row];
    cell.directionImage.image = [UIImage imageNamed:@"settings_arrow"];
    cell.directionImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
    return cell;
}

- (UITableViewCell *)headerCellWithTitle:(NSString *)title
{
    SettingsHeaderTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:HeaderCell];
    cell.descriptionLabel.text = title;
    cell.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if (!section) {
        cell = [self headerCellWithTitle:NSLocalizedString(@"settings.item.general_settings", nil)];
    } else if (section == 1) {
        cell = [self headerCellWithTitle:NSLocalizedString(@"settings.item.mode_settings", nil)];
    }
    UIView *view;
    if (cell) {
        view = [[UIView alloc]initWithFrame:cell.frame];
        [view addSubview:cell];
    }
    return view;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row || !indexPath.section) {
        return;
    }
    if (self.openedSection >= 0) {
        SettingsDetailedTableViewCell *openedCell = (SettingsDetailedTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.openedSection]];
        [UIView animateWithDuration:0.2 animations:^{
            openedCell.directionImage.transform = CGAffineTransformRotate(openedCell.directionImage.transform, -M_PI_2);
        }];
        
        NSMutableDictionary *data = [self.sectionArray[self.openedSection] mutableCopy];
        NSArray *keys = [data allKeys];
        NSRange range = NSMakeRange(1, keys.count - 1);
        NSArray *keysToRemove = [keys subarrayWithRange:range];
        [data removeObjectsForKeys:keysToRemove];
        NSIndexPath *indexPathToClose = [NSIndexPath indexPathForItem:0 inSection:self.openedSection];
        [self.sectionArray replaceObjectAtIndex:indexPathToClose.section withObject:data];

        [self removeRows:keysToRemove.count InSection:self.openedSection];
    }
    
    if (indexPath.section == self.openedSection) {
        self.openedSection = -1;
    } else {
        NSMutableDictionary *dataDictionary = [self.sectionArray[indexPath.section] mutableCopy];
        SettingsDetailedTableViewCell *openedCell = (SettingsDetailedTableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]];
        [UIView animateWithDuration:0.2 animations:^{
            openedCell.directionImage.transform = CGAffineTransformRotate(openedCell.directionImage.transform, M_PI_2);
        }];
        if (indexPath.section == 1) {
            [dataDictionary setObject:NSLocalizedString(@"settings.item.mode_de_selection", nil) forKey:@"Mode de selection"];
            [dataDictionary setObject:NSLocalizedString(@"settings.item.settings.item.field_of_view_resolution", nil) forKey:@"Field of view / Resolution"];
            [dataDictionary setObject:NSLocalizedString(@"settings.item.auto_centering", nil) forKey:@"Auto centering"];
            [dataDictionary setObject:NSLocalizedString(@"settings.item.position_display", nil) forKey:@"Position display"];
        } else if(indexPath.section == 2) {
            [dataDictionary setObject:NSLocalizedString(@"settings.item.hdr", nil) forKey:@"HDR"];
            [dataDictionary setObject:NSLocalizedString(@"settings.item.self_timer", nil) forKey:@"Self timer"];
        } else if(indexPath.section == 3) {
            [dataDictionary setObject:NSLocalizedString(@"settings.item.burst_interval", nil) forKey:@"Interval"];
        } else if(indexPath.section == 4) {
            [dataDictionary setObject:NSLocalizedString(@"settings.item.timel_rate", nil) forKey:@"Rate"];
        }
        
        [self.sectionArray replaceObjectAtIndex:indexPath.section withObject:dataDictionary];
        NSInteger newItemCount = [[dataDictionary allKeys] count];
        [self insertRows:newItemCount InSection:indexPath.section];
        self.openedSection = indexPath.section;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Private

- (void)insertRows:(NSInteger)rowsCount InSection:(NSInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray new];
    for (int i = 1; i <= rowsCount - 1; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    self.openedSection = section;
}

- (void)removeRows:(NSInteger)rowsCount InSection:(NSInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray new];
    for (int i = 1; i <= rowsCount; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
}

- (void)prepareDataSourceForDropDownList
{
    self.composedDataSourceForDropDownList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SettingsModes" ofType:@"plist"]];
}

- (void)prepereDataSourceForTableView
{
    NSMutableArray *dataSource = [NSMutableArray new];

    [dataSource addObject:@{@"GPS Data" : NSLocalizedString(@"settings.item.gps_data", nil),
                            @"Gyroscope" : NSLocalizedString(@"settings.item.gyroscope", nil)}];
    [dataSource addObject:@{@"Video" : NSLocalizedString(@"settings.item.video", nil)}];
    [dataSource addObject:@{@"Photo" : NSLocalizedString(@"settings.item.photo", nil)}];
    [dataSource addObject:@{@"Timelapse" : NSLocalizedString(@"settings.item.timelapse", nil)}];
    [dataSource addObject:@{@"Burst" : NSLocalizedString(@"settings.item.brust", nil)}];
    
    self.sectionArray = dataSource;
    self.openedSection = -1;
}

#pragma mark - SettingsSwitchTableViewCellProtocol

- (void)valueChanged:(BOOL)newValue forKey:(NSString *)key sender:(id)sender
{
}

@end
