//
//  CameraSettingsViewController.h
//  360cam
//
//  Created by Misha Gajdan on 15.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "SettingsSwitchTableViewCell.h"

@interface CameraSettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, SettingsSwitchTableViewCellProtocol>

@end

