//
//  ArticleMyGallerieViewController.h
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef NS_ENUM (NSUInteger, ViewerType) {
    ViewerTypeUndefined,
    ViewerTypeLocalGallery,
    ViewerTypeLocalBestOf,
    ViewerTypeRemoteCamera
};

@interface MediaFileDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) ViewerType viewerType;
@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) UIImage *thumbnail;
@property (assign, nonatomic) NSUInteger itemIndex;

@property (assign, nonatomic) MediaFileType mediaType;

@end
