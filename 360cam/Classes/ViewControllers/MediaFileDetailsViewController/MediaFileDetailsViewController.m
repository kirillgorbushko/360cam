//
//  ArticleMyGallerieViewController.m
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

// Controllers
#import "MediaFileDetailsViewController.h"
#import "PhotoPlayerViewController.h"
#import "VideoPlayerViewController.h"
#import "RootViewController.h"
#import "ShareImportViewController.h"
#import "RemoveFileViewController.h"

// Cells
#import "TimelapseTableViewCell.h"
#import "PhotoContentTableViewCell.h"
#import "PhotoDescriptionTableViewCell.h"
#import "MapDetailTableViewCell.h"
#import "PhotoNameTableViewCell.h"

// Categories
#import "UIImage+Metadata.h"
#import "MBProgressHUD+CancelButton.h"

// Manager
#import "LocationManager.h"
#import "LoadImageManager.h"
#import "AssetsManager.h"

// Models
#import "AssetModel.h"

static NSString *const TimelapseCellIdentifier = @"timelapseCell";
static NSString *const PhotoContentCellIdentifier = @"photoContentCell";
static NSString *const PhotoDescriptionCellIdentifier = @"photoDescriptionCell";
static NSString *const MapDetailCellIdentifier = @"mapDetailCell";
static NSString *const PhotoNameCellIdentifier = @"photoNameCell";

static NSString *const FileTypeIconVideoNameActive = @"ic_video_active";
static NSString *const FileTypeIconPhotoNameActive = @"ic_photo_active";
static NSString *const FileTypeIconBurstNameActive = @"ic_burst_active";
static NSString *const FileTypeIconTimeLapseNameActive = @"ic_timelapse_active";

static NSString *const VideoPlayerSegue = @"videoPlayerSegue";
static NSString *const PhotoPlayerSegue = @"photoPlayerSegue";
static NSString *const ShareSeque = @"shareFromMediaFileDetailsSegue";
static NSString *const RemoveBarButtonSegue = @"removeFromDetailsScreenSegue";

@interface MediaFileDetailsViewController () <NSURLSessionTaskDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fakeBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *fakeBarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftToolbarItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightToolBarItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *previousToolBarItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextToolBarItem;

@property (strong, nonatomic) NSArray *cellTypes;
@property (strong, nonatomic) NSMutableArray *toolbarDatasource;
@property (strong, nonatomic) PhotoContentTableViewCell *prototypeCell;
@property (strong, nonatomic) NSDictionary *metadataForImage;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) NSURLSessionDownloadTask *currentTask;

@end

@implementation MediaFileDetailsViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateUIIfNeeded];
    [self configureNavigationBar];
    [self configureToolBarFont];
    [self configureTableView];
    [self prepareCellList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self prepareProgressHUD];
    [self.navigationController setToolbarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.HUD removeFromSuperview];
    [self.navigationController setToolbarHidden:YES animated:NO];
}

#pragma mark - CustomAccessors

- (void)setViewerType:(ViewerType)viewerType
{
    _viewerType = viewerType;
    [self updateToolBarrButtons];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cellTypes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:self.cellTypes[indexPath.row] forIndexPath:indexPath];
    if (indexPath.row == 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else {
        cell.userInteractionEnabled = NO;
    }

    [self configureCell:cell];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(nonnull UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        if (self.viewerType == ViewerTypeLocalBestOf) {
            [self didSelectBestOfCellAtIndexPath:indexPath];
        } else if (self.viewerType == ViewerTypeLocalGallery) {
            [self didSelectLocalGalleryCellAtIndexPath:indexPath];
        } else if (self.viewerType == ViewerTypeRemoteCamera) {

        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateCellHeight:self.cellTypes[indexPath.row] atIndexPath:indexPath];
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath
{
    static PhotoDescriptionTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:PhotoDescriptionCellIdentifier];
    });
    
    [self configureCell:sizingCell];
    
    CGFloat height = [self calculateHeightForConfiguredSizingCell:sizingCell];
    return height;
}

- (CGFloat)heightForPhotoNameCellAtIndexPath:(NSIndexPath *)indexPath
{
    static PhotoNameTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:PhotoNameCellIdentifier];
    });
    
    [self configureCell:sizingCell];
    
    CGFloat height = [self calculateHeightForConfiguredSizingCell:sizingCell];
    return height;
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    CGFloat progress = (float)totalBytesWritten / totalBytesExpectedToWrite;
    self.HUD.progress = progress;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSData* data = [NSData dataWithContentsOfURL:location];
    UIImage *image = [UIImage imageWithData:data];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf showPreview:image viewerType:ViewerTypeLocalBestOf];
    });
}

- (void)URLSession:(nonnull NSURLSession *)session task:(nonnull NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error
{
    if (error) {
        [self.HUD hide:YES];
        if (error.code != -999) {
            [AppHelper alertViewWithMessage:[error.localizedDescription capitalizedString]];
        }
    }
}

#pragma mark - IBActions

- (IBAction)toolbarPreviousTapped:(id)sender
{
    [self gotoPreviousItem];
    [self configureToolbarIfNeeded];
}

- (IBAction)leftToolBrItemPressed:(id)sender
{
    if (self.viewerType == ViewerTypeLocalGallery) {
        [self performSegueWithIdentifier:ShareSeque sender:self];
    }
}

- (IBAction)rightToolbarItemPressed:(id)sender
{
    if (self.viewerType == ViewerTypeLocalBestOf) {
        [self performSegueWithIdentifier:ShareSeque sender:self];
    } else if (self.viewerType == ViewerTypeRemoteCamera) {
        [self performSegueWithIdentifier:RemoveBarButtonSegue sender:self];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        __weak typeof(self) weakSelf = self;
        [[AssetsManager sharedInstance] deleteAsset:asset.sourceUrl withCompletitionHandler:^(NSError *error) {
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                });
            }
        }];
    }
}

- (IBAction)toolbarNextTapped:(id)sender
{
    [self gotoNextItem];
    [self configureToolbarIfNeeded];
}

- (void)MBProgressHUDCancelButtonDidPressed
{
    if (self.currentTask.state == NSURLSessionTaskStateCompleted) {
        [self.HUD hide:YES];
    } else {
        [self.currentTask cancel];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:PhotoPlayerSegue]) {
        [self photoPlayerSeque:segue];
    } else if ([segue.identifier isEqualToString:VideoPlayerSegue]) {
        [self videoPlayerSegue:segue];
    } else if ([segue.identifier isEqualToString:ShareSeque]) {
        [self prepareToPresentShareScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:RemoveBarButtonSegue]) {
        [self prepareToPresentRemoveFileScreenWithSegue:segue];
    }
}

#pragma mark - Private

- (void)prepareToPresentRemoveFileScreenWithSegue:(UIStoryboardSegue *)segue
{
    //RemoveFileViewController *removeFileViewController = segue.destinationViewController;
    //todo additional configuration
}

- (void)photoPlayerSeque:(UIStoryboardSegue *)segue
{
    PhotoPlayerViewController *player = segue.destinationViewController;
    player.detailDataSource = self.dataSource[self.itemIndex];
    player.thumbnail = self.thumbnail;
    if (self.viewerType == ViewerTypeLocalGallery) {
        player.viewerType = ViewerTypeLocalGallery;
    } else if(self.viewerType == ViewerTypeRemoteCamera) {
        player.viewerType = ViewerTypeRemoteCamera;
    }
}

- (void)videoPlayerSegue:(UIStoryboardSegue *)segue
{
    VideoPlayerViewController *player = segue.destinationViewController;
    player.dataSource = self.dataSource[self.itemIndex];
    if (self.viewerType == ViewerTypeLocalBestOf) {
        player.viewerType = ViewerTypeLocalBestOf;
        player.sourceVideoURL = [[NSURL URLWithString:[@"http://player.vimeo.com/external/" stringByAppendingPathComponent:[self.dataSource[self.itemIndex] valueForKey:@"path_high"]]] absoluteString];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        player.viewerType = ViewerTypeLocalGallery;
        player.sourceVideoURL = asset.sourceUrl.absoluteString;
    } else if (self.viewerType == ViewerTypeRemoteCamera){
        player.viewerType = ViewerTypeRemoteCamera;
    }
}

- (void)prepareToPresentShareScreenWithSegue:(UIStoryboardSegue *)segue
{
    UINavigationController *shareNavigationController = segue.destinationViewController;
    ShareImportViewController *controller = (ShareImportViewController *)shareNavigationController.topViewController;
    controller.thumbnail = self.thumbnail;
    controller.detailDataSource = [self.dataSource objectAtIndex:self.itemIndex];
    if (self.viewerType == ViewerTypeLocalGallery) {
        controller.fullResolutionImage = [((AssetModel *)controller.detailDataSource) fullResolutionImage];
    }
    __weak typeof(self) weakSelf = self;
    controller.didCloseViewController = ^() {
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
}

- (void)configureTableView
{
    self.tableView.estimatedRowHeight = [UIScreen mainScreen].bounds.size.height * 0.3f;
}

- (CGFloat)calculateCellHeight:(NSString*)cellType atIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight;
    if ([cellType isEqualToString:TimelapseCellIdentifier]) {
        cellHeight =  50.f;
    } else if ([cellType isEqualToString:PhotoContentCellIdentifier]) {
        cellHeight = [UIScreen mainScreen].bounds.size.height * 0.3f;
    } else if ([cellType isEqualToString:PhotoDescriptionCellIdentifier]) {
        cellHeight = [self heightForBasicCellAtIndexPath:indexPath];
    } else if ([cellType isEqualToString:MapDetailCellIdentifier]) {
        cellHeight = 300.f;
    } else if ([cellType isEqualToString:PhotoNameCellIdentifier]) {
        cellHeight = [self heightForPhotoNameCellAtIndexPath:indexPath];
    }
    return cellHeight;
}

- (void)configureCell:(UITableViewCell *)cell
{
    if ([cell isKindOfClass:[TimelapseTableViewCell class]]) {
        [self configureTimelapseCell:(TimelapseTableViewCell *)cell];
    } else if ([cell isKindOfClass:[PhotoContentTableViewCell class]]) {
        [self configurePhotoContentCell:(PhotoContentTableViewCell *)cell];
    } else if ([cell isKindOfClass:[PhotoDescriptionTableViewCell class]]) {
        [self configurePhotoDescriptionCell:(PhotoDescriptionTableViewCell *)cell];
    } else if ([cell isKindOfClass:[MapDetailTableViewCell class]]) {
        [self configureMapDetailCell:(MapDetailTableViewCell *)cell];
    } else if ([cell isKindOfClass:[PhotoNameTableViewCell class]]) {
        [self configurePhotoNameCell:(PhotoNameTableViewCell *)cell];
    }
}

- (void)configurePhotoNameCell:(PhotoNameTableViewCell *)cell
{
    cell.fileNameLabel.text = @"TEST Title";
}

- (void)configureTimelapseCell:(TimelapseTableViewCell *)cell
{
    if (self.viewerType == ViewerTypeLocalBestOf) {
        cell.timelapseImageView.image = [UIImage imageNamed:[self iconForType:[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"type"]]];
        cell.timelapseTitleLabel.text = [self localizedTitleForType:[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"type"]];
    }
    if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        cell.timelapseImageView.image = [UIImage imageNamed:[self iconForType:asset.type]];
        cell.timelapseTitleLabel.text = [self localizedTitleForType:asset.type];
        
        self.thumbnail = [asset fullScreenImage];
        self.metadataForImage = asset.metadata;
        
        cell.timelapseDescriptionLabel.text = [self dateToString:asset.createdDate];
        return;
    }
    
    if (self.metadataForImage) {
        [self showImageDateForCell:cell];
    } else {
        __weak typeof(self) weakSelf = self;
        [self getImageMetaData:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
            });
        }];
    }
}

- (NSString *)dateToString:(NSDate *)createdDate
{
    NSDateFormatter* exifFormat = [[NSDateFormatter alloc] init];
    [exifFormat setDateFormat:@"MMMM, dd, HH:mm aaa"];
    return [[exifFormat stringFromDate:createdDate] capitalizedString];
}

- (void)showImageDateForCell:(TimelapseTableViewCell *)cell
{
    NSString *originalDate = [self.thumbnail dateFromExifMetadata:self.metadataForImage];
    NSString *dateStringDefault = @" ";
    if (originalDate.length) {
        
        NSDateFormatter* exifFormat = [[NSDateFormatter alloc] init];
        [exifFormat setDateFormat:@"YYYY:MM:dd HH:mm:ss"];
        NSDate *data = [exifFormat dateFromString:originalDate];

        [exifFormat setDateFormat:@"MMMM, dd, HH:mm aaa"];
        dateStringDefault = [[exifFormat stringFromDate:data] capitalizedString];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        cell.timelapseDescriptionLabel.text = dateStringDefault;
    });
}

- (void)configurePhotoContentCell:(PhotoContentTableViewCell *)cell
{
    cell.photoContentImageView.image = self.thumbnail;
}

- (void)configurePhotoDescriptionCell:(PhotoDescriptionTableViewCell *)cell
{
    if (self.viewerType == ViewerTypeLocalBestOf) {
        cell.titleLabel.attributedText = [self titleWithText:[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"title"]];
        
        NSString *cellDescription = [[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"description"];
        if ([cellDescription stringByReplacingOccurrencesOfString:@" " withString:@""].length) {
            cell.descriptionLabel.attributedText = [self descriptionWithText:[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"description"]];
        } else {
            cell.descriptionLabel.attributedText = [self descriptionWithText:NSLocalizedString(@"article_my_gallerie.no_description_avaliable", nil)];
        }
        cell.createdLabel.text = NSLocalizedString(@"article_my_gallerie.created", nil);
        if (self.metadataForImage) {
            cell.createdValueLabel.text = [self.thumbnail ownerNameFromExifMetadata:self.metadataForImage];
        }
    }
    
    if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        cell.titleLabel.attributedText = [self titleWithText:asset.fileName];
        cell.descriptionLabel.attributedText = [self descriptionWithText:NSLocalizedString(@"article_my_gallerie.no_description_avaliable", nil)];
        cell.createdLabel.text = NSLocalizedString(@"article_my_gallerie.created", nil);
        if (asset.metadata) {
            cell.createdValueLabel.text = [[asset fullScreenImage] ownerNameFromExifMetadata:asset.metadata];
        }
    }
}

- (void)configureMapDetailCellForBestOf:(MapDetailTableViewCell *)cell
{
    NSDictionary *locationDetails = [self.thumbnail locationDataFromMetadata:self.metadataForImage];
    
    CGFloat latitude = [[locationDetails objectForKey:(NSString *)kCGImagePropertyGPSLatitude] floatValue];
    CGFloat longtitude = [[locationDetails objectForKey:(NSString *)kCGImagePropertyGPSLongitude] floatValue];
    
    CLLocationCoordinate2D locationCoordinate =  CLLocationCoordinate2DMake(latitude, longtitude);
    cell.location = locationCoordinate;
    
    cell.gpsValueLabel.text = [LocationManager GPSDataFromLocation:locationCoordinate
                                                       latitudeRef:[locationDetails objectForKey:(NSString *)kCGImagePropertyGPSLatitudeRef]
                                                     longtitudeRef:[locationDetails objectForKey:(NSString *)kCGImagePropertyGPSLongitudeRef]];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:cell.location.latitude longitude:cell.location.longitude];
    [[LocationManager sharedManager] fetchAddressWithLocation:location completionBlock:^(NSString *address) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (address.length) {
                cell.locationValueLabel.text = address;
            }
        });
    }];
}

- (void)configureMapDetailCellForLocalGallery:(MapDetailTableViewCell *)cell
{
    AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
    CLLocation *location = asset.location;
    
    if (!location) {
        cell.locationValueLabel.text = NSLocalizedString(@"local.my_gallery.location", nil);
        cell.gpsValueLabel.text = NSLocalizedString(@"local.my_gallery.gps", nil);
        return;
    }
    
    CLLocationCoordinate2D locationCoordinate = location.coordinate;
    cell.location = locationCoordinate;
    
    NSString *GPSLatitudeRef = (locationCoordinate.latitude > 0) ? @"N" : @"S";
    NSString *GPSLongitudeRef = (locationCoordinate.longitude > 0) ? @"E" : @"W";
    
    cell.gpsValueLabel.text = [LocationManager GPSDataFromLocation:locationCoordinate
                                                       latitudeRef:GPSLatitudeRef
                                                     longtitudeRef:GPSLongitudeRef];
    
    [[LocationManager sharedManager] fetchAddressWithLocation:location completionBlock:^(NSString *address) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (address.length) {
                cell.locationValueLabel.text = address;
            }
        });
    }];
}

- (void)configureMapDetailCell:(MapDetailTableViewCell *)cell
{
    cell.locationLabel.text = NSLocalizedString(@"article_my_gallerie.location", nil);
    cell.gpsLabel.text = NSLocalizedString(@"article_my_gallerie.gps", nil);
    
    if (self.viewerType == ViewerTypeLocalBestOf) {
        [self configureMapDetailCellForBestOf:cell];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        [self configureMapDetailCellForLocalGallery:cell];
    }
}

- (void)configureNavigationBar
{
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIFont *titleFont = [UIFont camFontZonaProRegularWithSize:20];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                    NSFontAttributeName : titleFont};
    if (self.viewerType == ViewerTypeLocalBestOf) {
        self.fakeBarView.backgroundColor = [UIColor colorGalleryLightBlue];
        self.navigationItem.title = [[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"title"];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        self.fakeBarView.backgroundColor = [UIColor colorGalleryLightBlue];
        self.navigationItem.title = asset.fileName;
    } else {
        self.fakeBarView.backgroundColor = [UIColor grayForDetailSettingsNavigationBar];
        //to do
    }
}

- (void)prepareCellList
{
    if (self.viewerType == ViewerTypeRemoteCamera) {
        self.cellTypes = @[TimelapseCellIdentifier, PhotoContentCellIdentifier, PhotoNameCellIdentifier, MapDetailCellIdentifier];
    } else {
        self.cellTypes = @[TimelapseCellIdentifier, PhotoContentCellIdentifier, PhotoDescriptionCellIdentifier, MapDetailCellIdentifier];
    }
}

- (NSAttributedString*)titleWithText:(NSString *)text
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8.f];
    NSDictionary *font = @{NSFontAttributeName : [UIFont camFontZonaProLightWithSize:28.f],
                           NSForegroundColorAttributeName : [UIColor colorGalleryBlackTextColor],
                           NSParagraphStyleAttributeName : paragraphStyle};
    [attributedString addAttributes:font range:NSMakeRange(0, text.length)];
    return attributedString;
}

- (NSAttributedString*)descriptionWithText:(NSString *)text
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8.f];
    NSDictionary *font = @{NSFontAttributeName : [UIFont camFontZonaProRegularWithSize:15.f],
                           NSForegroundColorAttributeName : [UIColor colorGalleryDescriptionText],
                           NSParagraphStyleAttributeName : paragraphStyle};
    [attributedString addAttributes:font range:NSMakeRange(0, text.length)];
    return attributedString;
}

- (void)configureToolBarFont
{
    NSDictionary *toolBarTextAttributes = @{NSFontAttributeName : [UIFont camFontZonaProBoldWithSize:10.f],
                                             NSForegroundColorAttributeName : [UIColor colorGalleryToolbarTextColor] };
    for (UIBarButtonItem *barButtonItem in self.toolbarItems) {
        [barButtonItem setTitleTextAttributes:toolBarTextAttributes forState:UIControlStateNormal];
    }
    
    [self configureToolbarIfNeeded];
}

- (void)updateUIIfNeeded
{
    self.fakeBarHeightConstraint.constant = [UIApplication sharedApplication].statusBarFrame.size.height + ((UINavigationController *)self.parentViewController).navigationBar.frame.size.height;
}

- (void)updateToolBarrButtons
{
    if (self.viewerType == ViewerTypeRemoteCamera) {
        self.leftToolbarItem.image = [UIImage imageNamed:@"ic_export"];
        self.rightToolBarItem.image = [UIImage imageNamed:@"icn_bean"];
    } else if (self.viewerType == ViewerTypeLocalBestOf) {
        self.leftToolbarItem.image = [UIImage imageNamed:@"ic_blocks"];
        self.rightToolBarItem.image = [UIImage imageNamed:@"icn_share"];
    } else {
        self.leftToolbarItem.image = [UIImage imageNamed:@"icn_share"];
        self.rightToolBarItem.image = [UIImage imageNamed:@"icn_bean"];
    }
}

- (void)getImageMetaData:(void (^)())completition
{
    dispatch_queue_t exifQueuq = dispatch_queue_create("com.thinkmobiles.exif.decoding", DISPATCH_QUEUE_CONCURRENT);
    __weak typeof(self) weakSelf = self;
    dispatch_async(exifQueuq, ^() {
        weakSelf.metadataForImage = [weakSelf.thumbnail metadata];
        completition();
    });
}

- (void)getFullResolutionImageForPreview
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{@"Content-Type": @"application/json", @"Accept": @"application/json"}];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[@"http://api.360.tv/" stringByAppendingPathComponent:[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"path_high"]]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0.f];
    [request setHTTPMethod:@"GET"];
    
    self.currentTask = [session downloadTaskWithRequest:request];
    [self.currentTask resume];
}

- (void)showPreview:(UIImage *)image viewerType:(ViewerType)viewerType
{
    PhotoPlayerViewController *player = [self.storyboard instantiateViewControllerWithIdentifier:@"photoPlayer"];
    player.sourceImage = image;
    player.viewerType = viewerType;
    player.detailDataSource = self.dataSource[self.itemIndex];
    player.thumbnail = self.thumbnail;
    if (viewerType == ViewerTypeLocalGallery) {
        player.sourceUrl = ((AssetModel *)player.detailDataSource).sourceUrl;
    }
    [self.navigationController pushViewController:player animated:YES];
}

- (NSString *)iconForType:(NSString *)type
{
    NSString *icon;
    if ([type isEqualToString:@"photo"]) {
        icon = FileTypeIconPhotoNameActive;
    } else if ([type isEqualToString:@"video"]) {
        icon = FileTypeIconVideoNameActive;
    } else if ([type isEqualToString:@"burst"]) {
        icon = FileTypeIconBurstNameActive;
    } else if ([type isEqualToString:@"timelapse"]) {
        icon = FileTypeIconTimeLapseNameActive;
    }
    return icon;
}

- (NSString *)localizedTitleForType:(NSString *)type
{
    NSString *title;
    if ([type isEqualToString:@"photo"]) {
        title = NSLocalizedString(@"article_my_gallerie.photo_title", nil);
    } else if ([type isEqualToString:@"video"]) {
        title = NSLocalizedString(@"article_my_gallerie.video_title", nil);
    } else if ([type isEqualToString:@"burst"]) {
        title = NSLocalizedString(@"article_my_gallerie.burst_title", nil);
    } else if ([type isEqualToString:@"timelapse"]) {
        title = NSLocalizedString(@"article_my_gallerie.timelapse_title", nil);
    }
    return title;
}

- (void)prepareProgressHUD
{
    self.HUD = [[MBProgressHUD alloc] initWithView:[AppHelper topView]];
    [((RootViewController *)[AppHelper rootViewControllerSideMenu]).view addSubview:self.HUD];
    self.HUD.mode = MBProgressHUDModeDeterminate;
    self.HUD.dimBackground = YES;
    self.HUD.labelText = @"Downloading image";
    [self.HUD addCancelButtonForTarger:self andSelector:NSStringFromSelector(@selector(MBProgressHUDCancelButtonDidPressed))];
}

- (void)didSelectBestOfCellAtIndexPath:(NSIndexPath *)indexPAth
{
    self.HUD.progress = 0.f;
    [self.HUD show:YES];
    
    if ([[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"type"] isEqualToString:@"photo"]) {
        [self getFullResolutionImageForPreview];
    } else if ([[[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"type"] isEqualToString:@"video"]) {
        [self performSegueWithIdentifier:VideoPlayerSegue sender:self];
    }
}

- (void)didSelectLocalGalleryCellAtIndexPath:(NSIndexPath *)indexPath
{
    AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
    if ([asset.type isEqualToString:@"photo"]) {
        NSData *imageData = UIImagePNGRepresentation([asset fullScreenImage]);
        UIImage *img = [UIImage imageWithData:imageData];
        [self showPreview:img viewerType:ViewerTypeLocalGallery];
    } else if ([asset.type isEqualToString:@"video"]) {
        [self performSegueWithIdentifier:VideoPlayerSegue sender:self];
    }
}

- (void)gotoNextItem
{
    self.itemIndex ++;
    [self configureNavigationBar];
    if (self.viewerType == ViewerTypeLocalBestOf) {
        [self downloadThumbnailImageAndReloadTable];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        self.thumbnail = [asset fullScreenImage];
        self.metadataForImage = asset.metadata;
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)gotoPreviousItem
{
    self.itemIndex --;
    [self configureNavigationBar];
    if (self.viewerType == ViewerTypeLocalBestOf) {
        [self downloadThumbnailImageAndReloadTable];
    } else if (self.viewerType == ViewerTypeLocalGallery) {
        AssetModel *asset = [self.dataSource objectAtIndex:self.itemIndex];
        self.thumbnail = [asset fullScreenImage];
        self.metadataForImage = asset.metadata;
        [self.tableView beginUpdates];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)configureToolbarIfNeeded
{
    NSDictionary *toolBarActiveTextAttributes = @{NSFontAttributeName : [UIFont camFontZonaProBoldWithSize:10.f],
                                       NSForegroundColorAttributeName : [UIColor colorGalleryToolbarTextColor]};
    NSDictionary *toolBarInactiveTextAttributes = @{NSFontAttributeName : [UIFont camFontZonaProBoldWithSize:10.f],
                                         NSForegroundColorAttributeName : [UIColor colorGalleryToolbarUnactiveTextColor]};
    
    self.previousToolBarItem.enabled = self.itemIndex;
    self.nextToolBarItem.enabled = !(self.itemIndex == self.dataSource.count - 1);
    
    [self.previousToolBarItem setTitleTextAttributes:self.previousToolBarItem.enabled ? toolBarActiveTextAttributes : toolBarInactiveTextAttributes
                                            forState:UIControlStateNormal];
    [self.nextToolBarItem setTitleTextAttributes:self.nextToolBarItem.enabled ? toolBarActiveTextAttributes : toolBarInactiveTextAttributes
                                        forState:UIControlStateNormal];

    if (self.viewerType == ViewerTypeLocalGallery && ![AppHelper isIOS_8]) {
        self.rightToolBarItem.enabled = NO;
    } else if (self.viewerType == ViewerTypeLocalBestOf && self.mediaType == MediaTypeVideo) {
        self.rightToolBarItem.enabled = NO;
    } else if (self.viewerType == ViewerTypeLocalGallery && self.mediaType == MediaTypeVideo) {
        self.leftToolbarItem.enabled = NO;
    }
}

- (void)downloadThumbnailImageAndReloadTable
{
    NSString *baseURL = [[LoadImageManager sharedManager] baseURLString];
    NSString *imageURL = [NSString stringWithFormat:@"%@%@", baseURL, [[self.dataSource objectAtIndex:self.itemIndex] valueForKey:@"thumb_path"]];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:((RootViewController *)[AppHelper rootViewControllerSideMenu]).view animated:YES];
    hud.color = [UIColor clearColor];
    hud.removeFromSuperViewOnHide = YES;
    __weak typeof(self) weakSelf = self;
    [[LoadImageManager sharedManager] loadImageWithURL:imageURL complemention:^(NSData *imageData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hide:YES];
            self.thumbnail = [UIImage imageWithData:imageData];
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        });
    }];
}

@end