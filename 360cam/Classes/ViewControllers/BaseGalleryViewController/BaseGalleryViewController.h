//
//  BaseGallaryViewController.h
//  360cam
//
//  Created by Norbert Citrak on 6/25/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UniversalSwipableTableViewCell.h"
#import "UINavigationController+Transparent.h"
#import "TouchView.h"
#import "CameraNavigationController.h"

static NSString *const CellIdentefier = @"universalSwipableCell";

@interface BaseGalleryViewController : UIViewController <UITableViewDelegate, UniversalSwipableTableViewCellDelegate, CameraNavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *actionImageView;

@property (strong, nonatomic) NSArray *demoDataSource;

@property (assign, nonatomic) MediaFileType selectedMediaType;

- (UIImage *)getImageForAssetType:(NSString *)type;
- (void)setGalleryTitle:(NSString *)title;
- (void)localizeUI;

- (IBAction)photosButtonAction:(id)sender;
- (IBAction)videosButtonAction:(id)sender;

- (BOOL)isTutorialHidden;
- (void)tutorialFinished;
- (void)configureCell:(UniversalSwipableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
