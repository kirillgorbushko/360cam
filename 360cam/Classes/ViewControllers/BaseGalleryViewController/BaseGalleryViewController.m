//
//  BaseGallaryViewController.m
//  360cam
//
//  Created by Norbert Citrak on 6/25/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "BaseGalleryViewController.h"
#import "Animation.h"
#import "RootViewController.h"

#import "BestOfViewController.h"
#import "MyGalleryViewController.h"

static NSString *const VideoIconName = @"ic_video_blue";
static NSString *const PhotoIconName = @"ic_camera_blue";
static NSString *const SwipeTutorialLeftImage = @"icn_arrow_left";
static NSString *const SwipeTutorialRightImage = @"icn_arrow_right";

static CGFloat const DefaultMultiplierForCellHeight = 0.275f;

@interface BaseGalleryViewController ()

@property (weak, nonatomic) IBOutlet UIButton *photosButton;
@property (weak, nonatomic) IBOutlet UIButton *videosButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet TouchView *tutorialView;

@property (strong, nonatomic) NSIndexPath *prevOpenedCell;
@property (assign, nonatomic) BOOL shouldBlockUIElements;

@end

@implementation BaseGalleryViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNib];
    [self setStartParameters];
    
    if ([self shouldShowTutorial]) {
        [self addTutorialTapGesture];
        [self createDemoDataSource];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self shouldShowTutorial]) {
        [self showTutorial];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
    [openedCell hideCellButtons:nil];
}

#pragma mark - IBActions

- (IBAction)photosButtonAction:(id)sender
{
    [self setActiveMediaType:MediaFileTypePhotos];
    [self animateIndicatorViewMovingToXPosition:self.photosButton.center.x];
}

- (IBAction)videosButtonAction:(id)sender
{
    [self setActiveMediaType:MediaFileTypeVideos];
    [self animateIndicatorViewMovingToXPosition:self.videosButton.center.x];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CGRectGetHeight([UIScreen mainScreen].bounds) * DefaultMultiplierForCellHeight;
}

#pragma mark - CameraNavigationControllerDelegate

- (void)menuWillShown
{
    if (self.prevOpenedCell) {
        UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
        [openedCell hideCellButtons:nil];
    }
}

#pragma mark - UniversalSwipableTableViewCellDelegate

- (void)swipeCellDidOpened:(UniversalSwipableTableViewCell *)cell
{
    NSIndexPath *openedIndexPath = [self.tableView indexPathForCell:cell];
    self.prevOpenedCell = openedIndexPath;
    if (!self.tutorialView.hidden) {
        self.tutorialView.blockAllTouch = YES;
    }
}

- (void)swipeCellDidClosed:(UniversalSwipableTableViewCell *)cell
{
    self.prevOpenedCell = nil;
}

- (void)swipeCellDidBeginSwipe:(UniversalSwipableTableViewCell *)cell
{
    self.tutorialView.gestureRecognizers = nil;
    NSIndexPath *activeCell = [self.tableView indexPathForCell:cell];
    if (self.prevOpenedCell && activeCell != self.prevOpenedCell) {
        UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
        [openedCell hideCellButtons:nil];
    }
}

- (void)swipeCellLeftSwipeDidEnd:(UniversalSwipableTableViewCell *)cell
{
    if (!self.tutorialView.hidden) {
        __weak typeof(self) weakSelf = self;
        void (^HideButtonsCompleted)(BOOL) = ^(BOOL completed) {
            if (completed) {
                weakSelf.tutorialView.blockAllTouch = NO;
            }
        };
        [cell performSelector:@selector(hideCellButtons:) withObject:HideButtonsCompleted afterDelay:0.25f];
        
        if ([self isKindOfClass:[MyGalleryViewController class]] && [AppHelper isIOS_8]) {
            [self setSwipeTutorialLabelTextForDelete];
            [self setDeleteSwipeActive];
        } else {
            [self dismissTutorial];
        }
    }
}

- (void)swipeCellRightSwipeDidEnd:(UniversalSwipableTableViewCell *)cell
{
    if (!self.tutorialView.hidden) {
        __weak typeof(self) weakSelf = self;
        void (^HideButtonsCompleted)(BOOL) = ^(BOOL completed) {
            if (completed) {
                weakSelf.tutorialView.blockAllTouch = NO;
            }
        };
        [cell performSelector:@selector(hideCellButtons:) withObject:HideButtonsCompleted afterDelay:0.25f];
        [self dismissTutorial];
    }
}

- (void)setSwipeTutorialLabelTextForDelete
{
    __weak typeof(self) weakSelf = self;
    CGFloat animDuration = 0.25f;
    [UIView animateWithDuration:animDuration animations:^{
        weakSelf.instructionLabel.text = NSLocalizedString(@"swipe_tutorial.left", nil);
    } completion:^(BOOL finished) {
        CABasicAnimation* fadeAnim = [CABasicAnimation animationWithKeyPath:@"contents"];
        fadeAnim.fromValue = (__bridge id __nullable)(weakSelf.actionImageView.image.CGImage);
        fadeAnim.toValue = (__bridge id __nullable)[UIImage imageNamed:SwipeTutorialLeftImage].CGImage;
        fadeAnim.duration = animDuration / 2;
        fadeAnim.removedOnCompletion = YES;
        [weakSelf.actionImageView.layer addAnimation:fadeAnim forKey:nil];
        weakSelf.actionImageView.image = [UIImage imageNamed:SwipeTutorialLeftImage];
    }];
}

- (void)swipeCellDidCancel:(UniversalSwipableTableViewCell *)cell
{
    self.tutorialView.blockAllTouch = NO;
}

#pragma mark - Gesture

- (void)addTutorialTapGesture
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTutorialView:)];
    [self.tutorialView addGestureRecognizer:tapGesture];
}

- (void)didTapOnTutorialView:(UITapGestureRecognizer *)gesture
{
    gesture.enabled = NO;
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = YES;
    cell.disableRightSwipe = YES;
    __weak typeof(self) weakSelf = self;
    [cell showAndHideLeftButtonWithCompletion:^{
        if ([weakSelf isKindOfClass:[MyGalleryViewController class]] && [AppHelper isIOS_8]) {
            [weakSelf setSwipeTutorialLabelTextForDelete];
            [cell showAndHideRightButtonWithCompletion:^{
                [weakSelf dismissTutorial];
            }];
        } else {
            [weakSelf dismissTutorial];
        }
    }];
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.tutorialView.layer animationForKey:@"hideTutorialView"]) {
        [self.tutorialView.layer removeAllAnimations];
        self.tutorialView.hidden = YES;
        [self unblockUI];
        [self setSwipeActive];
    }
}

#pragma mark - Tutorial

- (void)showTutorial
{
    [self blockUI];
    [self setShareSwipeActive];
    [self setupShapeLayerForTutorial];
}

- (void)dismissTutorial
{
    if ([self isKindOfClass:[BestOfViewController class]]) {
        [AppHelper setTutorialViewedForKey:KeyIsBestOfTutorialShown];
    } else if ([self isKindOfClass:[MyGalleryViewController class]]) {
        [AppHelper setTutorialViewedForKey:KeyIsMyGalleryTutorialShown];
    }

    [self.tutorialView.layer addAnimation:[Animation fadeAnimFromValue:1. to:0 delegate:self] forKey:@"hideTutorialView"];
    self.tutorialView.layer.opacity = 0.f;
}

- (void)setupShapeLayerForTutorial
{
    CGRect tableViewRect = self.tableView.frame;
    CGFloat cellHeight = (int)([UIScreen mainScreen].bounds.size.height * DefaultMultiplierForCellHeight);
    CGRect secondCellRect = CGRectMake(tableViewRect.origin.x, tableViewRect.origin.y + cellHeight, tableViewRect.size.width, cellHeight);
    
    self.tutorialView.activeRect = secondCellRect;
    
    CGRect topRect = CGRectMake(0, 0, tableViewRect.size.width, tableViewRect.origin.y + cellHeight + 1);
    CGRect bottomRect = CGRectMake(tableViewRect.origin.x,  tableViewRect.origin.y + cellHeight * 2 - 1, tableViewRect.size.width, [UIScreen mainScreen].bounds.size.height - tableViewRect.origin.y + cellHeight * 2);
    
    UIBezierPath *topPath = [UIBezierPath bezierPathWithRect:topRect];
    UIBezierPath *bottomPath = [UIBezierPath bezierPathWithRect:bottomRect];
    [topPath appendPath:bottomPath];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = topPath.CGPath;
    self.tutorialView.layer.mask = maskLayer;
    
    self.tutorialView.hidden = NO;
    [self.tutorialView.layer addAnimation:[Animation fadeAnimFromValue:0 to:1 delegate:nil] forKey:nil];
    self.tutorialView.layer.opacity = 1.f;
}


- (void)setShareSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableRightSwipe = YES;
    cell.disableLeftSwipe = NO;
}

- (void)setDeleteSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = YES;
    cell.disableRightSwipe = NO;
}

- (void)setSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = NO;
    cell.disableRightSwipe = NO;
}

- (UniversalSwipableTableViewCell *)getSecondCell
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    return (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
}

- (void)blockUI
{
    self.shouldBlockUIElements = YES;
    self.tableView.scrollEnabled = NO;
    self.tableView.allowsSelection = NO;
    [self getSecondCell].lockButtons = YES;
    [self.navigationController hideTransparentNavigationBarAnimated:NO];
    ((RootViewController *)[AppHelper rootViewControllerSideMenu]).panGestureEnabled = NO;
}

- (void)unblockUI
{
    self.shouldBlockUIElements = NO;
    self.tableView.scrollEnabled = YES;
    self.tableView.allowsSelection = YES;
    
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.lockButtons = NO;
    cell.disableLeftSwipe = NO;
    cell.disableRightSwipe = NO;
    
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    ((RootViewController *)[AppHelper rootViewControllerSideMenu]).panGestureEnabled = YES;
    [self tutorialFinished];
}

- (BOOL)shouldShowTutorial
{
    BOOL needToShowTutorial = NO;
    if ([self isKindOfClass:[BestOfViewController class]]) {
        if (![AppHelper isTutorialViewedForKey:KeyIsBestOfTutorialShown]) {
            needToShowTutorial = YES;
        }
    } else if ([self isKindOfClass:[MyGalleryViewController class]]) {
        if (![AppHelper isTutorialViewedForKey:KeyIsMyGalleryTutorialShown]) {
            needToShowTutorial = YES;
        }
    }
    return  needToShowTutorial;
}

- (BOOL)isTutorialHidden
{
    return ![self shouldShowTutorial];
}

- (void)tutorialFinished
{
    // override
}

- (void)createDemoDataSource
{
    NSMutableArray *tmpArray = [NSMutableArray array];
    for (int i = 1; i < 5; i++) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:[@"Demo" stringByAppendingFormat:@"%i", i] ofType:@"jpg"];
        [tmpArray addObject:[UIImage imageWithContentsOfFile:imagePath]];
    }
    self.demoDataSource = tmpArray;
}

- (void)configureCell:(UniversalSwipableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.thumbnailImageView.image = self.demoDataSource[indexPath.row];
    cell.delegate = self;
}

#pragma mark - Private

- (void)animateIndicatorViewMovingToXPosition:(CGFloat)Xposition
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.lineView .center.x, self.lineView .center.y)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(Xposition, self.lineView .center.y)];
    animation.duration = 0.3f;
    [self.lineView.layer addAnimation:animation forKey:nil];
    self.lineView.layer.position = [animation.toValue CGPointValue];
}

- (void)setActiveMediaType:(MediaFileType)type
{
    if (type) {
        self.videosButton.enabled = NO;
        self.photosButton.enabled = YES;
        self.selectedMediaType = MediaFileTypeVideos;
    } else {
        self.photosButton.enabled = NO;
        self.videosButton.enabled = YES;
        self.selectedMediaType = MediaFileTypePhotos;
    }
}

- (void)setStartParameters
{
    [self setActiveMediaType:MediaFileTypePhotos];
    
    [self setStyleForButton:self.photosButton];
    [self setStyleForButton:self.videosButton];
    
    ((CameraNavigationController *)self.navigationController).subDelegate = self;
}

- (void)setStyleForButton:(UIButton *)button
{
    button.layer.borderWidth = 1.f;
    button.layer.borderColor = [UIColor greyBorderColor].CGColor;
}

- (UIImage *)getImageForAssetType:(NSString *)type
{
    UIImage *image;
    if ([type isEqualToString:@"photo"]) {
        image = [UIImage imageNamed:PhotoIconName];
    } else if ([type isEqualToString:@"video"]) {
        image = [UIImage imageNamed:VideoIconName];
    }
    return image;
}

- (void)localizeUI
{
    [self.videosButton setTitle:NSLocalizedString(@"local.base_gallary.video.button.title", nil) forState:UIControlStateNormal];
    [self.photosButton setTitle:NSLocalizedString(@"local.base_gallary.photos.button.title", nil) forState:UIControlStateNormal];
    self.instructionLabel.text = NSLocalizedString(@"swipe_tutorial.right", nil);
}

- (void)setGalleryTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (void)registerNib
{
    [self.tableView registerNib:[UINib nibWithNibName:@"UniversalSwipableTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentefier];
}

@end
