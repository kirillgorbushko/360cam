//
//  ViewModeViewController.m
//  360cam
//
//  Created by Mike on 7/1/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ModeSelectionViewController.h"
#import "ModeSelectionCollectionViewCell.h"
#import "Animation.h"

static NSString *const HorizontalCellIdentifier = @"HorizontalCell";
static NSString *const VerticalCellIdentifier = @"VerticalCell";

static NSString *const SelectedImageKey = @"SelectedImage";
static NSString *const DeselectedImageKey = @"DeselectedImage";

static CGFloat const VerticalCellLeftInsetPersentage = 0.125;

@interface ModeSelectionViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *learnMoreButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopSpaceConstraint;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) CGFloat cellHeightVertical;
@property (assign, nonatomic) NSUInteger lastselectedCellIndex;

@end

@implementation ModeSelectionViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self localizeString];
    [self prepareDataSource];
    [self setScrollDirection];
    [self updateUIIfNeededWithAnimationDuration:0. withInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.layer.opacity = 0.f;
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
    self.view.layer.opacity = 1.f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.lastselectedCellIndex = self.activeModel;
}

#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self deselectCellAtIndex:self.lastselectedCellIndex];
    [self deselectCellAtIndex:self.activeModel];
    [self changeScrollDircetion];
    [self.collectionView reloadData];
    
    [self updateUIIfNeededWithAnimationDuration:duration withInterfaceOrientation:toInterfaceOrientation];
}

- (void)canRotate
{
    //dummy
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ModeSelectionCollectionViewCell *cell;
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:HorizontalCellIdentifier forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:VerticalCellIdentifier forIndexPath:indexPath];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        CGFloat width = CGRectGetWidth(self.collectionView.frame) / self.dataSource.count - 1;
        return CGSizeMake(width, CGRectGetHeight(self.collectionView.frame) - 1);
    } else {
        return CGSizeMake(CGRectGetWidth(self.collectionView.frame) * (1 - VerticalCellLeftInsetPersentage), CGRectGetHeight(self.collectionView.frame) * 0.15f);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        return UIEdgeInsetsZero;
    } else {
        return UIEdgeInsetsMake(0, CGRectGetWidth(self.collectionView.frame) * VerticalCellLeftInsetPersentage, 0.0, 0.0);
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectCellAtIndex:self.activeModel];
    [self selectCellAtIndex:indexPath.row];
    self.activeModel = indexPath.row;
    
    NSInteger selectedMode = SphericalModel;
    switch (indexPath.row) {
        case 0: {
            selectedMode = SphericalModel;
            break;
        }
        case 1: {
            selectedMode = PlanarModel;
            break;
        }
        case 2: {
            selectedMode = LittlePlanetModel;
            break;
        }
        case 3: {
            selectedMode = -1;
            break;
        }
        default:
            return;
            break;
    }

    if (self.didSelectMode) {
        self.didSelectMode(selectedMode);
    }
    [self closeViewControllerAction:self];
}

#pragma mark - IBAction

- (IBAction)closeViewControllerAction:(id)sender
{
    if (self.didCloseView) {
        self.didCloseView();
    }
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1.f to:0.f delegate:self] forKey:@"hideView"];
    self.view.layer.opacity = 0.f;
}

- (IBAction)learnMoreButtonPressed:(id)sender
{
    
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"hideView"]) {
        [self.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Private

- (void)selectCellAtIndex:(NSInteger)index
{
    ModeSelectionCollectionViewCell *cell = (ModeSelectionCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    cell.descriptionLabel.textColor = [UIColor lightBlueViewModeColor];
    cell.mainImage.image = [UIImage imageNamed:self.dataSource[index][SelectedImageKey]];
}

- (void)deselectCellAtIndex:(NSInteger)index
{
    ModeSelectionCollectionViewCell *cell = (ModeSelectionCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    cell.descriptionLabel.textColor = [UIColor whiteColor];
    cell.mainImage.image = [UIImage imageNamed:self.dataSource[index][DeselectedImageKey]];
}

- (void)configureCell:(ModeSelectionCollectionViewCell *)cell atIndexPath:(NSIndexPath*)indexPath
{
    cell.descriptionLabel.text = NSLocalizedString(self.dataSource[indexPath.row][@"Title"], nil);
    if (self.activeModel == indexPath.row) {
        cell.mainImage.image = [UIImage imageNamed:self.dataSource[indexPath.row][SelectedImageKey]];
        cell.descriptionLabel.textColor = [UIColor lightBlueViewModeColor];
    } else {
        cell.mainImage.image = [UIImage imageNamed:self.dataSource[indexPath.row][DeselectedImageKey]];
    }
}

- (void)localizeString
{
    self.titleLabel.text = NSLocalizedString(@"viewMode.title", nil);
    [self.learnMoreButton setTitle: NSLocalizedString(@"viewMode.bottom", nil) forState:UIControlStateNormal];
}

- (void)prepareDataSource
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ViewMode" ofType:@"plist"];
    self.dataSource = [[NSArray alloc] initWithContentsOfFile:path];
}

- (void)setScrollDirection
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    } else {
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
}

- (void)changeScrollDircetion
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    [layout invalidateLayout];
    
    if(layout.scrollDirection) {
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    } else {
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
}

- (void)updateUIIfNeededWithAnimationDuration:(NSTimeInterval)animationDuration withInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect screenRect = [UIScreen mainScreen].bounds;
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
            weakSelf.collectionViewTopSpaceConstraint.constant = CGRectGetHeight(screenRect) * 0.2;
        } else {
            weakSelf.collectionViewTopSpaceConstraint.constant = CGRectGetHeight(screenRect) * 0.15;
        }
    }];
}

@end