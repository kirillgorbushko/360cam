//
//  ViewModeViewController.h
//  360cam
//
//  Created by Mike on 7/1/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SPHBaseViewController.h"

@interface ModeSelectionViewController : UIViewController

@property (strong, nonatomic) void (^didSelectMode)(SPHViewModel model);
@property (strong, nonatomic) void (^didCloseView)();

@property (assign, nonatomic) SPHViewModel activeModel;

@end
