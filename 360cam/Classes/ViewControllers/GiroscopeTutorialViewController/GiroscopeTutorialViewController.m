//
//  GiroscopeTutorialViewController.m
//  360cam
//
//  Created by Kirill Gorbushko on 10.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "GiroscopeTutorialViewController.h"

@interface GiroscopeTutorialViewController ()

@property (weak, nonatomic) IBOutlet UIView *contentHolderView;
@property (weak, nonatomic) IBOutlet UIImageView *tutorialImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet SwitchView *giroSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTextTopSpace;

@property (strong, nonatomic) NSArray *animationImagesForActiveGyroState;
@property (strong, nonatomic) NSArray *animationImagesForInactiveGyroState;

@end

@implementation GiroscopeTutorialViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self performUILocalization];
    [self prepareDataSourceForAnimations];
    [self updateUIForInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    self.giroSwitch.delegate = self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.giroSwitch setNeedsToDisplay];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.giroSwitch setOn:self.currentGyroState animated:YES];
    [self registerForNotification];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startAnimation];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.tutorialImageView stopAnimating];
    [self removeNotifications];
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self updateUIForInterfaceOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self.giroSwitch setNeedsToDisplay];
    [self.giroSwitch setOn:self.currentGyroState animated:YES];
}

#pragma mark - IbActions

- (IBAction)closeButtonPress:(id)sender
{
    if (self.didCloseViewController) {
        self.didCloseViewController();
    }

    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - SwitchViewDelegate

- (void)switchViewDidChangeState:(BOOL)switchState
{
    if (self.didSwitchGiro) {
        self.didSwitchGiro(switchState);
    }
    self.currentGyroState = !self.currentGyroState;
    self.descriptionLabel.text = [self localizeDescription];
    [self.tutorialImageView stopAnimating];
    [self startAnimation];
}

#pragma mark - Notifications

- (void)registerForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)removeNotifications
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *ex) {
        NSLog(@"Canr remove observer - %@, %s", ex.description, __PRETTY_FUNCTION__);
    }
}

- (void)didEnterBackground
{
    [self.tutorialImageView stopAnimating];
}

- (void)willEnterForeground
{
    [self.tutorialImageView startAnimating];
}

#pragma mark - Private

- (void)startAnimation
{
    NSArray *activeScreens = self.animationImagesForInactiveGyroState;
    if (self.giroSwitch.switchState) {
        activeScreens = self.animationImagesForActiveGyroState;
    }

    self.tutorialImageView.animationImages = activeScreens;
    self.tutorialImageView.animationDuration = 1.f;
    self.tutorialImageView.animationRepeatCount = MAXFLOAT;
    [self.tutorialImageView startAnimating];
}

- (void)prepareDataSourceForAnimations
{
    //for demo purpose only
    self.animationImagesForActiveGyroState = @[[UIImage imageNamed:@"active1"],[UIImage imageNamed:@"active2"]];
    self.animationImagesForInactiveGyroState = @[[UIImage imageNamed:@"inactive1"],[UIImage imageNamed:@"inactive2"],[UIImage imageNamed:@"inactive3"]];
}

- (void)updateUIForInterfaceOrientation:(UIInterfaceOrientation)toOrientation
{
    [self.view layoutIfNeeded];
    NSTimeInterval interval = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:interval animations:^{
        CGSize size = [UIScreen mainScreen].bounds.size;
        BOOL sameOrientation = [UIApplication sharedApplication].statusBarOrientation == toOrientation;
        CGFloat value = size.height;
        if (!sameOrientation) {
            value = size.width;
        }
        
        //float values for constant - due to design - no aspect and proportion saved
        if (UIInterfaceOrientationIsPortrait(toOrientation)) {
            weakSelf.topSpaceImageView.constant = value * .17f;
            weakSelf.descriptionTextTopSpace.constant = value * .07f;
        } else {
            weakSelf.topSpaceImageView.constant = value * .12f;
            weakSelf.descriptionTextTopSpace.constant = value * .04f;
        }
        
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)performUILocalization
{
    self.titleLabel.text = NSLocalizedString(@"giroscope_tutorial.title", nil);
    self.descriptionLabel.text = [self localizeDescription];
    self.giroSwitch.activeStateTitle = NSLocalizedString(@"giroscope_tutorial.button.active", nil);
    self.giroSwitch.inactiveStateTitle = NSLocalizedString(@"giroscope_tutorial.button.inactive", nil);
}

- (NSString *)localizeDescription
{
    NSString *description = NSLocalizedString(@"giroscope_tutorial.inactiveGyro.description", nil);
    if (self.currentGyroState) {
        description = NSLocalizedString(@"giroscope_tutorial.activeGyro.description", nil);
    }
    return description;
}

@end