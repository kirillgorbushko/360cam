//
//  GiroscopeTutorialViewController.h
//  360cam
//
//  Created by Kirill Gorbushko on 10.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//
#import "SwitchView.h"

@interface GiroscopeTutorialViewController : UIViewController <SwitchViewDelegate>

@property (strong, nonatomic) void (^didSwitchGiro)(BOOL state);
@property (strong, nonatomic) void (^didCloseViewController)();

@property (assign, nonatomic) BOOL currentGyroState;

@end
