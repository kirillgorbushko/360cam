//
//  HomeViewController.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "HomeViewController.h"
#import "RootViewController.h"
#import "HomeTableViewCell.h"
#import "UINavigationController+RightButton.h"
#import "BadgeLabel.h"

static NSString *const CellIdentifier = @"TableViewCell";

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) NSInteger cellSize;

@end

@implementation HomeViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSource];
    [self initBarButtons];
}

- (void)viewDidLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self prepareCellZise];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellSize;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSInteger destinationControllerIndex = -1;
    switch (indexPath.row) {
        case 0:
            destinationControllerIndex = 3;
            break;
        case 1:
            destinationControllerIndex = 4;
            break;
        case 2:
            destinationControllerIndex = 6;
            break;
        default:
            break;
    }
    if (destinationControllerIndex >= 0) {
        [[AppHelper rootViewControllerSideMenu] navigateToViewControllerAtIndex:destinationControllerIndex];
    }
}

#pragma mark - Action

- (void)curtButtonClickAction:(id)sender
{
    
}

#pragma mark - Private

- (void)configureCell:(HomeTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.descriptionLabel.text = NSLocalizedString(self.dataSource[indexPath.row][@"title"], nil);
    cell.mainImageView.backgroundColor = [UIColor darkGrayForHomePageWithAlpha:0.3f];
    cell.imageLeft.image = [UIImage imageNamed:self.dataSource[indexPath.row][@"leftImage"]];
    
    if ([self.dataSource[indexPath.row][@"background"]  isEqual: @""]) {
        cell.contentView.backgroundColor = [UIColor darkGrayForHomePageWithAlpha:0.9f];
    } else {
        cell.backgroundView = [self backgroundForCell:cell atIndexPath:indexPath];
    }
}

- (UIView *)backgroundForCell:(HomeTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *imageBackground = [[UIImageView alloc] initWithFrame:cell.backgroundView.frame];
    imageBackground.backgroundColor = [UIColor clearColor];
    imageBackground.opaque = NO;
    imageBackground.image = [UIImage imageNamed:self.dataSource[indexPath.row][@"background"]];
    return imageBackground;
}

- (void)initBarButtons
{
    [self.navigationController setButtonWithImageNamed:@"cart" andActionDelegate:self tintColor:[UIColor whiteColor] position:ButtonPositionModeRight selector:@selector(curtButtonClickAction:)];
    BadgeLabel *badgeLabel = [[BadgeLabel alloc] init];
    UIButton *cartButton = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    [badgeLabel addBadgeLabelForView:cartButton withNumber:0];
}

- (void) prepareCellZise
{
    self.cellSize = CGRectGetHeight(self.tableView.frame)/[self.dataSource count] + 1;
}

- (void)prepareDataSource
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Home" ofType:@"plist"];
    self.dataSource = [[NSArray alloc] initWithContentsOfFile:path];
}

@end