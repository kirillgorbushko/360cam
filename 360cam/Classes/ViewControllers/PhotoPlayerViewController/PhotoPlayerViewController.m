//
//  PlayerMobileViewController.m
//  360cam
//
//  Created by Misha Gajdan on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "PhotoPlayerViewController.h"
#import "VideoProgressBarView.h"
#import "ModeSelectionViewController.h"
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"
#import "RemoveFileViewController.h"
#import "ShareImportViewController.h"
#import "GiroscopeTutorialViewController.h"

#import "AssetModel.h"
#import "AssetsManager.h"

static NSUInteger const GradientViewTag = 100;

@interface PhotoPlayerViewController()

@property (assign, nonatomic) SPHViewModel selectedModel;

@end

@implementation PhotoPlayerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initBarButtons];
    [self addTapGesture];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self prepareGradient];
    if (!self.presentedViewController) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self forceRotation];
    [self updateNavigationElements];
}

#pragma mark - Custom Accessors

- (void)setSelectedModel:(SPHViewModel)selectedModel
{
    _selectedModel = selectedModel;
    [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:!selectedModel];
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)forceRotation
{
    [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self prepareGradient];
}

#pragma mark - IBAction

- (IBAction)backButtonClickAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapOnScreen
{
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

- (void)viewModeButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"modeSelectionFromPhotoSegue" sender:self];
}

- (void)shareButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"shareFromPhotoSegue" sender:self];
}

- (void)trashButtonClickAction:(id)sender
{
    if (self.viewerType == ViewerTypeLocalGallery) {
        __weak typeof(self) weakSelf = self;
        [[AssetsManager sharedInstance] deleteAsset:self.sourceUrl withCompletitionHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    [weakSelf updateNavigationElements];
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                }
            });
        }];
    } else {
        [self performSegueWithIdentifier:@"removeFromPhotoSegue" sender:self];
    }
}

- (void)gyroButtonPressed:(id)sender
{
    [self performSegueWithIdentifier:@"giroTutorialFromPhoto" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"shareFromPhotoSegue"]) {
        [self prepareToPresentShareScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"removeFromPhotoSegue"]) {
        [self prepareToPresentRemoveFileScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"modeSelectionFromPhotoSegue"]) {
        [self prepareToPresentModeSelectionScreenScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"giroTutorialFromPhoto"]) {
        [self prepareToPresentGyroTutorialScreenScreenWithSegue:segue];
    }
}

#pragma mark - Private

- (void)prepareToPresentGyroTutorialScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    __weak typeof(self) weakSelf = self;
    GiroscopeTutorialViewController *giroTutorialViewController = segue.destinationViewController;
    giroTutorialViewController.currentGyroState = self.isGyroscopeActive;
    giroTutorialViewController.didSwitchGiro = ^(BOOL state) {
        [weakSelf setGyroscopeActive:state];
    };
    giroTutorialViewController.didCloseViewController = ^() {
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
}

- (void)prepareToPresentModeSelectionScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    __weak typeof(self) weakSelf = self;
    void (^SelectModel)(NSInteger model) = ^(NSInteger model){
        [self setGyroscopeActive:NO];
        [weakSelf switchToModel:model];
        weakSelf.selectedModel = model;
    };
    
    ModeSelectionViewController *controller = segue.destinationViewController;
    controller.didSelectMode = ^(SPHViewModel model) {
        SelectModel(model);
    };
    controller.didCloseView = ^() {
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
    controller.activeModel = self.selectedModel;
}

- (void)prepareToPresentRemoveFileScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    RemoveFileViewController *removeFileVC = segue.destinationViewController;
    removeFileVC.sourceUrl = self.sourceUrl;
}

- (void)prepareToPresentShareScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UINavigationController *shareNavigationController = segue.destinationViewController;
    ShareImportViewController *controller = (ShareImportViewController *)shareNavigationController.topViewController;
    controller.thumbnail = self.thumbnail;
    controller.detailDataSource = self.detailDataSource;
    controller.fullResolutionImage = self.sourceImage;
    __weak typeof(self) weakSelf = self;
    controller.didCloseViewController = ^() {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
}

- (void)initBarButtons
{
    NSArray *buttonIcons = [[NSArray alloc] init];
    NSArray *buttonsColor = [[NSArray alloc] init];
    NSArray *buttonsSelectors = [[NSArray alloc] init];
    
    if (self.viewerType == ViewerTypeLocalBestOf) {
        buttonIcons = @[@"ic_eye_port_2", @"ic_share_port", @"ic_gyro"];
        buttonsColor = @[ [UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor]];
        buttonsSelectors = @[@"viewModeButtonClickAction:", @"shareButtonClickAction:", @"gyroButtonPressed:"];
    } else {
        if (self.viewerType == ViewerTypeLocalGallery && [((AssetModel *)self.detailDataSource) isLittlePlanetPhoto]) {
            buttonIcons = @[@"ic_share_port", @"ic_trash_port"];
            buttonsColor = @[ [UIColor whiteColor],[UIColor whiteColor]];
            buttonsSelectors = @[@"shareButtonClickAction:", @"trashButtonClickAction:"];
            [self switchToModel:PlanarModel];
            self.sourceUrl = ((AssetModel *)self.detailDataSource).sourceUrl;
        } else {
            buttonIcons = @[@"ic_eye_port_2", @"ic_share_port", @"ic_trash_port", @"ic_gyro"];
            buttonsColor = @[ [UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor],[UIColor whiteColor]];
            buttonsSelectors = @[@"viewModeButtonClickAction:", @"shareButtonClickAction:", @"trashButtonClickAction:", @"gyroButtonPressed:"];
        }
    }
    
    [self.navigationController setButtonsWithImageNamed:buttonIcons
                                      andActionDelegate:self
                                             tintColors:buttonsColor
                                               position:ButtonPositionModeRight
                          selectorsStringRepresentation:buttonsSelectors
                                            buttonWidth:40.f];
}

- (void)prepareGradient
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    
    CAGradientLayer *headerGradient = [CAGradientLayer layer];
    headerGradient.frame = self.navigationController.navigationBar.bounds;
    headerGradient.colors = @[(id)[UIColor blackTransparentColor].CGColor, (id)[UIColor clearColor].CGColor];
    
    UIGraphicsBeginImageContext(self.navigationController.navigationBar.bounds.size);
    [headerGradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    view.image = viewImage;
    view.tag = GradientViewTag;
    [self.navigationController.navigationBar insertSubview:view atIndex:0];
}

- (void)addTapGesture
{
    UITapGestureRecognizer *tapOnScreenGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen)];
    [self.view addGestureRecognizer:tapOnScreenGesture];
}

- (void)updateNavigationElements
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

@end