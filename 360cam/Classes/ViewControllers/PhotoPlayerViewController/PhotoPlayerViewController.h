//
//  PlayerMobileViewController.h
//  360cam
//
//  Created by Misha Gajdan on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SPHPhotoViewController.h"
#import "MediaFileDetailsViewController.h"

@interface PhotoPlayerViewController : SPHPhotoViewController

@property (assign, nonatomic) ViewerType viewerType;
@property (strong, nonatomic) NSDictionary *detailDataSource;
@property (strong, nonatomic) UIImage *thumbnail;
@property (strong, nonatomic) NSURL *sourceUrl;

@end
