//
//  RemoteSDGalleryViewController.h
//  360cam
//
//  Created by Kirill Gorbushko on 30.06.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "UniversalSwipableTableViewCell.h"
#import "CameraNavigationController.h"

@interface RemoteCameraSDGalleryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UniversalSwipableTableViewCellDelegate, CameraNavigationControllerDelegate>

@end
