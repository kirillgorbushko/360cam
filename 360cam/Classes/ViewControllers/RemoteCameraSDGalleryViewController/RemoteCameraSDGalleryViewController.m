//
//  RemoteSDGalleryViewController.m
//  360cam
//
//  Created by Kirill Gorbushko on 30.06.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "RemoteCameraSDGalleryViewController.h"
#import "RemoveFileViewController.h"
#import "TouchView.h"
#import "Animation.h"
#import "UINavigationController+Transparent.h"
#import "RootViewController.h"
#import "MediaFileDetailsViewController.h"

static NSString *const CellIdentefier = @"universalSwipableCell";
static NSString *const DetailedSegueIdentifier = @"showMediaFileDetailsPlaybackSegue";

static NSString *const FileTypeIconVideoName = @"ic_video";
static NSString *const FileTypeIconPhotoName = @"ic_photo";
static NSString *const FileTypeIconBurstName = @"ic_burst";
static NSString *const FileTypeIconTimeLapseName = @"ic_timelapse";

static NSString *const SwipeTutorialLeftImage = @"icn_arrow_left";
static NSString *const SwipeTutorialRightImage = @"icn_arrow_right";

static NSString *const RemoveFileViewControllerIndentifier = @"removeFile";
static CGFloat const DefaultMultiplierForCellHeight = 0.23f;

@interface RemoteCameraSDGalleryViewController ()

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cameraModelNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet TouchView *tutorialView;
@property (weak, nonatomic) IBOutlet UIImageView *actionImageView;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

@property (strong, nonatomic) NSIndexPath *prevOpenedCell;
@property (assign, nonatomic) BOOL shouldBlockUIElements;

@end

@implementation RemoteCameraSDGalleryViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNib];
    [self prepareDataSource];
    [self performUILocalization];
    
    if ([self shouldShowTutorial]) {
        [self addTutorialTapGesture];
    }
    
    ((CameraNavigationController *)self.navigationController).subDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self shouldShowTutorial]) {
        [self showTutorial];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (self.prevOpenedCell) {
        UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
        [openedCell hideCellButtons:nil];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UniversalSwipableTableViewCell *cell = (UniversalSwipableTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentefier];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UIScreen mainScreen].bounds.size.height * DefaultMultiplierForCellHeight;
}

- (void)configureCell:(UniversalSwipableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.delegate = self;
    
    cell.cellGalleryType = CellGalleryTypeRemote;
    cell.cellSwipeType = SwipeTypeLeft | SwipeTypeRight;
    [cell setContentForSelectedCellType];
    
    cell.buttonWidth = CGRectGetWidth([UIScreen mainScreen].bounds) * 0.23f;
    
    [cell setLeftSectionBackgroundColor:[UIColor lightBlueButtonBackgroundColor]];
    [cell setRightSectionBackgroundColor:[UIColor yellowButtonBackgroundColor]];
    
    [cell setImageForLeftButton:[UIImage imageNamed:@"ic_export"]];
    [cell setImageForRightButton:[UIImage imageNamed:@"ic_trash"]];
    
    cell.thumbnailImageView.image = [UIImage imageNamed:self.dataSource[indexPath.row]];
    cell.remoteFileDetailsLabel.text = @"TEST";
    cell.remoteFileNameLabel.text = @"TESTTEST";
    cell.remoteFileTypeImageView.image = [UIImage imageNamed:FileTypeIconPhotoName];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tutorialView.isHidden) {
        [self performSegueWithIdentifier:DetailedSegueIdentifier sender:self];
    }
}

#pragma mark - Gesture

- (void)addTutorialTapGesture
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTutorialView:)];
    [self.tutorialView addGestureRecognizer:tapGesture];
}

- (void)didTapOnTutorialView:(UITapGestureRecognizer *)gesture
{
    gesture.enabled = NO;
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = YES;
    cell.disableRightSwipe = YES;
    __weak typeof(self) weakSelf = self;
    [cell showAndHideLeftButtonWithCompletion:^{
        [weakSelf setSwipeTutorialLabelTextForDelete];
        [cell showAndHideRightButtonWithCompletion:^{
            [weakSelf dismissTutorial];
        }];
    }];
}


#pragma mark - UniversalSwipableTableViewCellDelegate

- (void)swipeCellMoreButtonPressed:(UniversalSwipableTableViewCell *)cell
{
    if (!self.shouldBlockUIElements) {
        NSLog(@"More");
    }
}

-(void)swipeCellLeftButtonDidPressed:(UniversalSwipableTableViewCell *)cell
{
    if (!self.shouldBlockUIElements) {
        NSLog(@"Left");
    }
}

- (void)swipeCellRightButtonDidPressed:(UniversalSwipableTableViewCell *)cell
{
    if (!self.shouldBlockUIElements) {
        RemoveFileViewController *removeFileVC = [self.storyboard instantiateViewControllerWithIdentifier:RemoveFileViewControllerIndentifier];
        removeFileVC.modalPresentationStyle = UIModalPresentationCurrentContext;
#ifdef __IPHONE_8_0
        removeFileVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
#endif
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.navigationController presentViewController:removeFileVC animated:NO completion:nil];
    }
}

- (void)swipeCellDidOpened:(UniversalSwipableTableViewCell *)cell
{
    NSIndexPath *openedIndexPath = [self.tableView indexPathForCell:cell];
    self.prevOpenedCell = openedIndexPath;
    
    if (!self.tutorialView.hidden) {
        self.tutorialView.blockAllTouch = YES;
    }
}

- (void)swipeCellDidClosed:(UniversalSwipableTableViewCell *)cell
{
    self.prevOpenedCell = nil;
}

- (void)swipeCellDidBeginSwipe:(UniversalSwipableTableViewCell *)cell
{
    self.tutorialView.gestureRecognizers = nil;
    NSIndexPath *activeCell = [self.tableView indexPathForCell:cell];
    if (self.prevOpenedCell && activeCell != self.prevOpenedCell) {
        UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
        [openedCell hideCellButtons:nil];
    }
}

- (void)swipeCellRightSwipeDidEnd:(UniversalSwipableTableViewCell *)cell
{
    if (!self.tutorialView.hidden) {
        __weak typeof(self) weakSelf = self;
        void (^HideButtonsCompleted)(BOOL) = ^(BOOL completed) {
            if (completed) {
                weakSelf.tutorialView.blockAllTouch = NO;
            }
        };
        [cell performSelector:@selector(hideCellButtons:) withObject:HideButtonsCompleted afterDelay:0.25f];
        [self dismissTutorial];
    }
}

- (void)swipeCellLeftSwipeDidEnd:(UniversalSwipableTableViewCell *)cell
{
    if (!self.tutorialView.hidden) {
        __weak typeof(self) weakSelf = self;
        void (^HideButtonsCompleted)(BOOL) = ^(BOOL completed) {
            if (completed) {
                weakSelf.tutorialView.blockAllTouch = NO;
            }
        };
        [cell performSelector:@selector(hideCellButtons:) withObject:HideButtonsCompleted afterDelay:0.25f];
        [self setSwipeTutorialLabelTextForDelete];
        [self setDeleteSwipeActive];
    }
}

- (void)swipeCellDidCancel:(UniversalSwipableTableViewCell *)cell
{
    self.tutorialView.blockAllTouch = NO;
}

#pragma mark - CameraNavigationControllerDelegate

- (void)menuWillShown
{
    if (self.prevOpenedCell) {
        UniversalSwipableTableViewCell *openedCell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:self.prevOpenedCell];
        [openedCell hideCellButtons:nil];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:DetailedSegueIdentifier]) {
        MediaFileDetailsViewController *mediaDetailsViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        mediaDetailsViewController.dataSource = self.dataSource[indexPath.row];
        mediaDetailsViewController.viewerType = ViewerTypeRemoteCamera;
        UniversalSwipableTableViewCell *cell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        mediaDetailsViewController.thumbnail = cell.thumbnailImageView.image;
    }
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.tutorialView.layer animationForKey:@"hideTutorialView"]) {
        [self.tutorialView.layer removeAllAnimations];
        self.tutorialView.hidden = YES;
        [self unblockUI];
        [self setSwipeActive];
    }
}

#pragma mark - TutorialView

- (void)showTutorial
{
    [self blockUI];
    [self prepareSwipeTutorial];
    [self setShareSwipeActive];
    [self setupShapeLayerForTutorial];
}

- (void)dismissTutorial
{
    [AppHelper setTutorialViewedForKey:KeyIsPlaybackTutorialShown];
    [self.tutorialView.layer addAnimation:[Animation fadeAnimFromValue:1. to:0 delegate:self] forKey:@"hideTutorialView"];
    self.tutorialView.layer.opacity = 0.f;
}

- (void)setupShapeLayerForTutorial
{
    CGRect tableViewRect = self.tableView.frame;
    CGFloat cellHeight = (int)([UIScreen mainScreen].bounds.size.height * DefaultMultiplierForCellHeight);
    CGRect secondCellRect = CGRectMake(tableViewRect.origin.x, tableViewRect.origin.y + cellHeight, tableViewRect.size.width, cellHeight);
    
    self.tutorialView.activeRect = secondCellRect;
    
    CGRect topRect = CGRectMake(0, 0, tableViewRect.size.width, tableViewRect.origin.y + cellHeight + 1);
    CGRect bottomRect = CGRectMake(tableViewRect.origin.x,  tableViewRect.origin.y + cellHeight * 2 - 1, tableViewRect.size.width, [UIScreen mainScreen].bounds.size.height - tableViewRect.origin.y + cellHeight * 2);
    
    UIBezierPath *topPath = [UIBezierPath bezierPathWithRect:topRect];
    UIBezierPath *bottomPath = [UIBezierPath bezierPathWithRect:bottomRect];
    [topPath appendPath:bottomPath];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = topPath.CGPath;
    self.tutorialView.layer.mask = maskLayer;
    
    self.tutorialView.hidden = NO;
    [self.tutorialView.layer addAnimation:[Animation fadeAnimFromValue:0 to:1 delegate:nil] forKey:nil];
    self.tutorialView.layer.opacity = 1.f;
}

- (void)setSwipeTutorialLabelTextForDelete
{
    __weak typeof(self) weakSelf = self;
    CGFloat animDuration = 0.25f;
    [UIView animateWithDuration:animDuration animations:^{
        weakSelf.instructionLabel.text = NSLocalizedString(@"swipe_tutorial.left", nil);
    } completion:^(BOOL finished) {
        CABasicAnimation* fadeAnim = [CABasicAnimation animationWithKeyPath:@"contents"];
        fadeAnim.fromValue = (__bridge id __nullable)(weakSelf.actionImageView.image.CGImage);
        fadeAnim.toValue = (__bridge id __nullable)[UIImage imageNamed:SwipeTutorialLeftImage].CGImage;
        fadeAnim.duration = animDuration / 2;
        fadeAnim.removedOnCompletion = YES;
        [weakSelf.actionImageView.layer addAnimation:fadeAnim forKey:nil];
        weakSelf.actionImageView.image = [UIImage imageNamed:SwipeTutorialLeftImage];
    }];
}

- (void)setShareSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableRightSwipe = YES;
    cell.disableLeftSwipe = NO;
}

- (void)setDeleteSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = YES;
    cell.disableRightSwipe = NO;
}

- (void)setSwipeActive
{
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.disableLeftSwipe = NO;
    cell.disableRightSwipe = NO;
}

- (UniversalSwipableTableViewCell *)getSecondCell
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    return (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
}

- (void)blockUI
{
    self.shouldBlockUIElements = YES;
    self.tableView.scrollEnabled = NO;
    self.tableView.allowsSelection = NO;
    [self getSecondCell].lockButtons = YES;
    [self.navigationController hideTransparentNavigationBarAnimated:NO];
    ((RootViewController *)[AppHelper rootViewControllerSideMenu]).panGestureEnabled = NO;
}

- (void)unblockUI
{
    self.shouldBlockUIElements = NO;
    self.tableView.scrollEnabled = YES;
    self.tableView.allowsSelection = YES;
    
    UniversalSwipableTableViewCell *cell = [self getSecondCell];
    cell.lockButtons = NO;
    cell.disableLeftSwipe = NO;
    cell.disableRightSwipe = NO;
    
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    ((RootViewController *)[AppHelper rootViewControllerSideMenu]).panGestureEnabled = YES;
    [self tutorialFinished];
}

- (BOOL)shouldShowTutorial
{
    return  ![AppHelper isTutorialViewedForKey:KeyIsPlaybackTutorialShown];
}

- (void)tutorialFinished
{
    
}


#pragma mark - Private


- (void)performUILocalization
{
    self.titleLabel.text = NSLocalizedString(@"remote.camera.gallery_SD.title", nil);
    NSString *cameraModel = [self getCameraModelName];
    self.cameraModelNameLabel.text = [NSLocalizedString(@"remote.camera.gallery_SD_subtitle", nil) stringByAppendingString:cameraModel];
}

- (void)prepareSwipeTutorial
{
    self.instructionLabel.text = NSLocalizedString(@"swipe_tutorial.right", nil);
    self.actionImageView.image = [UIImage imageNamed:SwipeTutorialRightImage];
}

- (NSString *)getCameraModelName
{
    //demo
    return @"0230D32";
}

- (void)prepareDataSource
{
    if (!self.dataSource) {
        self.dataSource = [[NSMutableArray alloc] init];
    }
    
    self.dataSource = [@[@"temp_testPlayback", @"temp_testPlayback", @"temp_testPlayback", @"temp_testPlayback"] mutableCopy];
}

- (void)registerNib
{
    [self.tableView registerNib:[UINib nibWithNibName:@"UniversalSwipableTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellIdentefier];
}

@end
