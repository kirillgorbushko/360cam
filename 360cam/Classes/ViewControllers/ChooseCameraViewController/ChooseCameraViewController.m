//
//  ChooseCameraViewController.m
//  360cam
//
//  Created by Victor on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ChooseCameraViewController.h"
#import "ChooseCameraTableViewCell.h"
#import "ChooseCameraRefreshFooterCell.h"
#import "RemoteCameraHomeViewController.h"
#import "CameraNetworkManager.h"
#import "WiFiFetchService.h"
#import "Camera.h"
#import "Network.h"

static NSString *const ChooseCameraCellIdentifier = @"choseCameraCell";
static NSString *const ChooseCameraRefreshCellIdentifier = @"refreshFooterCell";
static NSString *const AddNewDeviceSegue = @"addNewDeviceSegue";
static NSString *const RemoteCameraSegue = @"remoteCamera";

@interface ChooseCameraViewController ()

@property (weak, nonatomic) IBOutlet UILabel *chooseCameraTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *chooseCameraSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *addNewDeviceButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSFetchedResultsController *fetchResultsController;
@property (assign, nonatomic) NSInteger cellsCount;
@property (copy, nonatomic) NSString *activeCameraId;

@end

@implementation ChooseCameraViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
    [self configureAddNewDeviceButton];
    [self configureRefreshControl];
    [self addWiFiObservers];
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initNewCameraIfNeeded];
    self.navigationItem.title = @"";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[CameraNetworkManager sharedManager] isCameraConnected]) {
        [self cameraConnected];
    }
}

- (void)dealloc
{
    [self removeAllObservers];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo =
    [self.fetchResultsController.sections objectAtIndex:section];
    self.cellsCount = [sectionInfo numberOfObjects];
    return self.cellsCount +1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row < self.cellsCount) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:ChooseCameraCellIdentifier forIndexPath:indexPath];
        [self configureCell:(ChooseCameraTableViewCell*)cell atIndexPath:indexPath];
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:ChooseCameraRefreshCellIdentifier forIndexPath:indexPath];
        [self configureRefreshCell:(ChooseCameraRefreshFooterCell*)cell];
    }
    if (indexPath.row == self.cellsCount + 1) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.f;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteRowFromFetchControllerAtIndex:indexPath];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[ChooseCameraRefreshFooterCell class]]) {
        ChooseCameraTableViewCell *cell = (ChooseCameraTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *segueName;
        if (cell.cameraState == CameraStateEnable) {
            segueName = RemoteCameraSegue;
        } else {
            segueName = AddNewDeviceSegue;
        }
        [self performSegueWithIdentifier:segueName sender:cell];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[ChooseCameraRefreshFooterCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:AddNewDeviceSegue]) {
        UIViewController *destinationController = segue.destinationViewController;
        if ([sender isKindOfClass:[UIButton class]]) {
            destinationController.title = NSLocalizedString(@"add_device.title", nil);
        } else {
            destinationController.title = NSLocalizedString(@"connect_device.title", nil);
        }
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(ChooseCameraTableViewCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation: UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

#pragma mark - Accesors

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchResultsController != nil) {
        return _fetchResultsController;
    }
    CoreDataManager *manager = [CoreDataManager sharedManager];
    NSManagedObjectContext *context = manager.managedObjectContext;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Camera" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    [fetchRequest setFetchBatchSize:20];
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:@"Root"];
    self.fetchResultsController= theFetchedResultsController;
    _fetchResultsController.delegate = self;
    return _fetchResultsController;
}

#pragma mark - IBActions

- (void)handleRefresh
{
    [self performSelector:@selector(hideRefresh) withObject:nil afterDelay:1.f];
}

- (void)hideRefresh
{
    [self.refreshControl endRefreshing];
}
- (IBAction)addNewDeviceClick:(id)sender
{
    [self performSegueWithIdentifier:AddNewDeviceSegue sender:sender];
}

#pragma mark - Private

- (void)deleteRowFromFetchControllerAtIndex:(NSIndexPath *)indexPath
{
    Camera *objectToBeDeleted =
    [self.fetchedResultsController objectAtIndexPath:indexPath];
    CoreDataManager *manager = [CoreDataManager sharedManager];
    [manager.managedObjectContext deleteObject:objectToBeDeleted];
    [manager.managedObjectContext deleteObject:objectToBeDeleted.cameraNetwork];
    [manager saveContext];
}

- (void)cameraDisconnected
{
    if (self.activeCameraId) {
        NSArray *fetchedData = [self.fetchResultsController fetchedObjects];
        for (int i=0; i<fetchedData.count; i++) {
            Camera *camera = fetchedData[i];
            if ([camera.cameraId isEqualToString:self.activeCameraId]) {
                ChooseCameraTableViewCell *cell = (ChooseCameraTableViewCell *)[self.tableView  cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                cell.cameraState = CameraStateDisable;
                self.activeCameraId = nil;
            }
        }
    }
}

- (void)cameraConnected
{
    WiFiFetchService *sharedService=  [WiFiFetchService sharedService];
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    if ([manager isCameraConnected]) {
        NSArray *fetchedData = [self.fetchResultsController fetchedObjects];
        for (int i=0; i<fetchedData.count; i++) {
            Camera *camera = fetchedData[i];
            if ([camera.cameraNetwork.ssId isEqualToString:sharedService.cameraName]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                self.activeCameraId = camera.cameraId;
                [self configureActiveCameraCell:indexPath];
            }
        }
    }
}


- (void)startSessionIfNeededForCameraAtIndex:(NSIndexPath *)indexPath withCameraId:(NSString *)cameraId
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    __weak typeof(self) weakSelf = self;
        [manager giropticGetCameraInfoWithResult:^(BOOL success, id response, NSError *error) {
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        } else {
            ChooseCameraTableViewCell *cell = (ChooseCameraTableViewCell *)[weakSelf.tableView cellForRowAtIndexPath:indexPath];
            cell.cameraState = CameraStateEnable;
            weakSelf.activeCameraId = cameraId;
        }
    }];
}

- (void)configureActiveCameraCell:(NSIndexPath *)indexPath
{
    ChooseCameraTableViewCell *cell = (ChooseCameraTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.cameraState = CameraStateEnable;
}

- (void)initNewCameraIfNeeded
{
    CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
    WiFiFetchService *wiFiService = [WiFiFetchService sharedService];
    if (![self isCameraExistsInCoreData:wiFiService.macAddress] && [manager isCameraConnected]) {
        __weak typeof(self) weakSelf = self;
        [manager giropticGetCameraInfoWithResult:^(BOOL success, id response, NSError *error) {
            if (success) {
                [weakSelf addCameraObjectIntoCoreDataWithData:response andMacAddress:wiFiService.macAddress];
                [weakSelf cameraConnected];
            } else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }
}

- (void)addCameraObjectIntoCoreDataWithData:(NSDictionary *)data andMacAddress:(NSString *)macAddress
{
    WiFiFetchService *serivce = [WiFiFetchService sharedService];
    CoreDataManager *coreDataManager = [CoreDataManager sharedManager];
    NSManagedObjectContext *context = coreDataManager.managedObjectContext;
    Camera *camera = [NSEntityDescription insertNewObjectForEntityForName:@"Camera" inManagedObjectContext:context];
    Network *network = [NSEntityDescription insertNewObjectForEntityForName:@"Network" inManagedObjectContext:context];
    camera.name = serivce.cameraName;
    camera.addedDate = [NSDate new];
    camera.modelType = data[@"model"];
    camera.cameraId = macAddress;
    network.networkCamera = camera;
    network.ssId = serivce.cameraName;
    camera.cameraNetwork = network;
    network.networkCamera = camera;
    [coreDataManager saveContext];

}

- (BOOL)isCameraExistsInCoreData:(NSString *)cameraId
{
    NSArray *fetchedData = [self.fetchResultsController fetchedObjects];
    for (Camera *camera in fetchedData) {
        if (cameraId && [camera.cameraId isEqualToString:cameraId]) {
            return YES;
        }
    }
    return NO;
}

- (void)configureView
{
    self.chooseCameraTitleLabel.text = NSLocalizedString(@"choose_camera.title", nil);
    self.chooseCameraSubtitleLabel.text = NSLocalizedString(@"choose_camera.subtitle", nil);
}

- (void)configureAddNewDeviceButton
{
    self.addNewDeviceButton.layer.borderColor = [UIColor chooseCamLightBlue].CGColor;
    self.addNewDeviceButton.layer.borderWidth = 1.f;
    self.addNewDeviceButton.layer.cornerRadius = self.addNewDeviceButton.frame.size.height / 2;
}

- (void)configureCell:(ChooseCameraTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Camera *managedObject = [self.fetchResultsController objectAtIndexPath:indexPath];
    cell.cameraState = 1;
    cell.chooseCameraNameLabel.text = managedObject.name;
    if (self.activeCameraId && [managedObject.cameraId isEqualToString:self.activeCameraId]) {
        cell.cameraState = CameraStateEnable;
    }
}

- (void)configureRefreshCell:(ChooseCameraRefreshFooterCell *)cell
{
    cell.pullSearchLabel.text = NSLocalizedString(@"choose_camera.pull_to_search", nil);
}

- (void)configureRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(handleRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

#pragma mark - Observer

- (void)addWiFiObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraConnected) name:DeviceDidConnectedToCameraNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraDisconnected) name:DeviceDidDisconnectedToCameraNotification object:nil];
}

- (void)removeAllObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end