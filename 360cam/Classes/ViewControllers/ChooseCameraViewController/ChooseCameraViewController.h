//
//  ChooseCameraViewController.h
//  360cam
//
//  Created by Victor on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CoreDataManager.h"

@interface ChooseCameraViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@end