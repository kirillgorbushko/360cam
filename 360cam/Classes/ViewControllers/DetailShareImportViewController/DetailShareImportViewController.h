//
//  DetailShareImportViewController.h
//  360cam
//
//  Created by Mike on 7/3/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ShareImportViewController.h"
#import "MessageUI/MessageUI.h"

@interface DetailShareImportViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (void)setHeaderImage:(NSString *)headerImage andTitleText:(NSString*)titleText andImage:(UIImage*)image withBackgroundColor:(UIColor *)backgroundColor;

@property (strong, nonatomic) NSDictionary *detailDataSource;

@property (strong, nonatomic) UIImage *thumbnail;
@property (strong, nonatomic) UIImage *fullResolutionImage;

@property (assign, nonatomic) SharingType selectedSharingMode;
@property (assign, nonatomic) ShareOptionType selectedShareOption;

@end
