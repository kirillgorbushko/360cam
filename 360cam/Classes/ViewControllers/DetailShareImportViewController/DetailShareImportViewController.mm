//
//  DetailShareImportViewController.m
//  360cam
//
//  Created by Mike on 7/3/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

// Controllers
#import "DetailShareImportViewController.h"
#import "ShareImportViewController.h"
#import "RootViewController.h"

// Categories
#import "UINavigationController+Transparent.h"
#import "UINavigationController+RightButton.h"
#import "UIImage+OpenCV.h"
#import "UIImage+Resize.h"
#import "UIImage+Rotation.h"

// Managers
#import "AssetsManager.h"
#import "AssetModel.h"

// Views
#import "MBProgressHUD.h"

static NSUInteger const buttonsHeight = 53;

@interface DetailShareImportViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *sharePreviewImageView;

@property (weak, nonatomic) IBOutlet UIButton *flatButton;
@property (weak, nonatomic) IBOutlet UIButton *littlePlanetButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *sliderView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderViewCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topButtonsHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerEqualHeightConstraint;

@property (weak, nonatomic) UIButton *lastClickedButton;

@property (strong, nonatomic) UIImage *headerImage;
@property (strong, nonatomic) UIImage *littlePlanetImage;
@property (strong, nonatomic) UIImage *littlePlanetThumbnailImage;
@property (strong, nonatomic) UIImage *fullScreenImage;

@property (strong, nonatomic) UIColor *headerBackgroundColor;
@property (strong, nonatomic) MBProgressHUD *HUD;

@property (assign, nonatomic, getter=isImport) BOOL import;
@property (assign, nonatomic) BOOL isLittlePlanet;

@end

@implementation DetailShareImportViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    [self setupNavigationBar];
    [self updateHeaderViewToOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [self updateUIToOrientation:[UIApplication sharedApplication].statusBarOrientation];
    [self prepareProgressHUD];
    
    self.lastClickedButton = self.flatButton;
    self.isLittlePlanet = NO;
    self.sharePreviewImageView.image = [self flatImageToShare];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
}

#pragma mark - Accessors

- (void)setFullResolutionImage:(UIImage *)fullResolutionImage
{
    _fullResolutionImage = fullResolutionImage;
    self.fullScreenImage = [fullResolutionImage fullScreenImage];
}

- (void)setFullScreenImage:(UIImage *)fullScreenImage
{
    _fullScreenImage = fullScreenImage;
    if (!self.littlePlanetImage && fullScreenImage) {
        if ([self isLittlePlanetPhoto]) {
            self.littlePlanetImage = fullScreenImage;
            return;
        }
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *flippedImage = [fullScreenImage flippedImageHorizontally:YES];
            Mat modImageMat = LittlePlanet([UIImage cvMatFromUIImage:flippedImage]);
            weakSelf.littlePlanetImage = [UIImage UIImageFromCVMat:modImageMat];
        });
    }
}

- (void)setThumbnail:(UIImage *)thumbnail
{
    _thumbnail = thumbnail;
    if (!self.littlePlanetThumbnailImage && thumbnail) {
        if ([self isLittlePlanetPhoto]) {
            self.littlePlanetThumbnailImage = thumbnail;
            return;
        }
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *flippedImage = [thumbnail flippedImageHorizontally:YES];
            Mat modImageMat = LittlePlanet([UIImage cvMatFromUIImage:flippedImage]);
            weakSelf.littlePlanetThumbnailImage = [UIImage UIImageFromCVMat:modImageMat];
        });
    }
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self updateHeaderViewToOrientation:toInterfaceOrientation];
    [self updateUIToOrientation:toInterfaceOrientation];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self updateSliderViewPosition];
}

#pragma mark - IBActions

- (IBAction)clickPlanetButtonAction:(id)sender
{
    [self moveView:self.sliderView toXpoint:self.littlePlanetButton.center.x withAnimationTime:0.2f];
    [self updateClickedButtonAlpha:self.littlePlanetButton];
    self.sharePreviewImageView.image = [self littlePlanetImageToShare];
    self.isLittlePlanet = YES;
}

- (IBAction)clickFlatButtonAction:(id)sender
{
    [self moveView:self.sliderView toXpoint:self.flatButton.center.x withAnimationTime:0.2f];
    [self updateClickedButtonAlpha:self.flatButton];
    self.sharePreviewImageView.image = [self flatImageToShare];
    self.isLittlePlanet = NO;
}

- (void)confirmButtonPressed
{
    if (self.selectedShareOption == ShareOptionTypeShare) {
        if (self.selectedSharingMode == SharingTypeMail) {
            [self shareByMail];
        } else if (self.selectedSharingMode == SharingTypeFacebook) {
            
        } else if (self.selectedSharingMode == SharingTypeLinkedin) {
            
        } else if (self.selectedSharingMode == SharingTypeTwitter) {
            
        } else if (self.selectedSharingMode == SharingTypeGooglePlus) {
            
        }
    } else {
        [self.HUD show:YES];
        NSString *albumName = self.isLittlePlanet ? AlbumNameGiropticLittlePlanetLocalFies : AlbumNameGiropticLocalFies;
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *imageToSave = weakSelf.fullResolutionImage;
            if (weakSelf.isLittlePlanet && ![weakSelf isLittlePlanetPhoto]) {
                UIImage *flippedImage = [imageToSave flippedImageHorizontally:YES];
                Mat modImageMat = LittlePlanet([UIImage cvMatFromUIImage:flippedImage]);
                imageToSave = [UIImage UIImageFromCVMat:modImageMat];
            }
            [[AssetsManager sharedInstance] savePhoto:imageToSave toAlbum:albumName withCompletitionHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.HUD hide:YES];
                    [weakSelf closeShareController];
                });
            }];
        });
    }
}

- (void)closeShareController
{
    [((ShareImportViewController *)[self.navigationController.viewControllers firstObject]) closeViewControllerAction:self];
}

#pragma mark - Public

- (void)setHeaderImage:(NSString *)headerimage andTitleText:(NSString *)titleText andImage:(UIImage *)image withBackgroundColor:(UIColor *)backgroundColor
{
    self.navigationItem.title = titleText;
    if ([titleText isEqualToString:NSLocalizedString(@"share_import.export", nil)]) {
        self.import = YES;
    } else {
        self.import = NO;
        self.headerImage = [UIImage imageNamed:headerimage];
        self.headerBackgroundColor = backgroundColor;
    }
}

#pragma mark - Share

- (UIImage *)flatImageToShare
{
    if (self.fullResolutionImage) {
        return self.fullResolutionImage;
    }
    return self.thumbnail;
}

- (UIImage *)littlePlanetImageToShare
{
    if (self.littlePlanetImage) {
        return self.littlePlanetImage;
    }
    return self.littlePlanetThumbnailImage;
}

- (void)shareByMail
{
    if ([MFMailComposeViewController canSendMail]) {
        [self configureNavigationBarAppearance];

        NSData *imageData = UIImageJPEGRepresentation(self.sharePreviewImageView.image, 1.0f);
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [mailController addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@"Photo"];
        
        [self presentViewController:mailController animated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }];
    } else {
        [AppHelper alertViewWithMessage:NSLocalizedString(@"share_import_detail.setup_email", nil)];
    }
}

#pragma mark - Private

- (BOOL)isLittlePlanetPhoto
{
    if ([self.detailDataSource isKindOfClass:[AssetModel class]]) {
        if ([((AssetModel *)self.detailDataSource) isLittlePlanetPhoto]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - UI

- (void)configureNavigationBarAppearance
{
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor lightGrayShareImport]];
}

- (void)setupNavigationBar
{
    [self.navigationController setButtonWithImageNamed:@"ic_check_land" andActionDelegate:self tintColor:[UIColor whiteColor] position:ButtonPositionModeRight selector:@selector(confirmButtonPressed)];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIFont *titleFont = [UIFont camFontZonaProRegularWithSize:20];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                    NSFontAttributeName : titleFont};
}

- (void)prepareProgressHUD
{
    self.HUD = [[MBProgressHUD alloc] initWithView:[AppHelper topView]];
    [((RootViewController *)[AppHelper rootViewControllerSideMenu]).view addSubview:self.HUD];
    self.HUD.mode = MBProgressHUDModeIndeterminate;
    self.HUD.dimBackground = YES;
    self.HUD.labelText = self.isImport ? @"Saving" : @"Sharing";
}

- (void)updateHeaderViewToOrientation:(UIInterfaceOrientation)orientation
{
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        self.headerImageView.hidden = YES;
    } else {
        self.headerImageView.hidden = NO;
    }
}

- (void)moveView:(UIView *)view toXpoint:(CGFloat)xPoint withAnimationTime:(CGFloat)duration
{
    CGFloat originalY = view.center.y;
    CGFloat originalX = view.center.x;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(originalX, originalY)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(xPoint, originalY)];
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:@"position"];
    view.layer.position = [animation.toValue CGPointValue];
    
}

- (void)updateSliderViewPosition
{
    if (self.lastClickedButton == self.littlePlanetButton) {
        self.sliderViewCenterXConstraint.constant = (CGRectGetWidth([UIScreen mainScreen].bounds) / 2);
    } else {
        self.sliderViewCenterXConstraint.constant = 0;
    }
}

- (void)setExportDesign
{
    self.headerView.backgroundColor = [UIColor blackColor];
    self.headerView.alpha = 0.7;
    self.sliderView.backgroundColor = [UIColor colorLightBlueShareImport];
}

- (void)setShareDesign:(UIColor *)backgroundColor
{
    self.headerImageView.image = self.headerImage;
    self.headerView.backgroundColor = backgroundColor;
    self.headerView.alpha = 0.9;
}

- (void)updateClickedButtonAlpha:(UIButton *)button
{
    if (button != self.lastClickedButton) {
        [self.lastClickedButton setAlpha:1.0];
        [button setAlpha:0.5];
        self.lastClickedButton = button;
    }
}

- (void)setupUI
{
    [self.flatButton setTitle:NSLocalizedString(@"share_import_detail.flat", nil) forState:UIControlStateNormal];
    [self.littlePlanetButton setTitle:NSLocalizedString(@"share_import_detail.littlePlanet", nil) forState:UIControlStateNormal];
    if (self.isImport) {
        [self setExportDesign];
    } else {
        [self setShareDesign:self.headerBackgroundColor];
    }
}

- (void)updateUIToOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    NSTimeInterval interval = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [self.view layoutIfNeeded];
    
    CGSize screen = [UIScreen mainScreen].bounds.size;
    __weak typeof(self) weakSelf = self;
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
        CGFloat lessSide = MIN(screen.width, screen.height);
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [UIView animateWithDuration:interval animations:^{
            weakSelf.topButtonsHeightConstraint.constant = (lessSide * self.headerEqualHeightConstraint.multiplier) * 0.45f;//height of top part * % for buttons
            [weakSelf.view layoutIfNeeded];
        }];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [UIView animateWithDuration:interval animations:^{
            weakSelf.topButtonsHeightConstraint.constant = buttonsHeight;
            [weakSelf.view layoutIfNeeded];
        }];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *alertViewMessage;
    __block BOOL hideShareController = NO;
    switch (result) {
        case MFMailComposeResultFailed:
            alertViewMessage = NSLocalizedString(@"share_import_detail.failed_send_mail", nil);
            break;
        case MFMailComposeResultSent:
            hideShareController = YES;
            break;
        default:
            break;
    }
    __weak typeof(self) weakSelf = self;
    [controller dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        if (alertViewMessage.length) {
            [AppHelper alertViewWithMessage:alertViewMessage];
        }
        if (hideShareController) {
            [weakSelf closeShareController];
        }
    }];
}

@end