//
//  MyGallaryViewController.h
//  360cam
//
//  Created by Norbert Citrak on 7/1/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "BaseGalleryViewController.h"

@interface MyGalleryViewController : BaseGalleryViewController <UITableViewDataSource>

@end
