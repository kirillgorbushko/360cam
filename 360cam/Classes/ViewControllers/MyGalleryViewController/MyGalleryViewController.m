//
//  MyGallaryViewController.m
//  360cam
//
//  Created by Norbert Citrak on 7/1/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

// Controllers
#import "MyGalleryViewController.h"
#import "MediaFileDetailsViewController.h"
#import "RootViewController.h"
#import "ShareImportViewController.h"

// Models
#import "AssetsManager.h"
#import "AssetModel.h"
#import "MBProgressHUD.h"

static NSString *const DetailedSegueIdentifier = @"showMediaFileDetailsMyGallerySegue";
static NSString *const ShareSegueIdentifier = @"shareImportSegue";

@interface MyGalleryViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fakeBarHeightConstraint;

@property (strong, nonatomic) NSArray *dataSource;
@property (assign, nonatomic) MediaFileType mediaType;

@end

@implementation MyGalleryViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self localizeUI];
    [self updateUIIfNeeded];
    [self addObservers];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self isTutorialHidden]) {
        [self reloadData];
    } else {
        self.dataSource = self.demoDataSource;
    }
    self.navigationItem.title = @"";
}

- (void)dealloc
{
    [self removeObservers];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UniversalSwipableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentefier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isTutorialHidden]) {
        [self performSegueWithIdentifier:DetailedSegueIdentifier sender:self];
    }
}

#pragma mark - UniversalSwipableTableViewCellDelegate

- (void)swipeCellLeftButtonDidPressed:(UniversalSwipableTableViewCell *)cell
{
    if ([self isTutorialHidden]) {
        [self performSegueWithIdentifier:ShareSegueIdentifier sender:cell];
    }
}

- (void)swipeCellRightButtonDidPressed:(UniversalSwipableTableViewCell *)cell
{
    if ([self isTutorialHidden]) {
        [cell hideCellButtons:nil];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        AssetModel *asset = [self.dataSource objectAtIndex:indexPath.row];
        __weak typeof(self) weakSelf = self;
        [[AssetsManager sharedInstance] deleteAsset:asset.sourceUrl withCompletitionHandler:^(NSError *error) {
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf reloadData];
                });
            }
        }];
    }
}

- (void)swipeCellMoreButtonPressed:(UniversalSwipableTableViewCell *)cell
{
    if ([self isTutorialHidden]) {
        
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:DetailedSegueIdentifier])
    {
        MediaFileDetailsViewController *mediaDetailsViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        mediaDetailsViewController.dataSource = self.dataSource;
        mediaDetailsViewController.itemIndex = indexPath.row;
        mediaDetailsViewController.viewerType = ViewerTypeLocalGallery;
        UniversalSwipableTableViewCell *cell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        mediaDetailsViewController.thumbnail = cell.thumbnailImageView.image;
        mediaDetailsViewController.mediaType = self.selectedMediaType;
    }
    else if ([segue.identifier isEqualToString:ShareSegueIdentifier])
    {
        [AppHelper rootViewControllerSideMenu].panGestureEnabled = NO;
        UniversalSwipableTableViewCell *cell = (UniversalSwipableTableViewCell *)sender;
        ShareImportViewController *shareVC = (ShareImportViewController *)((UINavigationController *)segue.destinationViewController).topViewController;
        shareVC.thumbnail = cell.thumbnailImageView.image;
        NSIndexPath *idxPath = [self.tableView indexPathForCell:cell];
        shareVC.detailDataSource = [self.dataSource objectAtIndex:idxPath.row];
        shareVC.fullResolutionImage = [((AssetModel *)shareVC.detailDataSource) fullResolutionImage];
        [cell hideCellButtons:nil];
        __weak typeof(self) weakSelf = self;
        shareVC.didCloseViewController = ^() {
            [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
        };
    }
}

#pragma mark - IBActions

- (void)photosButtonAction:(id)sender
{
    [super photosButtonAction:sender];
    
    self.mediaType = MediaFileTypePhotos;
    [self reloadData];
}

- (void)videosButtonAction:(id)sender
{
    [super videosButtonAction:sender];
    
    self.mediaType = MediaFileTypeVideos;
    [self reloadData];
}

#pragma mark - Observers

- (void)applicationWillEnterForegroundNotification:(NSNotification *)notification
{
    [self reloadData];
}

- (void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UI

- (void)configureCell:(UniversalSwipableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (![self isTutorialHidden]) {
        [super configureCell:cell atIndexPath:indexPath];
    } else {
        cell.delegate = self;
        
        cell.localTitleLabel.text = ((AssetModel *)[self.dataSource objectAtIndex:indexPath.row]).fileName;
        cell.localTypeImageView.image = [self getImageForAssetType:@"photo"];
        cell.localFileTypeLabel.text = (self.mediaType == MediaFileTypePhotos) ? NSLocalizedString(@"local.my_gallery.photo", nil) : NSLocalizedString(@"local.my_gallery.video", nil);
        cell.thumbnailImageView.image = [((AssetModel *)[self.dataSource objectAtIndex:indexPath.row]) fullScreenImage];
    }
    
    cell.cellGalleryType = CellGalleryTypeLocal;
    cell.cellSwipeType = ([AppHelper isIOS_8]) ? (SwipeTypeLeft | SwipeTypeRight) : SwipeTypeLeft;
    [cell setContentForSelectedCellType];
    
    cell.buttonWidth = CGRectGetWidth([UIScreen mainScreen].bounds) * 0.23f;
    [cell setImageForLeftButton:[UIImage imageNamed:@"ic_share"]];
    [cell setLeftSectionBackgroundColor:[UIColor lightBlueButtonBackgroundColor]];
    if ([AppHelper isIOS_8]) {
        [cell setImageForRightButton:[UIImage imageNamed:@"ic_trash"]];
        [cell setRightSectionBackgroundColor:[UIColor yellowButtonBackgroundColor]];
    }
    cell.disableLeftSwipe = self.mediaType == MediaFileTypeVideos;
}

- (void)localizeUI
{
    [super localizeUI];
    [super setGalleryTitle:NSLocalizedString(@"local.my_gallary.title", nil)];
}

- (void)updateUIIfNeeded
{
    self.fakeBarHeightConstraint.constant = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height;
}

#pragma mark - Override

- (void)tutorialFinished
{
    NSMutableArray *idxPaths = [NSMutableArray array];
    for (int i = 0; i < self.dataSource.count; i++) {
        [idxPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    self.dataSource = nil;

    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:idxPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    [self reloadData];
}

#pragma mark - Data

- (void)reloadData
{
    [self loadAlbumsWithCompletitionHandler:nil];
}

- (void)loadAlbumsWithCompletitionHandler:(void (^)())completion
{
    self.dataSource = [[NSMutableArray alloc] init];
    __weak typeof(self) weakSelf = self;
    [[AssetsManager sharedInstance] loadDataForType:self.mediaType withCompletitionHandler:^(NSArray *result, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.dataSource = result;
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            if (completion) {
                completion();
            }
        });
    }];
}

@end
