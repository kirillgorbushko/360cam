//
//  PlayerMobileViewController.m
//  360cam
//
//  Created by Misha Gajdan on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import "ModeSelectionViewController.h"
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"
#import "RemoveFileViewController.h"
#import "ShareImportViewController.h"
#import "GiroscopeTutorialViewController.h"

static NSUInteger const GradientViewTag = 100;
static NSInteger const BottomBarInitialHeight = 43;

@interface VideoPlayerViewController()

@property (weak, nonatomic) IBOutlet VideoProgressBarView *videoProgressBarView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *currentProgressLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadingActivityIndicator;

@property (assign, nonatomic) __block SPHViewModel selectedModel;

@end

@implementation VideoPlayerViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initBarButtons];
    [self addTapGesture];
    [self setControllEnabled:NO];
    [self.downloadingActivityIndicator startAnimating];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self prepareGradientForNavigationBar];
    [self prepareGradientForBottomView];
    self.videoProgressBarView.delegate = self;
    if (!self.presentedViewController) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self.videoProgressBarView setNeedsToDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self forceRotation];
    [self updateNavigationElements];
}

#pragma mark - Custom Accessors

- (void)setSelectedModel:(SPHViewModel)selectedModel
{
    _selectedModel = selectedModel;
    [((UIBarButtonItem *)[self.navigationItem.rightBarButtonItems lastObject]) setEnabled:!selectedModel];
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)forceRotation
{
    [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self prepareGradientForNavigationBar];
    [self.videoProgressBarView setNeedsToDisplay];
}

#pragma mark - SPHVideoPlayerDelegate

- (void)progressDidUpdate:(CGFloat)progress
{
    [super progressDidUpdate:progress];
    
    if (self.isPlaying) {
        [self.downloadingActivityIndicator stopAnimating];
        [self.videoProgressBarView setSliderPosition:progress];
    }
}

- (void)downloadingProgress:(CGFloat)progress
{
    [super downloadingProgress:progress];
    
    [self.videoProgressBarView setBufferingProgresBar:progress];
    if (progress >= (self.playedProgress)) {
        [self.downloadingActivityIndicator startAnimating];
    } else {
        [self.downloadingActivityIndicator stopAnimating];
    }
    
    if ((progress - self.playedProgress) > 0.03) {
        [self setControllEnabled:YES];
        [self.downloadingActivityIndicator stopAnimating];
        if (self.isPlaying) {
            [self.videoPlayer play];
        }
    }
}

- (void)isReadyToPlay
{
    [self setControllEnabled:YES];
    [self playButtonPress:self.playButton];
}

- (void)playerDidChangeProgressTime:(CGFloat)time totalTime:(CGFloat)totalDuration
{
    [super playerDidChangeProgressTime:time totalTime:totalDuration];

    NSString *timeString = [NSString stringWithFormat:@"%.2f", time];
    NSString *format = @"%i:%i:%@";
    if ((time < 9.9 || (int)time % 60 < 9.9) && time > 0 && self.currentProgressLabel.text.length) {
        format = @"%i:0%i:%@";
    }
    self.currentProgressLabel.text = [NSString stringWithFormat:format, (int)(time / 60), (int)time % 60, [[timeString componentsSeparatedByString:@"."] lastObject]];
    format = @"%i:%i";
    if (totalDuration / 60 < 10) {
        format = @"0%i:%i";
    }
    self.totalTimeLabel.text = [NSString stringWithFormat:format, (int)(totalDuration / 60), (int)totalDuration % 60];
}

- (void)didFailLoadVideo
{
    [AppHelper alertViewWithMessage:NSLocalizedString(@"video_player.fail_load_video", nil) delegate:self];
}

#pragma mark - VideoProgressBarViewDelegate

- (void)didChangeProgressTo:(CGFloat)progress
{
    [self.downloadingActivityIndicator startAnimating];
    [self seekPositionAtProgress:progress withPlayingStatus:self.isPlaying];
    if (self.isPlaying) {
        self.playButton.selected = YES;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self backButtonClickAction:self];
}

#pragma mark - PlayerAction

- (IBAction)playButtonPress:(id)sender
{
    if ([self isPlaying]) {
        [super pauseVideo];
    } else {
        [super playVideo];
    }
    self.playButton.selected = !self.playButton.selected;
}

#pragma mark - IBAction

- (IBAction)backButtonClickAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tapOnScreen
{
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [self hideBottomBar];
}

- (void)viewModeButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"ModeSelectionFromVideo" sender:self];
}

- (void)shareButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"shareScreenFromVideo" sender:self];
}

- (void)trashButtonClickAction:(id)sender
{
    [self performSegueWithIdentifier:@"removeFromVideoSegue" sender:self];
}

- (void)gyroButtonPressed:(id)sender
{
    [self performSegueWithIdentifier:@"giroTutorialFromVideo" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    if ([segue.identifier isEqualToString:@"shareScreenFromVideo"]) {
        [self prepareToPresentShareScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"removeFromVideoSegue"]) {
        [self prepareToPresentRemoveFileScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"ModeSelectionFromVideo"]) {
        [self prepareToPresentModeSelectionScreenScreenWithSegue:segue];
    } else if ([segue.identifier isEqualToString:@"giroTutorialFromVideo"]) {
        [self prepareToPresentGyroTutorialScreenScreenWithSegue:segue];
    }
}

#pragma mark - Private

- (void)prepareToPresentGyroTutorialScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self prepareForPresentingScreen];
    __weak typeof(self) weakSelf = self;
    GiroscopeTutorialViewController *giroTutorialViewController = segue.destinationViewController;
    giroTutorialViewController.currentGyroState = self.isGyroscopeActive;
    giroTutorialViewController.didSwitchGiro = ^(BOOL state) {
        [weakSelf setGyroscopeActive:state];
    };
    giroTutorialViewController.didCloseViewController = ^() {
        [weakSelf hideBottomBar];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
}

- (void)prepareToPresentModeSelectionScreenScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self prepareForPresentingScreen];
    
    __weak typeof(self) weakSelf = self;
    void (^SelectModel)(NSInteger model) = ^(NSInteger model){
        [self setGyroscopeActive:NO];
        [weakSelf switchToModel:model];
        weakSelf.selectedModel = model;
    };
    
    ModeSelectionViewController *controller = segue.destinationViewController;
    controller.didSelectMode = ^(NSInteger model) {
        SelectModel(model);
    };
    controller.didCloseView = ^() {
        [weakSelf hideBottomBar];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
    controller.activeModel = self.selectedModel;
}

- (void)prepareToPresentRemoveFileScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self prepareForPresentingScreen];
    
    RemoveFileViewController *removeFileVC = segue.destinationViewController;
    __weak typeof(self) weakSelf = self;
    removeFileVC.didCloseView = ^(BOOL afterDeleteAsset) {
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
        [weakSelf hideBottomBar];
    };
}

- (void)prepareToPresentShareScreenWithSegue:(UIStoryboardSegue *)segue
{
    [self prepareForPresentingScreen];
    
    if (self.isPlaying) {
        [self playButtonPress:self];
    }
    
    UINavigationController *shareNavigationController = segue.destinationViewController;
    ShareImportViewController *controller = (ShareImportViewController *)shareNavigationController.topViewController;
    __weak typeof(self) weakSelf = self;
    controller.didCloseViewController = ^() {
        [weakSelf hideBottomBar];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
    };
}

- (void)prepareForPresentingScreen
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self hideBottomBar];
}

- (void)hideBottomBar
{
    BOOL hidden = self.bottomViewHeightConstraint.constant;
    CGFloat newHeight = hidden ? 0.0f : BottomBarInitialHeight;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.bottomViewHeightConstraint.constant = newHeight;
        [weakSelf.bottomView layoutIfNeeded];
    }];
}

- (void)initBarButtons
{
    NSArray *buttonIcons = [[NSArray alloc] init];
    NSArray *buttonsColor = [[NSArray alloc] init];
    NSArray *buttonsSelectors = [[NSArray alloc] init];
    
    if (self.viewerType == ViewerTypeLocalBestOf) {
        buttonIcons = @[@"ic_eye_port_2", @"ic_gyro"];
        buttonsColor = @[ [UIColor whiteColor], [UIColor whiteColor] ];
        buttonsSelectors = @[@"viewModeButtonClickAction:", @"gyroButtonPressed:"];
    } else {
        buttonIcons = @[@"ic_eye_port_2", @"ic_trash_port", @"ic_gyro"];
        buttonsColor = @[ [UIColor whiteColor],[UIColor whiteColor], [UIColor whiteColor]];
        buttonsSelectors = @[@"viewModeButtonClickAction:", @"trashButtonClickAction:", @"gyroButtonPressed:"];
    }
    
    [self.navigationController setButtonsWithImageNamed:buttonIcons
                                      andActionDelegate:self
                                             tintColors:buttonsColor
                                               position:ButtonPositionModeRight
                          selectorsStringRepresentation:buttonsSelectors
                                            buttonWidth:40.f];
}

- (void)prepareGradientForNavigationBar
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.navigationController.navigationBar.bounds;
    gradient.colors = @[(id)[UIColor blackTransparentColor].CGColor, (id)[UIColor clearColor].CGColor];
    
    UIGraphicsBeginImageContext(self.navigationController.navigationBar.bounds.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:self.navigationController.navigationBar.bounds];
    view.image = viewImage;
    view.tag = GradientViewTag;
    [self.navigationController.navigationBar insertSubview:view atIndex:0];
}

- (void)prepareGradientForBottomView
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bottomView.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor clearColor].CGColor, (id)[UIColor blackTransparentColor].CGColor, nil];
    NSArray *gradientLocations = [NSArray arrayWithObjects:[NSNumber numberWithInt:0.0],[NSNumber numberWithInt:1.0], nil];
    gradientLayer.locations = gradientLocations;
    [self.bottomView.layer insertSublayer:gradientLayer atIndex:0];
}

- (void)addTapGesture
{
    UITapGestureRecognizer *tapOnScreenGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen)];
    [self.view addGestureRecognizer:tapOnScreenGesture];
}

- (void)setControllEnabled:(BOOL)enabled
{
    self.videoProgressBarView.userInteractionEnabled = enabled;
    self.playButton.enabled = enabled;
    self.currentProgressLabel.hidden = !enabled;
    self.totalTimeLabel.hidden = !enabled;
}

- (void)updateNavigationElements
{
    [[self.navigationController.navigationBar viewWithTag:GradientViewTag] removeFromSuperview];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

@end