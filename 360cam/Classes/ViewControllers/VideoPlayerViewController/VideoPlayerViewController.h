//
//  PlayerMobileViewController.h
//  360cam
//
//  Created by Misha Gajdan on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SPHVideoViewController.h"
#import "VideoProgressBarView.h"
#import "MediaFileDetailsViewController.h"

@interface VideoPlayerViewController : SPHVideoViewController <VideoProgressBarViewDelegate, UIAlertViewDelegate>

@property (assign, nonatomic) ViewerType viewerType;
@property (strong, nonatomic) NSDictionary *dataSource;

@end
