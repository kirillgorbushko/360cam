//
//  HelpViewController.m
//  SettingsView
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "HelpViewController.h"

#import "HelpTableViewCell.h"
#import "HelpCollectionViewCell.h"
#import "SettingsHeaderTableViewCell.h"

static NSString *const CollectionViewHelpCellIdentifier = @"HelpCollectionViewCell";
static NSString *const TableViewHelpCellIdentifier = @"TitleTableViewCell";
static NSString *const TableViewHeaderCellIdentified = @"HeaderCell";

static NSInteger const CollectionViewCellWidth = 120;

@interface HelpViewController () 

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(weak, nonatomic) NSIndexPath *selectedCell;
@property(copy, nonatomic) NSArray *menuDataSource;
@property (strong, nonatomic) UIView *sliderView;
@property (assign, nonatomic) BOOL shouldUpdateSliderPosition;

@end

@implementation HelpViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareNavigationDataSource];
    [self setupStartParameters];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self prepareSliderView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self setSliderInitialPositionIfNeeded];
}

#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.menuDataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HelpCollectionViewCell *cell = (HelpCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewHelpCellIdentifier forIndexPath:indexPath];
    cell.descriptionLabel.text = self.menuDataSource[indexPath.row];
    [cell.descriptionLabel setAlpha:0.5f];

    return cell;
}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HelpCollectionViewCell *cell = (HelpCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if (self.selectedCell) {
        HelpCollectionViewCell *previousCell = (HelpCollectionViewCell *)[collectionView cellForItemAtIndexPath:self.selectedCell];
        [previousCell.descriptionLabel setAlpha:0.5f];
    }
    
    NSInteger scrollIndex = indexPath.row;
    if (self.selectedCell.row < indexPath.row) {
        if (indexPath.row < self.menuDataSource.count - 1 ) {
            scrollIndex = indexPath.row + 1;
        }
    }
    
    if (self.selectedCell.row >= indexPath.row) {
        if (indexPath.row > 0) {
            scrollIndex = indexPath.row - 1;
        }
    }
    
    [collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:scrollIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];

    CGFloat nextScrollPoint = cell.center.x;
    [self moveView:self.sliderView toXpoint:nextScrollPoint andAnimationTime:0.2f];
    self.selectedCell = indexPath;
    cell.descriptionLabel.alpha = 1.0f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2; //demo
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3; //demo
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HelpTableViewCell *cell = (HelpTableViewCell *)[tableView dequeueReusableCellWithIdentifier:TableViewHelpCellIdentifier forIndexPath:indexPath];
    cell.mainImageView.image = [UIImage imageNamed:@"help_logo"];
    cell.descriptionLabel.text = @"Text";
    return cell; //demo
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SettingsHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:TableViewHeaderCellIdentified];
    headerCell.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    if (section) {
        headerCell.descriptionLabel.text = NSLocalizedString(@"help.item.battery", nil);
    } else {
        headerCell.descriptionLabel.text = NSLocalizedString(@"help.item.optics", nil);
    }
    return headerCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Private

- (void)prepareNavigationDataSource
{
    self.menuDataSource = @[NSLocalizedString(@"help.item.camera", nil),
                                NSLocalizedString(@"help.item.application", nil),
                                NSLocalizedString(@"help.item.accessoried", nil),
                                NSLocalizedString(@"help.item.sofv", nil)];
}

- (void)setupStartParameters
{
    self.selectedCell = [NSIndexPath indexPathForRow:0 inSection:0];
    self.shouldUpdateSliderPosition = YES;
}

- (void)prepareSliderView
{
    if (!self.sliderView) {
        CGSize collectionViewContentSize = [self.collectionView contentSize];
        UIView *subview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, collectionViewContentSize.width, collectionViewContentSize.height)];
        [self.collectionView insertSubview:subview atIndex:0];
        subview.backgroundColor = [self.collectionView backgroundColor];
        CGFloat sliderHeight = 3;
        self.sliderView = [[UIView alloc] initWithFrame:CGRectMake(CollectionViewCellWidth / 4, CGRectGetMaxY(subview.frame) - sliderHeight, CollectionViewCellWidth / 2, sliderHeight)];
        self.sliderView.backgroundColor = [UIColor whiteColor];
        [subview addSubview:self.sliderView];
    }
}

- (void)setSliderInitialPositionIfNeeded
{
    if (self.shouldUpdateSliderPosition) {
        HelpCollectionViewCell *cell = (HelpCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        [self moveView:self.sliderView toXpoint:cell.center.x andAnimationTime:0.1f];
        self.shouldUpdateSliderPosition = NO;
    }
}

- (void)moveView:(UIView*)view toXpoint:(CGFloat)xPoint andAnimationTime:(CGFloat)duration
{
    CGFloat originalY = view.center.y;
    CGFloat originalX = view.center.x;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(originalX, originalY)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(xPoint, originalY)];
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:@"position"];
    view.layer.position = [animation.toValue CGPointValue];
}

@end
