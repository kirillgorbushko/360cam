//
//  ViewController.h
//  ShareImportViewController
//
//  Created by Misha Gajdan on 26.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface ShareImportViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

typedef NS_ENUM(NSUInteger, SharingType) {
    SharingTypeUndefined,
    SharingTypeMail,
    SharingTypeFacebook,
    SharingTypeLinkedin,
    SharingTypeTwitter,
    SharingTypeGooglePlus
};

typedef NS_ENUM(NSUInteger, ShareOptionType) {
    ShareOptionTypeUnderfind,
    ShareOptionTypeShare,
    ShareOptionTypeExport
};

@property (strong, nonatomic) void (^didCloseViewController)();
@property (strong, nonatomic) NSDictionary *detailDataSource;
@property (strong, nonatomic) UIImage *thumbnail;
@property (strong, nonatomic) UIImage *fullResolutionImage;

- (IBAction)closeViewControllerAction:(id)sender;

@end