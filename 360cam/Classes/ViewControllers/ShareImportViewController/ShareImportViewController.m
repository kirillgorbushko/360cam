//
//  ViewController.m
//  ShareImportViewController
//
//  Created by Misha Gajdan on 26.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

// Controllers
#import "ShareImportViewController.h"
#import "RootViewController.h"
#import "DetailShareImportViewController.h"

// Cells
#import "ShareImportCollectionViewCell.h"

// Categories
#import "UINavigationController+Transparent.h"
#import "MBProgressHUD+CancelButton.h"

// Models
#import "Animation.h"

static NSString *const HorizontalCellIdentifier = @"HorizontalCell";
static NSString *const VerticalCellIdentifier = @"VerticalCell";
static NSString *const DetailShareImportSegueIdentifier = @"DetailShareImport";

@interface ShareImportViewController ()

@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *holderLogoView;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *importButton;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderViewCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceForLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewHeightConstraint;

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSURLSessionDownloadTask *currentTask;

@property (weak, nonatomic) UIButton *lastClickedButton;
@property (strong, nonatomic) MBProgressHUD *HUD;

@property (assign, nonatomic) SharingType selectedSharingMode;
@property (assign, nonatomic) ShareOptionType selectedShareOption;

@end

@implementation ShareImportViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self animateAppearing];
    [self prepareDataSource];
    [self performUILocalization];
    [self prepareProgressHUD];
    
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    self.selectedShareOption = ShareOptionTypeShare;
    self.sliderView.backgroundColor = [UIColor colorLightBlueShareImport];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"";
    [self changeScrollDircetion:[UIApplication sharedApplication].statusBarOrientation];
    [self.collectionView reloadData];
    [self updateSliderViewPosition];
    
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self updateUIIFNeededFor:[UIApplication sharedApplication].statusBarOrientation];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self shareClickAction:self.shareButton];
    if (!self.fullResolutionImage) {
        [self downloadFullResolutionImage];
    }
}

#pragma mark - Rotation

- (void)canRotate
{
    //dummy
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self changeScrollDircetion:toInterfaceOrientation];
    [self.collectionView reloadData];
    [self updateSliderViewPosition];
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.navigationController.view.layer animationForKey:@"dismiss"]) {
        [self.navigationController.view.layer removeAllAnimations];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShareImportCollectionViewCell *cell;
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:HorizontalCellIdentifier forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:VerticalCellIdentifier forIndexPath:indexPath];
    }
    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedSharingMode = indexPath.row + 1;
    [self performSegueWithIdentifier:DetailShareImportSegueIdentifier sender:self];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))  {
        return CGSizeMake(CGRectGetWidth(self.collectionView.frame) / self.dataSource.count , CGRectGetHeight(self.collectionView.frame));
    } else {
        return CGSizeMake(CGRectGetWidth(self.collectionView.frame), CGRectGetHeight(self.collectionView.frame) / self.dataSource.count);
    }
}

#pragma mark - IBAction

- (IBAction)shareClickAction:(id)sender
{
    [self moveView:self.sliderView toXpoint:self.shareButton.center.x withAnimationTime:0.2f];
    [self updateClickedButtonAlpha:self.shareButton];
    self.selectedShareOption = ShareOptionTypeShare;
}

- (IBAction)importClickAction:(id)sender
{
    [self moveView:self.sliderView toXpoint:self.importButton.center.x withAnimationTime:0.2f];
    [self updateClickedButtonAlpha:self.importButton];
    self.selectedShareOption = ShareOptionTypeExport;
    [self performSegueWithIdentifier:DetailShareImportSegueIdentifier sender:self];
}

- (IBAction)closeViewControllerAction:(id)sender
{
    [self.navigationController hideTransparentNavigationBarAnimated:NO];
    [self.navigationController.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"dismiss"];
    self.navigationController.view.layer.opacity = 0.f;
    [AppHelper rootViewControllerSideMenu].panGestureEnabled = YES;
    if (self.didCloseViewController) {
        self.didCloseViewController();
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:DetailShareImportSegueIdentifier]) {
        [self detailShareSegue:segue];
    }
}

#pragma mark - Download

- (void)downloadFullResolutionImage
{
    self.HUD.progress = 0.f;
    [self.HUD show:YES];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{@"Content-Type": @"application/json", @"Accept": @"application/json"}];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:(id)self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[@"http://api.360.tv/" stringByAppendingPathComponent:[self.detailDataSource valueForKey:@"path_high"]]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0.f];
    [request setHTTPMethod:@"GET"];
    
    self.currentTask = [session downloadTaskWithRequest:request];
    [self.currentTask resume];

}

- (void)prepareProgressHUD
{
    self.HUD = [[MBProgressHUD alloc] initWithView:[AppHelper topView]];
    [((RootViewController *)[AppHelper rootViewControllerSideMenu]).view addSubview:self.HUD];
    self.HUD.mode = MBProgressHUDModeDeterminate;
    self.HUD.dimBackground = YES;
    self.HUD.labelText = @"Downloading image";
    [self.HUD addCancelButtonForTarger:self andSelector:NSStringFromSelector(@selector(MBProgressHUDCancelButtonDidPressed))];
}

- (void)MBProgressHUDCancelButtonDidPressed
{
    if (self.currentTask.state == NSURLSessionTaskStateCompleted) {
        [self.HUD hide:YES];
    } else {
        [self.currentTask cancel];
    }
}

#pragma mark - NSURLSessionTaskDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    CGFloat progress = (float)totalBytesWritten / totalBytesExpectedToWrite;
    self.HUD.progress = progress;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSData* data = [NSData dataWithContentsOfURL:location];
    self.fullResolutionImage = [UIImage imageWithData:data];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.HUD hide:YES];
    });
}

- (void)URLSession:(nonnull NSURLSession *)session task:(nonnull NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error
{
    if (error) {
        [self.HUD hide:YES];
        if (error.code != -999) {
            [AppHelper alertViewWithMessage:[error.localizedDescription capitalizedString]];
        } else {
            [self closeViewControllerAction:self];
        }
    }
}

#pragma mark - Private

- (void)detailShareSegue:(UIStoryboardSegue *)segue
{
    NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
    DetailShareImportViewController *controller = segue.destinationViewController;
    controller.detailDataSource = self.detailDataSource;
    controller.thumbnail = self.thumbnail;
    controller.fullResolutionImage = self.fullResolutionImage;
    controller.selectedSharingMode = self.selectedSharingMode;
    controller.selectedShareOption = self.selectedShareOption;
    NSString *titleText = [self detailShareImportViewControllerTitle:self.lastClickedButton];
    UIColor *shareHeaderBackgroundColor = self.dataSource[indexPath.row][@"backgroundColor"];
    [controller setHeaderImage:self.dataSource[indexPath.row][@"BigImage"] andTitleText:titleText andImage:nil withBackgroundColor:shareHeaderBackgroundColor];
}

- (void)configureCell:(ShareImportCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.descriptionLabel.text = NSLocalizedString(self.dataSource[indexPath.row][@"Description"], nil);
    cell.mainImage.image = [UIImage imageNamed:self.dataSource[indexPath.row][@"Image"]];
    cell.selectedBackgroundView.backgroundColor = [UIColor whiteColor];
}

- (void)updateSliderViewPosition
{
    if (self.lastClickedButton == self.importButton) {
        self.sliderViewCenterXConstraint.constant = - (CGRectGetWidth([UIScreen mainScreen].bounds) / 2);
    } else {
        self.sliderViewCenterXConstraint.constant = 0;
    }
}

- (void)performUILocalization
{
    [self.shareButton setTitle:NSLocalizedString(@"share_import.share", nil) forState:UIControlStateNormal];
    [self.importButton setTitle:NSLocalizedString(@"share_import.export", nil) forState:UIControlStateNormal];
}

- (NSString *)detailShareImportViewControllerTitle:(UIButton *)button
{
    NSString *resultText;
    if (button == self.importButton) {
        resultText = NSLocalizedString(@"share_import.export", nil);
        //self
    } else {
        resultText = NSLocalizedString(@"share_import_detail.shareOn", nil);
    }
    return resultText;
}

- (void)prepareDataSource
{
    self.lastClickedButton = self.shareButton;
    
    self.dataSource = [[NSMutableArray alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ShareImport" ofType:@"plist"];
    NSArray *tempData = [[NSArray alloc] initWithContentsOfFile:path];
    NSArray *shareBackgroundColors = @[[UIColor mailShareImport],
                                       [UIColor facebookGrayShareImport],
                                       [UIColor linkedinShareImport],
                                       [UIColor twitterShareImport],
                                       [UIColor googlePlusShareImport]];
    
    __weak typeof(self) weakSelf = self;
    [tempData enumerateObjectsUsingBlock:^(NSDictionary *shareInfo, NSUInteger idx, BOOL *stop) {
        NSMutableDictionary *socialData = [[NSMutableDictionary alloc] initWithDictionary:shareInfo];
        [socialData setObject:[shareBackgroundColors objectAtIndex:idx] forKey:@"backgroundColor"];
        [weakSelf.dataSource addObject:socialData];
    }];
}

- (void)moveView:(UIView*)view toXpoint:(CGFloat)xPoint withAnimationTime:(CGFloat)duration
{
    CGFloat originalY = view.center.y;
    CGFloat originalX = view.center.x;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(originalX, originalY)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(xPoint, originalY)];
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:@"position"];
    view.layer.position = [animation.toValue CGPointValue];
}

- (void)changeScrollDircetion:(UIInterfaceOrientation)orientation
{
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)[self.collectionView collectionViewLayout];
    [layout invalidateLayout];
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    } else {
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
}

- (void)updateClickedButtonAlpha:(UIButton *)button
{
    if (button != self.lastClickedButton) {
        [self.lastClickedButton setAlpha:1.0];
        [button setAlpha:0.5];
        self.lastClickedButton = button;
    }
}

- (void)animateAppearing
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
    self.view.layer.opacity = 1.f;
}

- (void)updateUIIFNeededFor:(UIInterfaceOrientation)toOrrientation
{
    [self.view layoutIfNeeded];

    [UIView beginAnimations:nil context:nil];
    NSTimeInterval interval = [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
    [UIView setAnimationDuration:interval];

    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if (UIInterfaceOrientationIsLandscape(toOrrientation)) {
        self.logoImageViewHeightConstraint.constant = (screenHeight * 0.23f) * 0.3f; //heigh top View * % for buttons
        self.bottomSpaceForLogo.constant = (self.holderLogoView.frame.size.height - (screenHeight * 0.23f) * 0.3f) / 2;
    } else {
        self.logoImageViewHeightConstraint.constant = (screenHeight * 0.23f) * 0.25f; //heigh top View * % for buttons
        self.bottomSpaceForLogo.constant = self.holderLogoView.frame.size.height / 3 - ((screenHeight * 0.23f) * 0.25f) / 2;
    }
    
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
}

@end
