//
//  CameraNavigationController.m
//  360cam
//
//  Created by Kirill Gorbushko on 26.06.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "CameraNavigationController.h"
#import "RootViewController.h"
#import "UINavigationController+RightButton.h"
#import "UINavigationController+Transparent.h"

static NSString *const MenuButtonImageName = @"menu-gray";

@interface CameraNavigationController ()

@end

@implementation CameraNavigationController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.showMenuButton) {
        [self prepareNavigationBarButtons];
    }
    if (self.presentTransparentNavigationBar) {
        [self presentTransparentNavigationBarAnimated:YES];
    }
    [self setDelegateToMenu];
}

#pragma mark - RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    if (self.subDelegate && [self.subDelegate respondsToSelector:@selector(menuWillShown)]) {
        [self.subDelegate menuWillShown];
    }
}

#pragma mark - Private

- (void)prepareNavigationBarButtons
{
    [self setButtonWithImageNamed:MenuButtonImageName andActionDelegate:self tintColor:[UIColor whiteColor] position:ButtonPositionModeLeft selector:@selector(menuButtonPressed)];
}

- (void)setDelegateToMenu
{
    ((RootViewController *)[AppHelper rootViewControllerSideMenu]).delegate = self;
}

#pragma mark - IBActions

- (void)menuButtonPressed
{
    if (self.subDelegate && [self.subDelegate respondsToSelector:@selector(menuButtonDidPressed)]) {
        [self.subDelegate menuButtonDidPressed];
    }
    [((RootViewController *)[AppHelper rootViewControllerSideMenu]) showHideMenu];
}


@end
