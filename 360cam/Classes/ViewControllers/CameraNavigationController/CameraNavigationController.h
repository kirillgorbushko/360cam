//
//  CameraNavigationController.h
//  360cam
//
//  Created by Kirill Gorbushko on 26.06.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//
#import "RESideMenu.h"

@protocol CameraNavigationControllerDelegate <NSObject>

@optional
- (void)menuButtonDidPressed;
- (void)menuWillShown;

@end

IB_DESIGNABLE
@interface CameraNavigationController : UINavigationController <RESideMenuDelegate>

@property (weak, nonatomic) id <CameraNavigationControllerDelegate> subDelegate;

@property (assign, nonatomic) IBInspectable BOOL presentTransparentNavigationBar;
@property (assign, nonatomic) IBInspectable BOOL showMenuButton;

@end
