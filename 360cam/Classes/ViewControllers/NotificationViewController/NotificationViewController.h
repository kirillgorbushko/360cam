//
//  NotificationViewController.h
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface NotificationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
