//
//  NotificationViewController.m
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NotificationViewController.h"
#import "TextNotificationTableViewCell.h"
#import "PhotoNotificationTableViewCell.h"

static NSString *const TextNotifiactionCellIdentefier = @"TextNotificationCell";
static NSString *const PhotoNotifiactionCellIdentefier = @"PhotoNotificationCell";
static NSInteger const ImageInLine = 4;
static NSString *const TypeKey = @"type";
static NSString *const ImagesKey = @"images";
static NSString *const UserNameKey = @"user_name";
static NSString *const MessageKey = @"message";
static NSString *const CreatedKey = @"created";
static NSString *const DiscriptionKey = @"discription";

static NSString *const PhotoLikedKey = @"photo_liked";
static NSString *const CommentRepliedKey = @"comment_replied";
static NSString *const FollowedKey = @"follow";

@interface NotificationViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSArray *dataSource;

@end

@implementation NotificationViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initRefreshControl];
    [self prepareDataSource];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    if ([[[self.dataSource objectAtIndex:indexPath.row] objectForKey:TypeKey] isEqualToString:PhotoLikedKey]) {
        cell = [tableView dequeueReusableCellWithIdentifier:PhotoNotifiactionCellIdentefier];
        [self configurePhotoNotificationCell:cell atIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:TextNotifiactionCellIdentefier];
        [self configureTextNotificationCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = [self.dataSource objectAtIndex:indexPath.row];
    if ([[data objectForKey:TypeKey] isEqualToString:PhotoLikedKey]) {
        NSArray *images = [data objectForKey:ImagesKey];
        return [self heightForPhotoNotificationCellbyImageCount:images.count atIndexPath:indexPath];
    } else {
        return [self heightForTextNotificationCellAtIndexPath:indexPath];
    }
}

#pragma  mark - Cell configuration

- (void)configurePhotoNotificationCell:(UITableViewCell *)tableViewCell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = [self.dataSource objectAtIndex:indexPath.row];
    
    PhotoNotificationTableViewCell *cell = (PhotoNotificationTableViewCell *)tableViewCell;
    
    cell.notificationImageView.image = [UIImage imageNamed:TimelineHeartIconName];
    cell.userNameLabel.text = [data objectForKey:UserNameKey];
    cell.notificationMessage.text = [data objectForKey:MessageKey];
    cell.notificationCreatedLabel.text = [data objectForKey:CreatedKey];
    [cell setImageArray:[data objectForKey:ImagesKey]];
}

- (void)configureTextNotificationCell:(UITableViewCell *)tableViewCell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = [self.dataSource objectAtIndex:indexPath.row];
    
    TextNotificationTableViewCell *cell = (TextNotificationTableViewCell *)tableViewCell;
    
    cell.notificationImageView.image = [UIImage imageNamed:ChatIconName];
    cell.userNameLabel.text = [data objectForKey:UserNameKey];
    cell.notificationCreatedLabel.text = [data objectForKey:CreatedKey];
    
    if ([[data objectForKey:TypeKey] isEqualToString:CommentRepliedKey]) {
        
        NSString *title = [data objectForKey:MessageKey];
        NSString *description = [data objectForKey:DiscriptionKey];
        cell.notificationDiscriptionLabel.attributedText = [self makeAtributedStringWith:title and:description];
        
    } else if ([[data objectForKey:TypeKey] isEqualToString:FollowedKey]) {
        
        NSString *title = [data objectForKey:MessageKey];
        cell.notificationDiscriptionLabel.attributedText = [self makeAtributedStringWith:title and:nil];

        cell.notificationImageView.image = [UIImage imageNamed:PlusIconName];
    }
}

#pragma mark - Cell Height Configuration

- (CGFloat)heightForTextNotificationCellAtIndexPath:(NSIndexPath *)indexPath
{
    static TextNotificationTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:TextNotifiactionCellIdentefier];
    });
    
    [self configureTextNotificationCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height ;
}

- (CGFloat)heightForPhotoNotificationCellbyImageCount:(NSInteger)imageCount atIndexPath:(NSIndexPath *)indexPath
{
    static PhotoNotificationTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:PhotoNotifiactionCellIdentefier];
    });
    [self configurePhotoNotificationCell:sizingCell atIndexPath:indexPath];
    
    CGFloat mainScreenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat collectionViewLeftOffset = sizingCell.collectionView.frame.origin.x;
    CGFloat collectionViewRightOffset = sizingCell.collectionViewTrailingConstraint.constant;
    CGFloat collectionViewTopOffset = sizingCell.collectionView.frame.origin.y;
    CGFloat collectionViewBottomOffset = sizingCell.collectionViewBottomConstraint.constant;
    CGFloat collectionViewWidth = mainScreenWidth - collectionViewLeftOffset - collectionViewRightOffset;
    
    NSInteger cellSideDimension = collectionViewWidth / ImageInLine;
    NSUInteger linesQuantity = ceilf(imageCount / (CGFloat)ImageInLine);
    
    CGFloat collectionViewHeight = linesQuantity * cellSideDimension;
    
    return collectionViewHeight + collectionViewTopOffset + collectionViewBottomOffset;
}

#pragma mark - Private

- (NSAttributedString *)makeAtributedStringWith:(NSString *)firstString and:(NSString *)secondString
{
    UIFont *titleFont = [UIFont fontWithName:@"Zona Pro" size:11.f];
    UIFont *descriptionFont = [UIFont fontWithName:@"Zona Pro" size:15.f];
    UIColor *color = [UIColor lightGrayColorForNotificationText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.paragraphSpacing = 15.f;
    paragraphStyle.minimumLineHeight = 13.25f;
    
    NSMutableAttributedString *titleWithDescription = [[NSMutableAttributedString alloc]
                                                       initWithString:firstString
                                                       attributes:@{
                                                                    NSFontAttributeName : titleFont,
                                                                    NSForegroundColorAttributeName : color,
                                                                    NSParagraphStyleAttributeName : paragraphStyle
                                                                    }];
    
    if (secondString) {
        NSMutableAttributedString *description = [[NSMutableAttributedString alloc]
                                                  initWithString:secondString
                                                  attributes:@{
                                                               NSFontAttributeName : descriptionFont,
                                                               NSForegroundColorAttributeName : color,
                                                               NSParagraphStyleAttributeName : paragraphStyle
                                                               }];
        [titleWithDescription appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        [titleWithDescription appendAttributedString:description];
    }
    
    return titleWithDescription;
}

- (void)initRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(stopRefresh) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

- (void)stopRefresh
{
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    [self.refreshControl endRefreshing];
}

- (void)prepareDataSource
{
    self.dataSource = @[@{
                            @"created" : @"15 MIN AGO",
                            @"images" :     @[
                                    @"Photo1.jpg",
                                    @"Photo2.jpg",
                                    @"Photo3.jpg",
                                    @"Photo4.jpg",
                                    @"Photo5.jpg"
                                    ],
                            @"message" : @"LIKED 5 YOUR PHOTOS",
                            @"type" : @"photo_liked",
                            @"user_name" : @"STACY MARTIN",
                            },
                        @{
                            @"created" : @"15 MIN AGO",
                            @"discription" : @"I really don't get what all the fuss is about. She could never compareto Clindy, Christy and others.",
                            @"message" : @"REPLIED TO YOUR COMMENT",
                            @"type" : @"comment_replied",
                            @"user_name" : @"ALEXEY RYIBIN",
                            },
                        @{
                            @"created" : @"1 HOUR AGO",
                            @"message" : @"IS NOW FOLLOWING YOU",
                            @"type" : @"follow",
                            @"user_name" : @"STACY MARTIN",
                            },
                        @{
                            @"created" : @"2 HOURES AGO",
                            @"images" :     @[
                                    @"Photo1.jpg",
                                    @"Photo2.jpg",
                                    @"Photo3.jpg"
                                    ],
                            @"message" : @"LIKED 3 YOUR PHOTOS",
                            @"type" : @"photo_liked",
                            @"user_name" : @"STACY MARTIN",
                            },
                        @{
                            @"created" : @"1 HOUR AGO",
                            @"message" : @"IS NOW FOLLOWING YOU",
                            @"type" : @"follow",
                            @"user_name" : @"STACY MARTIN",
                            }
                        ];
}

@end
