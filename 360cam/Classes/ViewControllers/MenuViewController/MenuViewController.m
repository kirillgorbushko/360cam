//
//  MenuViewController.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "MenuViewController.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "MenuTableViewCell.h"
#import "MenuHeaderTableViewCell.h"
#import "WiFiFetchService.h"
#import "CameraNetworkManager.h"

static NSString *const MenuCellIdentifier = @"menuCell";
static NSString *const MenuCellHeaderIdentifier = @"menuCellHeader";
static CGFloat const MenuItemsCellHeight = 45.f;
static NSInteger const DefaultSessionDuration = 3600;

@interface MenuViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogoConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tralingTableViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoLeadingSpaceConstraint;

@property (assign, nonatomic) NSInteger activeCameraSectionItemsCount;
@property (strong, nonatomic) NSArray *dataSourceItems;

@end

@implementation MenuViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateConstraintsForMenuElementsIfNeeded];
    [self prepareDataSource];
    [self updateUIIfNeeded];
    [self addWiFiObservers];
    
}

- (void)dealloc
{
    [self removeWiFiObservers];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *valuesForKey = [self.dataSourceItems valueForKey:@"section"];
    NSSet *uniqueValues = [NSSet setWithArray:valuesForKey];
    return uniqueValues.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger itemsCount = [self itemsForSectionWithSection:section].count;
    if (section == 1) {
        itemsCount = self.activeCameraSectionItemsCount;
    }
    return itemsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MenuCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MenuCellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return MenuItemsCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *selectedItem = [self itemsForSectionWithSection:indexPath.section][indexPath.row];
    NSInteger indexToNavigate = [[selectedItem valueForKey:@"menuItem"] integerValue];
    [[AppHelper rootViewControllerSideMenu] navigateToViewControllerAtIndex:indexToNavigate];
}

- (void)configureCell:(MenuTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *selectedItem = [self itemsForSectionWithSection:indexPath.section][indexPath.row];
    
    if (((NSString *)[selectedItem valueForKey:@"image"]).length) {
        cell.menuLogoImage.image = [UIImage imageNamed:[selectedItem valueForKey:@"image"]];
    }
    cell.menuItemNameLabel.text = NSLocalizedString([selectedItem valueForKey:@"title"], nil);
    
    if (indexPath.section == 1 && indexPath.row > 1) {
        [cell setSubMenuStyle];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MenuHeaderTableViewCell *headerCell = (MenuHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MenuCellHeaderIdentifier];
    return headerCell.contentView;
}

#pragma mark - Private

- (void)updateUIIfNeeded
{
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        self.logoLeadingSpaceConstraint.constant /= 2;
    }
}

- (void)showHideSubMenuForCamera
{
    if (self.activeCameraSectionItemsCount == 2) {
        self.activeCameraSectionItemsCount = [self itemsForSectionWithSection:1].count;
    } else {
        self.activeCameraSectionItemsCount = 2;
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSArray *)itemsForSectionWithSection:(NSInteger)section
{
    NSMutableArray *sectionItems = [[NSMutableArray alloc] init];;
    for (NSDictionary *item in self.dataSourceItems) {
        if ([[item valueForKey:@"section"] integerValue] == section) {
            [sectionItems addObject:item];
        }
    }
    return sectionItems;
}

- (void)prepareDataSource
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Menu" ofType:@"plist"];
    self.dataSourceItems = [[NSArray alloc] initWithContentsOfFile:path];
    self.activeCameraSectionItemsCount = 2;
}

- (void)updateConstraintsForMenuElementsIfNeeded
{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    self.topLogoConstraint.constant = (height - height * [AppHelper rootViewControllerSideMenu].contentViewScaleValue) / 2;
    self.tralingTableViewConstraint.constant =  [AppHelper rootViewControllerSideMenu].panMinimumOpenThreshold;
    [self.view layoutIfNeeded];
}

#pragma mark - Device WiFi observer

- (void)addWiFiObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraConnected) name:DeviceDidConnectedToCameraNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraDisConected) name:DeviceDidDisconnectedToCameraNotification object:nil];
}

- (void)removeWiFiObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DeviceDidConnectedToCameraNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DeviceDidDisconnectedToCameraNotification object:nil];
}

- (void)cameraConnected
{
    [self showHideSubMenuForCamera];
    [self startSession];
}

- (void)startSession
{
    void (^StartSessionBlock)() = ^(){
        CameraNetworkManager *manager = [CameraNetworkManager sharedManager];
        [manager giropticStartSessionWithDurationInSec:DefaultSessionDuration result:^(BOOL success, id response, NSError *error) {
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    };
    NSString *prevSessionId;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:GiroCurrentSessionID]) {
        prevSessionId = [[NSUserDefaults standardUserDefaults] objectForKey:GiroCurrentSessionID];
    }
    if (prevSessionId.length) {
        [[CameraNetworkManager sharedManager] giropticCloseSessionWithSessionId:prevSessionId operationResult:^(BOOL success, id response, NSError *error) {
            if (error) {
                NSLog(@"close session error %@",error.description);
            }
            StartSessionBlock();
        }];
    } else {
        StartSessionBlock();
    }
}

- (void)cameraDisConected
{
    [self showHideSubMenuForCamera];
}

@end
