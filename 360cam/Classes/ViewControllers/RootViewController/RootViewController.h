//
//  RootViewController.h
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "RESideMenu.h"

@interface RootViewController : RESideMenu <RESideMenuDelegate>

- (void)navigateToViewControllerAtIndex:(NSInteger)index;
- (void)showHideMenu;

@end
