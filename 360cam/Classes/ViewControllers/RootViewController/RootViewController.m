//
//  RootViewController.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "RootViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "CameraSettingsViewController.h"
#import "DetailedSettingViewController.h"
#import "HelpViewController.h"
#import "RemoteCameraHomeViewController.h"
#import "RemoteCameraSDGalleryViewController.h"
#import "MyGalleryViewController.h"

@interface RootViewController ()

@property (strong, nonatomic) NSMutableArray *viewControllers;

@end

@implementation RootViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareViewControllers];

}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initSideMenu];
}

#pragma mark - Public

- (void)navigateToViewControllerAtIndex:(NSInteger)index
{
    if (index >= self.viewControllers.count) {
        [AppHelper alertViewWithMessage:@"Coming soon"];
        return;
    }
    
    [self setContentViewController:self.viewControllers[index] animated:YES];
    [self hideMenuViewController];
}

- (void)showHideMenu
{
    [self presentLeftMenuViewController];
}

#pragma mark - Private

- (void)prepareViewControllers
{
    if (!self.viewControllers) {
        self.viewControllers = [[NSMutableArray alloc] init];
    }
    
    UINavigationController *homeNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeNavigationController"];
    UINavigationController *settingsNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsNavigationViewController"];
    UINavigationController *choseCameraNavController = [self.storyboard instantiateViewControllerWithIdentifier:@"remoteCameraNavigationController"];
    UINavigationController *helpNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"helpNavigationController"];
    UINavigationController *bestOfNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"bestOfNavigationController"];
    UINavigationController *remoteCameraSDNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"remoteCameraSDNavigationController"];
    UINavigationController *myGalleryNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"myGallaryNavigationController"];
    UINavigationController *remoteCamHomeNavController = [self.storyboard instantiateViewControllerWithIdentifier:@"remoteCameraNavigationController"];
    
    UIViewController *remoteCameraHomeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"remoteCameraHome"];
    [remoteCamHomeNavController setViewControllers:@[remoteCameraHomeViewController]];
    
    [self.viewControllers addObjectsFromArray:@[
                                                homeNavigationController,
                                                settingsNavigationViewController,
                                                helpNavigationController,
                                                choseCameraNavController,
                                                bestOfNavigationController,
                                                remoteCameraSDNavigationController,
                                                myGalleryNavigationController,
                                                remoteCamHomeNavController
                                                ]];
}

- (void)initSideMenu
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeZero;
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 10;
    self.contentViewShadowEnabled = YES;
    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeNavigationController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
}

@end
