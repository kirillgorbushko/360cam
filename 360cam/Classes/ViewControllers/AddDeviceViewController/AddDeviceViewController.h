//
//  AddDeviceViewController.h
//  360cam
//
//  Created by Victor on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface AddDeviceViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@end
