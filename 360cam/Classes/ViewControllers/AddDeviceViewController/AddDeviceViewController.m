//
//  AddDeviceViewController.m
//  360cam
//
//  Created by Victor on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AddDeviceViewController.h"
#import "UINavigationController+Transparent.h"
#import "AddDeviceCollectionViewCell.h"

static NSInteger const ElementsCount = 4;
static NSString *const AddDeviceCellIdentifier = @"addDeviceCollectionViewCell";

@interface AddDeviceViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *wifiButton;
@property (weak, nonatomic) IBOutlet UIButton *routerButton;
@property (weak, nonatomic) IBOutlet UIView *tabView;
@property (weak, nonatomic) IBOutlet UIView *indicatorView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageIndicatorBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fakeNavigationBarHeightConstraint;

@property (strong, nonatomic) NSMutableArray *datasource;
@property (assign, nonatomic) CGFloat currentLeadingSpace;

@end

@implementation AddDeviceViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareDataSource];
    [self configurePageControl];
    [self configureStartValues];
    [self updateUIIfNeeded];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self configureView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    for (NSIndexPath *indexPath in self.collectionView.indexPathsForVisibleItems) {
        AddDeviceCollectionViewCell *cell = (AddDeviceCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        if (ABS(cell.frame.origin.x - self.collectionView.contentOffset.x) <= 50) {
            self.pageControl.currentPage = indexPath.row;
        }
    }
}

#pragma mark - IBActions

- (IBAction)pageOneTapped:(id)sender
{
    [self.wifiButton setTitleColor:[UIColor grayColorForText] forState:UIControlStateNormal];
    [self.routerButton setTitleColor:[UIColor darkGrayColorForText] forState:UIControlStateNormal];

    [self moveIndicatorToButton:self.wifiButton];
}

- (IBAction)pageTwoTapped:(id)sender
{
    [self.routerButton setTitleColor:[UIColor grayColorForText] forState:UIControlStateNormal];
    [self.wifiButton setTitleColor:[UIColor darkGrayColorForText] forState:UIControlStateNormal];
    
    [self moveIndicatorToButton:self.routerButton];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.datasource.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath
{
    AddDeviceCollectionViewCell *cell = (AddDeviceCollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:AddDeviceCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[AddDeviceCollectionViewCell alloc] init];
    }
    cell.contentImageView.image = self.datasource[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*)indexPath
{
    return self.collectionView.frame.size;
}

#pragma mark - Private

- (void)prepareDataSource
{
    if (!self.datasource) {
        self.datasource = [[NSMutableArray alloc] init];
    }
    for (int i = 0; i < ElementsCount; i ++) {
        [self.datasource addObject:[UIImage imageNamed:@"img_cam"]];
    }
}

- (void)configurePageControl
{
    self.pageControl.numberOfPages = self.datasource.count;
}

- (void)configureView
{
    self.indicatorViewLeadingConstraint.constant = self.currentLeadingSpace;
    for (UIView *subView in self.tabView.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            subView.layer.borderWidth = 1.f;
            subView.layer.borderColor = [UIColor grayColorForIndicatorBorder].CGColor;
        }
    }
}

- (void)moveIndicatorToButton:(UIButton *)targetButton
{
    CABasicAnimation *moveAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    moveAnimation.fromValue = [NSValue valueWithCGPoint:self.indicatorView.center];
    CGPoint toValue = self.indicatorView.center;
    toValue.x = CGRectGetMidX(targetButton.frame);
    self.currentLeadingSpace = toValue.x - self.indicatorView.frame.size.width / 2;
    moveAnimation.toValue = [NSValue valueWithCGPoint:toValue];
    [self.indicatorView.layer addAnimation:moveAnimation forKey:nil];
    self.indicatorView.center = toValue;
    self.indicatorViewLeadingConstraint.constant = self.currentLeadingSpace;
}

- (void)configureStartValues
{
    self.currentLeadingSpace = [UIScreen mainScreen].bounds.size.width / 8;
    [self.routerButton setTitleColor:[UIColor darkGrayColorForText] forState:UIControlStateNormal];
    [self.wifiButton setTitle:NSLocalizedString(@"add_device.nav_page_one", nil) forState:UIControlStateNormal];
    [self.routerButton setTitle:NSLocalizedString(@"add_device.nav_page_two", nil) forState:UIControlStateNormal];
}

- (void)updateUIIfNeeded
{
    if (IS_IPHONE_4_OR_LESS) {
        self.pageIndicatorBottomConstraint.constant -= 30;
    }
    self.fakeNavigationBarHeightConstraint.constant = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height;
}

- (void)setupNavigationBar
{
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor grayForDetailSettingsNavigationBar];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIFont *titleFont = [UIFont camFontZonaProRegularWithSize:20];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                    NSFontAttributeName : titleFont};
}

@end