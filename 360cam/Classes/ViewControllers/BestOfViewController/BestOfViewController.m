//
//  BestOfViewController.m
//  360cam
//
//  Created by Norbert Citrak on 7/1/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "BestOfViewController.h"
#import "ShareImportViewController.h"
#import "MediaFileDetailsViewController.h"
#import "RootViewController.h"

#import "LoadImageManager.h"

static NSString *const DetailedSegueIdentifier = @"showMediaFileDetailsBestOfSegue";
static NSString *const ShareSequeIdentifier = @"shareExportSeque";

@interface BestOfViewController ()

@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) __block NSArray *commonDataSource;

@end

@implementation BestOfViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self localizeUI];
    if ([self isTutorialHidden]) {
        [self loadData];
    } else {
        self.dataSource = self.demoDataSource;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = @"";
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UniversalSwipableTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentefier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isTutorialHidden]) {
        [self performSegueWithIdentifier:DetailedSegueIdentifier sender:self];
    }
}

#pragma mark - UniversalSwipableTableViewCellDelegate

- (void)swipeCellLeftButtonDidPressed:(UniversalSwipableTableViewCell *)cell
{
    if ([self isTutorialHidden]) {
        [self performSegueWithIdentifier:ShareSequeIdentifier sender:cell];
    }
}

- (void)swipeCellMoreButtonPressed:(UniversalSwipableTableViewCell *)cell
{
    NSLog(@"More");
}

#pragma mark - IBActions

- (IBAction)photosButtonAction:(id)sender
{
    [super photosButtonAction:sender];
    [self updateDataSource];
}

- (IBAction)videosButtonAction:(id)sender
{
    [super videosButtonAction:sender];
    [self updateDataSource];
}

#pragma mark - Navigation

- (void)prepareForSegue:(nonnull UIStoryboardSegue *)segue sender:(nullable id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UniversalSwipableTableViewCell *cell = (UniversalSwipableTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];

    if ([segue.identifier isEqualToString:DetailedSegueIdentifier]) {
        MediaFileDetailsViewController *mediaDetailsViewController = segue.destinationViewController;
        mediaDetailsViewController.dataSource = self.dataSource;
        mediaDetailsViewController.viewerType = ViewerTypeLocalBestOf;
        mediaDetailsViewController.itemIndex = indexPath.row;
        mediaDetailsViewController.thumbnail = cell.thumbnailImageView.image;
        mediaDetailsViewController.mediaType = self.selectedMediaType;
    } else if ([segue.identifier isEqualToString:ShareSequeIdentifier]) {
        [AppHelper rootViewControllerSideMenu].panGestureEnabled = NO;
        cell = (UniversalSwipableTableViewCell *)sender;
        ShareImportViewController *shareVC = (ShareImportViewController *)((UINavigationController *)segue.destinationViewController).topViewController;
        shareVC.thumbnail = cell.thumbnailImageView.image;
        shareVC.detailDataSource = [self.dataSource objectAtIndex:indexPath.row];
        [cell hideCellButtons:nil];
        __weak typeof(self) weakSelf = self;
        shareVC.didCloseViewController = ^() {
            [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
        };
    }
}

#pragma mark - Networking

- (void)loadData
{
    if (!self.commonDataSource) {
        self.commonDataSource = [[NSArray alloc] init];
    }
        
    NSURLSession *session = [[LoadImageManager sharedManager] urlSession];
    NSString *baseURL = [[LoadImageManager sharedManager] baseURLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[baseURL stringByAppendingPathComponent:@"app.json"]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0.f];
    [request setHTTPMethod:@"GET"];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSArray *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            weakSelf.commonDataSource = jsonResponse;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateDataSource];
            });
        }
    }];
    [dataTask resume];
}

#pragma mark - Override

- (void)tutorialFinished
{
    NSMutableArray *idxPaths = [NSMutableArray array];
    for (int i = 0; i < self.dataSource.count; i++) {
        [idxPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    self.dataSource = nil;
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:idxPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];

    [self loadData];
}

#pragma mark - Private

- (void)updateDataSource
{
    if (!self.commonDataSource) {
        return;
    }
    
    NSString *type = @"photo";
    if (self.selectedMediaType) {
        type = @"video";
    }
    self.dataSource = [[NSArray alloc] init];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *element in self.commonDataSource) {
        if ([[element objectForKey:@"type"] isEqualToString:type]) {
            [array addObject:element];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:NO];
    self.dataSource = [array sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)configureCell:(UniversalSwipableTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (![self isTutorialHidden]) {
        [super configureCell:cell atIndexPath:indexPath];
    } else {
        cell.delegate = self;
        
        NSDictionary *sourceDictionary = self.dataSource[indexPath.row];
        cell.localTitleLabel.text = [sourceDictionary valueForKey:@"title"];
        cell.localTypeImageView.image = [self getImageForAssetType:[sourceDictionary valueForKey:@"type"]];
        cell.localFileTypeLabel.text = [[sourceDictionary valueForKey:@"type"] uppercaseString];
        
        [self setImageForCell:cell withSource:sourceDictionary];
    }
    cell.cellGalleryType = CellGalleryTypeLocal;
    cell.cellSwipeType = SwipeTypeLeft;
    [cell setContentForSelectedCellType];
    cell.buttonWidth = CGRectGetWidth([UIScreen mainScreen].bounds) * 0.23f;

    [cell setImageForLeftButton:[UIImage imageNamed:@"ic_share"]];
    [cell setLeftSectionBackgroundColor:[UIColor lightBlueButtonBackgroundColor]];
}

- (void)setImageForCell:(UniversalSwipableTableViewCell *)cell withSource:(NSDictionary *)sourceDictionary
{
    NSString *baseURL = [[LoadImageManager sharedManager] baseURLString];
    NSString *imageURL = [NSString stringWithFormat:@"%@%@", baseURL,sourceDictionary[@"thumb_path"]];
    
    [[LoadImageManager sharedManager] loadImageWithURL:imageURL complemention:^(NSData *imageData) {
        cell.thumbnailImageView.image = [UIImage imageWithData:imageData];
    }];
}

- (void)localizeUI
{
    [super localizeUI];
    
    NSString *gallaryTitle = NSLocalizedString(@"local.best_of.title", nil);
    [self setGalleryTitle:gallaryTitle];
}

@end