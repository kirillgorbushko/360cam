//
//  DetailedSettingViewController.m
//  360cam
//
//  Created by Misha Gajdan on 20.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "DetailedSettingViewController.h"
#import "DetailedSettingsTableViewCell.h"
#import "UINavigationController+Transparent.h"

static NSString *const SelectModeCellIdentifier = @"SelectModeCell";

@interface DetailedSettingViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fakeNavBarHeightConstraint;

@property (strong, nonatomic) NSArray *options;
@property (assign, nonatomic) NSInteger indexOfSelectedOption;

@end

@implementation DetailedSettingViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.options) {
        self.options = @[@"text1", @"text2", @"text3", @"text4"];
        self.indexOfSelectedOption = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
    [self updateUIIfNeeded];
    [self setupTableView];
}

#pragma mark - Public

- (void)setOptions:(NSArray *)options withSelectedOption:(NSString *)option
{
    self.options = options;
    self.indexOfSelectedOption = [options indexOfObject:option];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailedSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectModeCellIdentifier forIndexPath:indexPath];
    cell.descriptionLabel.text = self.options[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.indexOfSelectedOption inSection:0]];
    cell.selected = NO;
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    newCell.selected = YES;
    
    self.indexOfSelectedOption = indexPath.row;
}

#pragma mark - Private

- (void)setupNavigationBar
{
    [self.navigationController presentTransparentNavigationBarAnimated:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIFont *titleFont = [UIFont camFontZonaProRegularWithSize:20];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                    NSFontAttributeName : titleFont};
}

- (void)setupTableView
{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
}

- (void)updateUIIfNeeded
{
    self.fakeNavBarHeightConstraint.constant = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.frame.size.height;
}

#pragma mark - IBAction

- (void)backAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
