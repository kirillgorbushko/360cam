//
//  DetailedSettingViewController.h
//  360cam
//
//  Created by Misha Gajdan on 20.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface DetailedSettingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (void)setOptions:(NSArray *)options withSelectedOption:(NSString *)option;

@end
