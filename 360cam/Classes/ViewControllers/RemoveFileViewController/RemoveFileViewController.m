//
//  RemoveFileViewController.m
//  360cam
//
//  Created by Kirill Gorbushko on 01.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "RemoveFileViewController.h"
#import "Animation.h"
#import "AssetsManager.h"
#import "MBProgressHUD.h"
#import "RootViewController.h"

@interface RemoveFileViewController ()

@property (weak, nonatomic) IBOutlet UILabel *assetNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end

@implementation RemoveFileViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self performUILocalization];
    [self setStyleForButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
    [self forceRotation];
}

- (void)forceRotation
{
    [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

#pragma mark - Animation

- (void)animationDidStop:(nonnull CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.view.layer animationForKey:@"dismiss"]) {
        [self.view.layer removeAllAnimations];
        if (self.didCloseView) {
            self.didCloseView(NO);
        }
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - IBActions

- (IBAction)removeButtonPressed:(id)sender
{

}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self.view.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"dismiss"];
    self.view.layer.opacity = 0.f;
}

#pragma mark - Private

- (void)performUILocalization
{
    self.assetNameLabel.text = [NSLocalizedString(@"remove_file.title", nil) stringByAppendingString:[self getImageName]];
    self.informationLabel.text = NSLocalizedString(@"remove_file.information", nil);
    [self.removeButton setTitle:NSLocalizedString(@"remove_file.button_title.remove", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"remove_file.button_title.cancel", nil) forState:UIControlStateNormal];
}

- (void)setStyleForButton
{
    self.removeButton.layer.cornerRadius = self.removeButton.frame.size.height / 2;
    self.removeButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.removeButton.layer.borderWidth = 1.f;
}

- (NSString *)getImageName
{
    return @"0145";
}

@end
