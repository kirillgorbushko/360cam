//
//  RemoveFileViewController.h
//  360cam
//
//  Created by Kirill Gorbushko on 01.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface RemoveFileViewController : UIViewController

@property (strong, nonatomic) NSURL *sourceUrl;
@property (strong, nonatomic) void (^didCloseView)(BOOL afterDeleteAsset);

@end
