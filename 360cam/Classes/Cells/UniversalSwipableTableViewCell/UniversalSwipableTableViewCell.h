//
//  BestOfTableViewCell.h
//  360cam
//
//  Created by Norbert Citrak on 6/25/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@class UniversalSwipableTableViewCell;

typedef NS_ENUM(NSUInteger, SwipeType) {
    SwipeTypeNone = 0,
    SwipeTypeLeft = 1 << 0,
    SwipeTypeRight = 1 << 1,
};

typedef NS_ENUM(NSUInteger, CellGalleryType) {
    CellGalleryTypeUndefined,
    CellGalleryTypeRemote,
    CellGalleryTypeLocal
};

typedef void(^Completion)(BOOL);

@protocol UniversalSwipableTableViewCellDelegate <NSObject>

@optional
- (void)swipeCellRightButtonDidPressed:(UniversalSwipableTableViewCell *)cell;
- (void)swipeCellLeftButtonDidPressed:(UniversalSwipableTableViewCell *)cell;
- (void)swipeCellRightButtonWillShow:(UniversalSwipableTableViewCell *)cell;
- (void)swipeCellLeftButtonWillShow:(UniversalSwipableTableViewCell *)cell;

- (void)swipeCellMoreButtonPressed:(UniversalSwipableTableViewCell *)cell;

- (void)swipeCellDidOpened:(UniversalSwipableTableViewCell *)cell;
- (void)swipeCellDidClosed:(UniversalSwipableTableViewCell *)cell;

- (void)swipeCellDidBeginSwipe:(UniversalSwipableTableViewCell *)cell;

- (void)swipeCellRightSwipeDidEnd:(UniversalSwipableTableViewCell *)cell;
- (void)swipeCellLeftSwipeDidEnd:(UniversalSwipableTableViewCell *)cell;

- (void)swipeCellDidCancel:(UniversalSwipableTableViewCell *)cell;

@end

@interface UniversalSwipableTableViewCell : UITableViewCell <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *localTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *localTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *localFileTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *remoteFileTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *remoteFileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *remoteFileDetailsLabel;

@property (assign, nonatomic) CGFloat buttonWidth;

@property (assign, nonatomic) BOOL disableLeftSwipe;
@property (assign, nonatomic) BOOL disableRightSwipe;
@property (assign, nonatomic) BOOL lockButtons;

@property (weak, nonatomic) id <UniversalSwipableTableViewCellDelegate> delegate;
@property (assign, nonatomic) SwipeType cellSwipeType;
@property (assign, nonatomic) CellGalleryType cellGalleryType;

- (void)setTitleForRightButton:(NSString *)title;
- (void)setImageForRightButton:(UIImage *)image;
- (void)setImageForRightButtonSelectedMode:(UIImage *)image;

- (void)setTitleForLeftButton:(NSString *)title;
- (void)setImageForLeftButton:(UIImage *)image;
- (void)setImageForLeftButtonSelectedMode:(UIImage *)image;

- (void)setLeftSectionBackgroundColor:(UIColor *)color;
- (void)setRightSectionBackgroundColor:(UIColor *)color;

- (void)setContentForSelectedCellType;
- (void)hideCellButtons:(Completion)completionHandler;

- (void)makeLeftCellButtonsVisible;
- (void)makeRightCellButtonsVisible;

- (void)showAndHideLeftButtonWithCompletion:(void (^)())completion;
- (void)showAndHideRightButtonWithCompletion:(void (^)())completion;

@end
