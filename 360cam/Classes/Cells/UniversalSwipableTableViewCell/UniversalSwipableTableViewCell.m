//
//  BestOfTableViewCell.m
//  360cam
//
//  Created by Norbert Citrak on 6/25/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UniversalSwipableTableViewCell.h"

static CGFloat const minDelta = 3.f;
static CGFloat const animationDuration = 0.4f;

@interface UniversalSwipableTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tralingMainViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingMainViewConstraint;

@property (weak, nonatomic) IBOutlet UIView *localGalleryContentView;
@property (weak, nonatomic) IBOutlet UIView *remoteGalleryContentView;

@property (assign, nonatomic) CGPoint startTouch;
@property (assign, nonatomic) CGFloat startLeftPosition;
@property (assign, nonatomic) CGFloat startRightPosition;

@property (assign, nonatomic) SwipeType typeOfViewToShow;

@end

@implementation UniversalSwipableTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(cellPanGestureRecognizer:)];
    panGesture.delegate = self;
    [self addGestureRecognizer:panGesture];
    
    self.leadingMainViewConstraint.constant = 0;
    self.tralingMainViewConstraint.constant = 0;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (!self.buttonWidth) {
        self.buttonWidth = 100.f;
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.leadingMainViewConstraint.constant = 0;
    self.tralingMainViewConstraint.constant = 0;
    
    self.localFileTypeLabel.text = @"";
    self.localTitleLabel.text = @"";
    self.localTypeImageView.image = nil;
    
    self.remoteFileDetailsLabel.text = @"";
    self.remoteFileNameLabel.text = @"";
    self.remoteFileTypeImageView.image = nil;
    
    self.thumbnailImageView.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

#pragma mark - Public

#pragma mark - RightButton

- (void)setTitleForRightButton:(NSString *)title
{
    if (title.length) {
        [self.rightButton setTitle:title forState:UIControlStateNormal];
    }
}

- (void)setImageForRightButton:(UIImage *)image
{
    if (image) {
        [self.rightButton setImage:image forState:UIControlStateNormal];
    }
}

- (void)setImageForRightButtonSelectedMode:(UIImage *)image
{
    if (image) {
        [self.rightButton setImage:image forState:UIControlStateHighlighted];
        [self.rightButton setImage:image forState:UIControlStateSelected];
    }
}

#pragma mark - LeftButton

- (void)setTitleForLeftButton:(NSString *)title
{
    if (title.length) {
        [self.leftButton setTitle:title forState:UIControlStateNormal];
    }
}

- (void)setImageForLeftButton:(UIImage *)image
{
    if (image) {
        [self.leftButton setImage:image forState:UIControlStateNormal];
    }
}

- (void)setImageForLeftButtonSelectedMode:(UIImage *)image
{
    if (image) {
        [self.leftButton setImage:image forState:UIControlStateHighlighted];
        [self.leftButton setImage:image forState:UIControlStateSelected];
    }
}

#pragma mark - BackgroundColors

- (void)setRightSectionBackgroundColor:(UIColor *)color
{
    self.rightView.backgroundColor = color;
}

- (void)setLeftSectionBackgroundColor:(UIColor *)color
{
    self.leftView.backgroundColor = color;
}

#pragma mark - CellControlls

- (void)setContentForSelectedCellType
{
    switch (self.cellGalleryType) {
        case CellGalleryTypeLocal: {
            self.localGalleryContentView.hidden = NO;
            break;
        }
        case CellGalleryTypeRemote: {
            self.remoteGalleryContentView.hidden = NO;
            break;
        }
        default:
            break;
    }
}

- (void)hideCellButtons:(Completion)completionHandler
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration animations:^{
        weakSelf.leadingMainViewConstraint.constant = 0;
        weakSelf.tralingMainViewConstraint.constant = 0;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (completionHandler) {
            completionHandler(finished);
        }
    }];
}

#pragma mark - IBAction

- (IBAction)leftButtonButtonPressed:(id)sender
{
    if (!self.lockButtons && [self.delegate respondsToSelector:@selector(swipeCellLeftButtonDidPressed:)]) {
        [self.delegate swipeCellLeftButtonDidPressed:self];
    }
}

- (IBAction)rightButtonButtonPressed:(id)sender
{
    if (!self.lockButtons && [self.delegate respondsToSelector:@selector(swipeCellRightButtonDidPressed:)]) {
        [self.delegate swipeCellRightButtonDidPressed:self];
    }
}

- (IBAction)moreButtonPressed:(id)sender
{
    if (!self.lockButtons && [self.delegate respondsToSelector:@selector(swipeCellMoreButtonPressed:)]) {
        [self.delegate swipeCellMoreButtonPressed:self];
    }
}

#pragma mark - Gesture

- (void)cellPanGestureRecognizer:(UIGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan: {
            self.startTouch = [sender locationInView:self];
            self.startLeftPosition = self.leadingMainViewConstraint.constant;
            self.startRightPosition = self.tralingMainViewConstraint.constant;
            break;
        }
        case UIGestureRecognizerStateChanged: {
            
            CGPoint location = [sender locationInView:self];
            CGFloat delta = (self.startTouch.x - location.x);
            
            if (ABS(delta) > self.buttonWidth / 2) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(swipeCellDidBeginSwipe:)]){
                    [self.delegate swipeCellDidBeginSwipe:self];
                }
            }
            
            BOOL canMove = NO;
            
            switch (self.cellSwipeType) {
                case SwipeTypeRight: {
                    canMove = delta <= 0 ? NO : YES;
                    if (self.disableRightSwipe) {
                        canMove = NO;
                    }
                    break;
                }
                case SwipeTypeLeft: {
                    canMove = delta >= 0 ? NO : YES;
                    if (self.disableLeftSwipe) {
                        canMove = NO;
                    }
                    break;
                }
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
                case (SwipeTypeLeft | SwipeTypeRight): {
#pragma GCC diagnostic pop
                    
                    canMove = fabs(delta) > minDelta;

                    break;
                }
                default:
                    break;
            }
            
            if (canMove) {
                if (delta < 0) {
                    self.typeOfViewToShow = (self.cellSwipeType == SwipeTypeLeft || self.cellSwipeType == 3) ? SwipeTypeLeft : SwipeTypeNone;
                } else {
                    self.typeOfViewToShow = (self.cellSwipeType == SwipeTypeRight || self.cellSwipeType == 3) ? SwipeTypeRight : SwipeTypeNone;
                }
                
                if ((self.cellSwipeType == 3 && self.disableRightSwipe && self.typeOfViewToShow == SwipeTypeRight) ||
                    (self.cellSwipeType == 3 && self.disableLeftSwipe && self.typeOfViewToShow == SwipeTypeLeft)) {
                    return;
                }
                
                [self moveViewWithDelta:delta];
            } else if (self.tralingMainViewConstraint.constant || self.leadingMainViewConstraint.constant) {
                [self moveViewWithDelta:delta];
            }
            break;
        }
        case UIGestureRecognizerStateEnded: {
            
            if (self.typeOfViewToShow == SwipeTypeRight && self.tralingMainViewConstraint.constant > (self.buttonWidth / 2)) {
                
                if (self.tralingMainViewConstraint.constant > self.buttonWidth) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(swipeCellRightButtonWillShow:)]) {
                        [self.delegate swipeCellRightButtonWillShow:self];
                    }
                }
                [self makeRightCellButtonsVisible];
            } else if (self.typeOfViewToShow == SwipeTypeLeft && self.leadingMainViewConstraint.constant > (self.buttonWidth / 2)) {
                if (self.leadingMainViewConstraint.constant > self.buttonWidth) {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(swipeCellLeftButtonWillShow:)]) {
                        [self.delegate swipeCellLeftButtonWillShow:self];
                    }
                }
                [self makeLeftCellButtonsVisible];
            } else {
                if (self.typeOfViewToShow == SwipeTypeLeft && self.leadingMainViewConstraint.constant < self.buttonWidth) {
                    [self hideCellButtons:nil];
                } else if (self.typeOfViewToShow == SwipeTypeRight && self.tralingMainViewConstraint.constant < self.buttonWidth) {
                    [self hideCellButtons:nil];
                }

                if (self.delegate && [self.delegate respondsToSelector:@selector(swipeCellDidClosed:)]) {
                    [self.delegate swipeCellDidClosed:self];
                }
            }
            break;
        }
        default: {
            break;
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer
{
    if (![panGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        return YES;
    }
    CGPoint velocity = [panGestureRecognizer velocityInView:self];
    return fabs(velocity.x) > fabs(velocity.y);
}

#pragma mark - Private

- (void)moveViewWithDelta:(CGFloat)delta
{
    if (fabs(delta) > 20) {
        [UIView beginAnimations:@"moveAnimation" context:nil];
        [UIView setAnimationDuration:0.1];
    }
    
    CGFloat modDelta = delta / 1.5;
    
    self.tralingMainViewConstraint.constant = self.startRightPosition + modDelta;
    self.leadingMainViewConstraint.constant = self.startLeftPosition + (-modDelta);
    [self layoutIfNeeded];
    
    if (fabs(delta) > 20) {
        [UIView commitAnimations];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(swipeCellDidCancel:)]) {
        [self.delegate swipeCellDidCancel:self];
    }
}

- (void)makeLeftCellButtonsVisible
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration animations:^{
        weakSelf.leadingMainViewConstraint.constant = weakSelf.buttonWidth;
        weakSelf.tralingMainViewConstraint.constant = - weakSelf.buttonWidth;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(swipeCellDidOpened:)]) {
            [weakSelf.delegate swipeCellDidOpened:weakSelf];
        }
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(swipeCellLeftSwipeDidEnd:)]) {
            [weakSelf.delegate swipeCellLeftSwipeDidEnd:weakSelf];
        }
        
    }];
}

- (void)makeRightCellButtonsVisible
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration animations:^{
        weakSelf.tralingMainViewConstraint.constant = weakSelf.buttonWidth;
        weakSelf.leadingMainViewConstraint.constant = - weakSelf.buttonWidth;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(swipeCellDidOpened:)]) {
            [weakSelf.delegate swipeCellDidOpened:weakSelf];
        }
        
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(swipeCellRightSwipeDidEnd:)]) {
            [weakSelf.delegate swipeCellRightSwipeDidEnd:weakSelf];
        }
    }];
}

- (void)showAndHideLeftButtonWithCompletion:(void (^)())completion
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration * 1.5 animations:^{
        weakSelf.leadingMainViewConstraint.constant = weakSelf.buttonWidth * 1.5;
        weakSelf.tralingMainViewConstraint.constant = - weakSelf.buttonWidth * 1.5;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf hideCellButtons:^(BOOL finished) {
            if (completion && finished) {
                completion();
            }
        }];
    }];
}
- (void)showAndHideRightButtonWithCompletion:(void (^)())completion
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animationDuration * 1.5 delay:0.3 options:kNilOptions animations:^{
        weakSelf.tralingMainViewConstraint.constant = weakSelf.buttonWidth * 1.5;
        weakSelf.leadingMainViewConstraint.constant = - weakSelf.buttonWidth * 1.5;
        [weakSelf layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf hideCellButtons:^(BOOL finished) {
            if (completion && finished) {
                completion();
            }
        }];
    }];
}

@end