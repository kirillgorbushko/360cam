//
//  NotifyPhotoCollectionViewCell.h
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifyPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
