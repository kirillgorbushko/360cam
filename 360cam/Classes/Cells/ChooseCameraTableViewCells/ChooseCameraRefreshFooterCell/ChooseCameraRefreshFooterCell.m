//
//  ChooseCameraRefreshFooterCell.m
//  360cam
//
//  Created by Victor on 01.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ChooseCameraRefreshFooterCell.h"

@implementation ChooseCameraRefreshFooterCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.pullSearchLabel.text = @"";
}

@end
