//
//  ChooseCameraRefreshFooterCell.h
//  360cam
//
//  Created by Victor on 01.07.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface ChooseCameraRefreshFooterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pullSearchLabel;

@end
