//
//  ChooseCameraTableViewCell.m
//  360cam
//
//  Created by Victor on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ChooseCameraTableViewCell.h"

@interface ChooseCameraTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *chooseCameraImageView;
@property (weak, nonatomic) IBOutlet UILabel *chooseCameraStateLabel;
@property (weak, nonatomic) IBOutlet UIView *chooseCameraMaskView;
@property (weak, nonatomic) IBOutlet UIImageView *chooseCameraArrowImageView;

@end

@implementation ChooseCameraTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.chooseCameraStateLabel.text = @"";
    self.chooseCameraNameLabel.text = @"";
}

#pragma mark - Setters

- (void)setCameraState:(CameraState)cameraState
{
    _cameraState = cameraState;
    [self configureCellView];
}

#pragma mark - Private

- (void)configureCellView
{
    self.chooseCameraStateLabel.text = (self.cameraState == CameraStateEnable) ? NSLocalizedString(@"choose_camera.camera_active", nil) : NSLocalizedString(@"choose_camera.camera_disactive", nil);
    self.chooseCameraMaskView.hidden = (self.cameraState == CameraStateEnable);
    self.chooseCameraArrowImageView.image = (self.cameraState == CameraStateEnable) ? [UIImage imageNamed:@"cc_arrow_right_enable"] : [UIImage imageNamed:@"cc_arrow_right_disable"];
    self.chooseCameraNameLabel.textColor = (self.cameraState == CameraStateEnable) ? [UIColor chooseCamDarkBlack] : [UIColor chooseCamLightBlack];
    self.chooseCameraStateLabel.textColor = (self.cameraState == CameraStateEnable) ? [UIColor chooseCamLightBlue] : [UIColor chooseCamLightBlack];
}

@end