//
//  ChooseCameraTableViewCell.h
//  360cam
//
//  Created by Victor on 30.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef NS_ENUM(NSUInteger, CameraState) {
    CameraStateEnable,
    CameraStateDisable
};

@interface ChooseCameraTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chooseCameraNameLabel;

@property (assign, nonatomic) CameraState cameraState;

@end
