//
//  AddDeviceCollectionViewCell.m
//  360cam
//
//  Created by Victor on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AddDeviceCollectionViewCell.h"

@interface AddDeviceCollectionViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionBottomSpace;

@end

@implementation AddDeviceCollectionViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self updateUIIfNeeded];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.contentImageView.image = nil;
    [self updateUIIfNeeded];
}

#pragma mark - Private

- (void)updateUIIfNeeded
{
    if (IS_IPHONE_4_OR_LESS) {
        self.imageHeightConstraint.constant = 300.f;
        self.descriptionBottomSpace.constant = 40.f;
    } else if (IS_IPHONE_6 || IS_IPHONE_6P) {
        self.imageHeightConstraint.constant = 400.f;
    }
}

@end
