//
//  AddDeviceCollectionViewCell.h
//  360cam
//
//  Created by Victor on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface AddDeviceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
