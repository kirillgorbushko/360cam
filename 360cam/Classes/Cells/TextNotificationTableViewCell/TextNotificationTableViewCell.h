//
//  TextNotificationTableViewCell.h
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ResizingLabel.h"

static NSString *const TimelineHeartIconName = @"ic_timeline_heart";
static NSString *const ChatIconName = @"ic_chat";
static NSString *const PlusIconName = @"ic_plus";

@interface TextNotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *notificationImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationCreatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationMessage;
@property (weak, nonatomic) IBOutlet ResizingLabel *notificationDiscriptionLabel;

@end
