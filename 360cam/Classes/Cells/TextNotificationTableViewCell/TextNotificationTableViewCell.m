//
//  TextNotificationTableViewCell.m
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TextNotificationTableViewCell.h"

@implementation TextNotificationTableViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self initialSettings];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.notificationImageView.image = nil;
    self.userNameLabel.text = @"";
    self.notificationCreatedLabel.text = @"";
    self.notificationMessage.text = @"";
    self.notificationDiscriptionLabel.text = @"";
}

#pragma mark - Private

- (void)initialSettings
{
    self.notificationImageView.layer.borderColor = [UIColor lightGrayColorForNotificationImageBorder].CGColor;
    self.notificationImageView.layer.borderWidth = 1.f;
    self.notificationImageView.layer.cornerRadius = CGRectGetHeight(self.notificationImageView.bounds) / 2;
}

@end
