//
//  PhotoNotificationTableViewCell.m
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "PhotoNotificationTableViewCell.h"
#import "NotifyPhotoCollectionViewCell.h"

static NSString *const NotifyPhotoCellIdentefier = @"NotifyPhotoCell";
static NSInteger const ImageInLine = 4;

@interface PhotoNotificationTableViewCell ()

@property (strong, nonatomic) NSArray *imageArray;

@end

@implementation PhotoNotificationTableViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.notificationDiscriptionLabel.text = @"";
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.imageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NotifyPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NotifyPhotoCellIdentefier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellSideDimension = [self getCellSideDimension];
    return CGSizeMake(cellSideDimension, cellSideDimension);
}

#pragma mark - Private

- (NSInteger)getCellSideDimension
{
    CGFloat mainScreenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    CGFloat collectionViewWidth = CGRectGetWidth(self.collectionView.bounds);
    CGFloat collectionViewOffset = mainScreenWidth - collectionViewWidth;
    CGFloat sideDimension = mainScreenWidth - collectionViewOffset;
    return sideDimension / ImageInLine;
}

- (void)configureCell:(UICollectionViewCell *)collectionViewCell atIndexPath:(NSIndexPath *)indexPath
{
    NotifyPhotoCollectionViewCell *cell = (NotifyPhotoCollectionViewCell *)collectionViewCell;
    
    cell.imageView.image = [UIImage imageNamed:[self.imageArray objectAtIndex:indexPath.row]];
}

@end
