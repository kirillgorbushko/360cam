//
//  PhotoNotificationTableViewCell.h
//  360cam
//
//  Created by Norbert Citrak on 7/6/15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TextNotificationTableViewCell.h"

@interface PhotoNotificationTableViewCell : TextNotificationTableViewCell <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTrailingConstraint;

- (void)setImageArray:(NSArray *)imageArray;

@end
