//
//  SettingsDetailedTableViewCell/h
//  360cam
//
//  Created by Misha Gajdan on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface SettingsDetailedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *directionImage;

@end
