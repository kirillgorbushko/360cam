//
//  SettingsDetailedTableViewCell.m
//  360cam
//
//  Created by Misha Gajdan on 25.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "SettingsDetailedTableViewCell.h"

@implementation SettingsDetailedTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([self respondsToSelector:@selector(layoutMargins)]) {
        self.layoutMargins = UIEdgeInsetsZero;
    }
}

@end
