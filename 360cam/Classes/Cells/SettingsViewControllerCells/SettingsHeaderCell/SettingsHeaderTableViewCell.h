//
//  SettingsHeaderTableViewCell.h
//  360cam
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface SettingsHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
