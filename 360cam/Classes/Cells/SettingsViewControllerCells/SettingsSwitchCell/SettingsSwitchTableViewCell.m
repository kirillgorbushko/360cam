//
//  SettingsSwitchTableViewCell.m
//  360cam
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "SettingsSwitchTableViewCell.h"

@implementation SettingsSwitchTableViewCell

- (IBAction)switchValueChanged:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(valueChanged:forKey:sender:)]){
        [self.delegate valueChanged:self.switchControl.isOn forKey:self.keyValue sender:self];
    }    
}

@end
