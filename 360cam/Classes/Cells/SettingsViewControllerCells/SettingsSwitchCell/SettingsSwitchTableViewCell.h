//
//  SettingsSwitchTableViewCellProtocol
//  360cam
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@protocol SettingsSwitchTableViewCellProtocol <NSObject>

- (void)valueChanged:(BOOL)newValue forKey:(NSString *)key sender:(id)sender;

@end

@interface SettingsSwitchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;

@property (copy, nonatomic) NSString *keyValue;
@property (weak, nonatomic) id<SettingsSwitchTableViewCellProtocol> delegate;

@end
