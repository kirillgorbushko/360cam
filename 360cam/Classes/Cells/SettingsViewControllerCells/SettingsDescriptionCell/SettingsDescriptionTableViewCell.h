//
//  SettingsDescriptionTableViewCell.h
//  360cam
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface SettingsDescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelDesciption;
@property (weak, nonatomic) IBOutlet UILabel *labelMode;
@property (weak, nonatomic) IBOutlet UIImageView *directionImage;

@end
