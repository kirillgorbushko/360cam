//
//  SettingsDescriptionTableViewCell.m
//  360cam
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "SettingsDescriptionTableViewCell.h"

@implementation SettingsDescriptionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([self respondsToSelector:@selector(layoutMargins)]) {
        self.layoutMargins = UIEdgeInsetsZero;
    }
}

@end
