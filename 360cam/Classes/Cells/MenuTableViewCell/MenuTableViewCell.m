//
//  MenuTableViewCell.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "MenuTableViewCell.h"

static CGFloat TitleLeadingDefaultSpace = 16;

@interface MenuTableViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingItemNameLabelConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingImageViewConstraint;

@end

@implementation MenuTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        [self setStyleForPhone5OrLess];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.menuItemNameLabel.text = @"";
    self.menuLogoImage.image = nil;
    self.menuNotificationLabel.text = @"";
    self.menuItemNameLabel.font = [UIFont camFontZonaProBoldWithSize:14.f];
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5) {
        self.leadingItemNameLabelConstraint.constant = TitleLeadingDefaultSpace / 2;
    } else {
        self.leadingItemNameLabelConstraint.constant = TitleLeadingDefaultSpace;
    }
}

#pragma mark - Public

- (void)setSubMenuStyle
{
    self.menuItemNameLabel.font = [UIFont camFontZonaProThinWithSize:14.f];
    self.menuItemNameLabel.font = [self.menuItemNameLabel.font fontWithSize:14.f];
    self.leadingItemNameLabelConstraint.constant *= 2.5;
}

- (void)setStyleForPhone5OrLess
{
    self.leadingImageViewConstraint.constant /= 2;
    self.leadingItemNameLabelConstraint.constant /= 2;
}

@end
