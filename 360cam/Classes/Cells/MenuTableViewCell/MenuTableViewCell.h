//
//  MenuTableViewCell.h
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuLogoImage;
@property (weak, nonatomic) IBOutlet UILabel *menuItemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuNotificationLabel;

- (void)setSubMenuStyle;

@end
