//
//  ShareImportCollectionViewVerticalCell.h
//  ShareImportViewController
//
//  Created by Misha Gajdan on 26.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface ModeSelectionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
