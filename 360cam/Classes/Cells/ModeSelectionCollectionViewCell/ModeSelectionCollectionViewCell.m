//
//  ShareImportCollectionViewVerticalCell.m
//  ShareImportViewController
//
//  Created by Misha Gajdan on 26.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "ModeSelectionCollectionViewCell.h"

@implementation ModeSelectionCollectionViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.mainImage.image = nil;
    self.descriptionLabel.text = @"";
    self.descriptionLabel.tintColor = [UIColor whiteColor];
    
    self.selected = NO;
    
}

@end
