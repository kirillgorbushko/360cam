//
//  ShareImportCollectionViewVerticalCell.m
//  ShareImportViewController
//
//  Created by Misha Gajdan on 26.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "ShareImportCollectionViewCell.h"

@implementation ShareImportCollectionViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.descriptionLabel.text = @"";
    self.mainImage.image = nil;
}

@end
