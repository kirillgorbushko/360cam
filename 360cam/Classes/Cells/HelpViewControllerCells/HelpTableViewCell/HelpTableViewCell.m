//
//  HelpTableViewCell.m
//  360cam
//
//  Created by Misha Gajdan on 24.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "HelpTableViewCell.h"

@implementation HelpTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.mainImageView.image = nil;
    self.descriptionLabel.text = @"";
}

@end
