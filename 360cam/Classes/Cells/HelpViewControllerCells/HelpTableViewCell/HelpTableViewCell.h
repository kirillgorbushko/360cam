//
//  HelpTableViewCell.h
//  360cam
//
//  Created by Misha Gajdan on 24.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface HelpTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
