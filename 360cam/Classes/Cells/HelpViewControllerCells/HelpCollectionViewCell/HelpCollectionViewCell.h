//
//  CollectionViewCell.h
//  SettingsView
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface HelpCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
