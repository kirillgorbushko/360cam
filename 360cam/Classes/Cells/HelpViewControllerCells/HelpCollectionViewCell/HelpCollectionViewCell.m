//
//  CollectionViewCell.m
//  SettingsView
//
//  Created by Misha Gajdan on 16.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "HelpCollectionViewCell.h"

@implementation HelpCollectionViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.descriptionLabel.text = @"";
}

@end
