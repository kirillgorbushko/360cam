//
//  HomeTableViewCell.h
//  360cam
//
//  Created by Misha Gajdan on 24.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface HomeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemsCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageLeft;

@end
