//
//  PhotoNameTableViewCell.m
//  360cam
//
//  Created by Kirill Gorbushko on 06.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "PhotoNameTableViewCell.h"

@implementation PhotoNameTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.fileNameLabel.text = @"";
}

@end
