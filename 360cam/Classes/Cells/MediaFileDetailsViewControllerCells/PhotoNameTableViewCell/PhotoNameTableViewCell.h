//
//  PhotoNameTableViewCell.h
//  360cam
//
//  Created by Kirill Gorbushko on 06.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface PhotoNameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;

@end
