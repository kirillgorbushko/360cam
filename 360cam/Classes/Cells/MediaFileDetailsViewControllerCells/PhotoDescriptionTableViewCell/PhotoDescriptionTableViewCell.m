//
//  PhotoDescriptionTableViewCell.m
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "PhotoDescriptionTableViewCell.h"

@interface PhotoDescriptionTableViewCell()

@end

@implementation PhotoDescriptionTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.titleLabel.text = @"";
    self.descriptionLabel.text = @"";
    self.createdLabel.text = @"";
    self.createdValueLabel.text = @"";
}


@end