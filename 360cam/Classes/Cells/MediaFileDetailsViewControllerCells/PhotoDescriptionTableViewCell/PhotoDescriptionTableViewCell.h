//
//  PhotoDescriptionTableViewCell.h
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface PhotoDescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdValueLabel;

@end
