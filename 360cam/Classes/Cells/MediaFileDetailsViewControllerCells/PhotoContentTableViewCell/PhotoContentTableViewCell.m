//
//  PhotoContentTableViewCell.m
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "PhotoContentTableViewCell.h"

@implementation PhotoContentTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.photoContentImageView.image = nil;
}

@end
