//
//  PhotoContentTableViewCell.h
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface PhotoContentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoContentImageView;

@end
