//
//  TimelapseTableViewCell.h
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

@interface TimelapseTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *timelapseImageView;
@property (weak, nonatomic) IBOutlet UILabel *timelapseTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timelapseDescriptionLabel;

@end
