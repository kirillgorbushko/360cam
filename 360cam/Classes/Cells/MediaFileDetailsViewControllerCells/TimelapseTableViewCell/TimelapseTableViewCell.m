//
//  TimelapseTableViewCell.m
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "TimelapseTableViewCell.h"

@implementation TimelapseTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.timelapseImageView.image = nil;
    self.timelapseTitleLabel.text = @"";
    self.timelapseDescriptionLabel.text = @"";
}

@end
