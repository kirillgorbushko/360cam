//
//  MapDetailTableViewCell.m
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "MapDetailTableViewCell.h"
#import "MapLocation.h"

@interface MapDetailTableViewCell()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapDetailTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.mapView.userInteractionEnabled = NO;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.locationLabel.text = @"";
    self.locationValueLabel.text = @"";
    self.gpsLabel.text = @"";
    self.gpsValueLabel.text = @"";
    [self.mapView removeAnnotations:self.mapView.annotations];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
     if ([annotation isKindOfClass:[MapLocation class]]) {
         MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
            annotationView.image = [UIImage imageNamed:@"icn_pin"];
         return annotationView;
    }
    return [MKAnnotationView new];
}

#pragma mark - Setters

- (void)setLocation:(CLLocationCoordinate2D)location
{
  _location = location;
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        MapLocation *mapLocation = [[MapLocation alloc] init];
        mapLocation.coordinate = location;
        if (weakSelf.mapView.annotations.count) {
            [weakSelf.mapView removeAnnotations:weakSelf.mapView.annotations];
        }
        [weakSelf.mapView addAnnotation:mapLocation];
        
        MKUserLocation *userLocation = weakSelf.mapView.userLocation;
        CGFloat distanceKM = 1 * 1000;
        if (weakSelf.distanceKm) {
            distanceKM = weakSelf.distanceKm * 1000;
        }
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, distanceKM, distanceKM);
        [weakSelf.mapView setRegion:region animated:NO];
        
        weakSelf.mapView.centerCoordinate = weakSelf.location;
    });
}

@end
