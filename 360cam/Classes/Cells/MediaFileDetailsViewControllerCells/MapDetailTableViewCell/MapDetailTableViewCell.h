//
//  MapDetailTableViewCell.h
//  360cam
//
//  Created by Victor on 29.06.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MapDetailTableViewCell : UITableViewCell <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *gpsLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *gpsValueLabel;

@property (assign, nonatomic) CLLocationCoordinate2D location;
@property (assign, nonatomic) CGFloat distanceKm;

@end
