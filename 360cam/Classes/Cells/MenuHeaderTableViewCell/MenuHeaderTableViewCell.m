//
//  MenuHeaderTableViewCell.m
//  
//
//  Created by Kirill Gorbushko on 24.06.15.
//
//

#import "MenuHeaderTableViewCell.h"
#import "RootViewController.h"

@implementation MenuHeaderTableViewCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.contentView.backgroundColor = [AppHelper rootViewControllerSideMenu].view.backgroundColor;
}

@end
