//
//  DetailedSettingsTableViewCell.m
//  360cam
//
//  Created by Misha Gajdan on 20.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "DetailedSettingsTableViewCell.h"

@implementation DetailedSettingsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    if ([self respondsToSelector:@selector(layoutMargins)]) {
        self.layoutMargins = UIEdgeInsetsZero;
    }
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected) {
        self.selectionImageView.image = [UIImage imageNamed:@"detailedSettings_ringFilled"];
    } else {
        self.selectionImageView.image = [UIImage imageNamed:@"detailedSettings_ring"];
    }
}

@end
