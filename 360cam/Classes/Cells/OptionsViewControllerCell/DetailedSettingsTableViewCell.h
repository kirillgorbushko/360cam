//
//  DetailedSettingsTableViewCell.h
//  360cam
//
//  Created by Misha Gajdan on 20.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@interface DetailedSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end
