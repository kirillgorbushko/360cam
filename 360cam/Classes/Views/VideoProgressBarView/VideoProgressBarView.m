//
//  ProgressBarView.m
//  PlayerMobileViewController
//
//  Created by Misha Gajdan on 29.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

#import "VideoProgressBarView.h"

@interface VideoProgressBarView()

@property (strong, nonatomic) UIImageView *sliderView;
@property (strong, nonatomic) UIView *progressBarView;
@property (strong, nonatomic) UIGestureRecognizer *sliderGesture;
@property (strong, nonatomic) UIGestureRecognizer *progressBarGesture;
@property (assign, nonatomic) CGFloat percentStep;
@property (strong, nonatomic) CALayer *layerPath;
@property (strong, nonatomic) CALayer *bufferingProgressBarLayer;
@property (assign, nonatomic) CGFloat lastBufferPercentValue;
@property (assign, nonatomic) CGFloat lastSliderPercentValue;

@end

@implementation VideoProgressBarView

#pragma mark - LifeCycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self preparation];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self preparation];
    }
    return self;
}

#pragma mark - Public

- (void)setNeedsToDisplay
{
    [self reloadProgressBarView];
    [self reloadSliderView];
    [self reloadLayerPathForProgressBar];
}

- (void)setBufferingProgresBar:(CGFloat)percent
{
    self.bufferingProgressBarLayer.frame = CGRectMake(0, 0, percent * self.percentStep, CGRectGetHeight(self.bufferingProgressBarLayer.frame));
    self.lastBufferPercentValue = percent;
}

- (void)setSliderPosition:(CGFloat)percent
{
    CGPoint point = CGPointMake(percent * self.percentStep, CGRectGetMinY(self.sliderView.frame));
    self.lastSliderPercentValue = percent;
    [self moveSliderViewTo:point];
}

#pragma mark - Gestures

- (void)panSliderView:(UIGestureRecognizer*)gestureRecognizer
{
    CGPoint translation = [gestureRecognizer locationInView:self.progressBarView];
    translation = CGPointMake(translation.x, translation.y);
    [self moveSliderViewTo:translation];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeProgressTo:)]) {
        CGFloat progressViewLength = translation.x / CGRectGetWidth(self.bounds);
        [self.delegate didChangeProgressTo:progressViewLength];
    }
}

- (void)tapProgressBar:(UIGestureRecognizer*)gestureRecognizer
{
    CGPoint gesturePoint = [gestureRecognizer locationInView:self];
    self.sliderView.center = CGPointMake(gesturePoint.x, CGRectGetMidY(self.sliderView.frame));
    self.lastSliderPercentValue = gesturePoint.x / CGRectGetWidth(self.progressBarView.frame);
    [self setLayerPathWidth:gesturePoint.x];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeProgressTo:)]) {
        [self.delegate didChangeProgressTo:self.lastSliderPercentValue];
    }
}

#pragma mark - CustomAccessors

- (void)setProgressBarBackgroundColor:(UIColor *)progressBarBackgroundColor
{
    _progressBarBackgroundColor = progressBarBackgroundColor;
    self.progressBarView.backgroundColor = self.progressBarBackgroundColor;
}

- (void)setProgressBarBufferingColor:(UIColor *)progressBarBufferingColor
{
    _progressBarBufferingColor = progressBarBufferingColor;
    self.bufferingProgressBarLayer.backgroundColor = self.progressBarBufferingColor.CGColor;
}

- (void)setProgressBarPlayedColor:(UIColor *)progressBarPlayedColor
{
    _progressBarPlayedColor = progressBarPlayedColor;
    self.layerPath.backgroundColor = self.progressBarPlayedColor.CGColor;
}

- (void)setProgressBarPointerImageName:(NSString *)progressBarPointerImageName
{
    _progressBarPointerImageName = progressBarPointerImageName;
    self.sliderView.image = [UIImage imageNamed:self.progressBarPointerImageName];
}

- (void)setIsPanEnabled:(BOOL)isPanEnabled
{
    _isPanEnabled = isPanEnabled;
    self.progressBarGesture.enabled = isPanEnabled;
}

- (void)setIsTapEnabled:(BOOL)isTapEnabled
{
    _isTapEnabled = isTapEnabled;
    self.sliderGesture.enabled = isTapEnabled;
}

#pragma mark - Private

- (void)preparation
{
    [self initProgressBarView];
    [self addTapGestureForProgressBar];
    [self addPanGestureForSliderView];
    [self initLayerPathForProgressBar];
    [self getStepValue];
    [self initSliderView];
}

- (void)initProgressBarView
{
    self.progressBarView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetHeight(self.frame), 0, CGRectGetWidth(self.frame) - CGRectGetHeight(self.frame) / 2, CGRectGetHeight(self.frame) * 0.4)];
    UIColor *backgroundColor = [UIColor grayColor];
    if (self.progressBarBackgroundColor) {
        backgroundColor = self.progressBarBackgroundColor;
    }
    self.progressBarView.backgroundColor = self.progressBarBackgroundColor;
    self.progressBarView.center = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    [self updateProgressBarMainLayer];
    [self addBufferingSubLayerToProgressBar];
    [self addSubview:self.progressBarView];
}

- (void)reloadProgressBarView
{
    self.progressBarView.frame = CGRectMake(CGRectGetHeight(self.frame), 0, CGRectGetWidth(self.frame) - CGRectGetHeight(self.frame) / 2, CGRectGetHeight(self.frame) * 0.4);
    self.progressBarView.center = CGPointMake(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    [self updateProgressBarMainLayer];
    [self getStepValue];
    [self updateBufferingSubLayer];
}

- (void)updateProgressBarMainLayer
{
    self.progressBarView.layer.cornerRadius = CGRectGetHeight(self.progressBarView.frame) / 2;
}

- (void)updateBufferingSubLayer
{
    self.bufferingProgressBarLayer.frame = CGRectMake(0, 0, self.lastBufferPercentValue * self.percentStep, CGRectGetHeight(self.progressBarView.frame));
    self.bufferingProgressBarLayer.cornerRadius = CGRectGetHeight(self.progressBarView.frame) / 2;
}

- (void)addBufferingSubLayerToProgressBar
{
    self.bufferingProgressBarLayer = [CALayer layer];
    self.bufferingProgressBarLayer.frame = CGRectMake(0, 0, 0, CGRectGetHeight(self.progressBarView.frame));
    
    UIColor *bufferingColor = [UIColor blackColor];
    if (self.progressBarBufferingColor) {
        bufferingColor = self.progressBarBufferingColor;
    }
    self.progressBarView.backgroundColor = self.progressBarBufferingColor;

    self.bufferingProgressBarLayer.backgroundColor = bufferingColor.CGColor;
    self.bufferingProgressBarLayer.cornerRadius = CGRectGetHeight(self.progressBarView.frame) / 2;
    [self.progressBarView.layer addSublayer:self.bufferingProgressBarLayer];
}

- (void)initSliderView
{
    self.sliderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetHeight(self.frame), CGRectGetHeight(self.frame))];
    self.sliderView.center = CGPointMake(CGRectGetHeight(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    self.sliderView.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.sliderView];
    [self.sliderView bringSubviewToFront:self];
}

- (void)reloadSliderView
{
    self.sliderView.frame = CGRectMake(0, 0, CGRectGetHeight(self.frame), CGRectGetHeight(self.frame));
    self.sliderView.center = CGPointMake((self.lastSliderPercentValue * self.percentStep) + CGRectGetHeight(self.frame) / 2, CGRectGetHeight(self.frame) / 2);
    self.sliderView.layer.cornerRadius = CGRectGetHeight(self.sliderView.frame) / 2;
}

- (void)addTapGestureForProgressBar
{
    self.userInteractionEnabled = YES;
    self.progressBarGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapProgressBar:)];
    [self addGestureRecognizer:self.progressBarGesture];
}

- (void)addPanGestureForSliderView
{
    self.sliderView.userInteractionEnabled = YES;
    self.sliderGesture  = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panSliderView:)];
    [self addGestureRecognizer:self.sliderGesture];
}

- (void)initLayerPathForProgressBar
{
    self.layerPath = [CALayer layer];
    self.layerPath.frame = CGRectMake(0, 0, 0, CGRectGetHeight(self.progressBarView.frame));
    self.layerPath.cornerRadius = CGRectGetHeight(self.progressBarView.frame) / 2;
    
    UIColor *playedColor = [UIColor blackColor];
    if (self.progressBarPlayedColor) {
        playedColor = self.progressBarPlayedColor;
    }
    
    self.layerPath.backgroundColor = self.progressBarPlayedColor.CGColor;
    [self.progressBarView.layer addSublayer:self.layerPath];
}

- (void)reloadLayerPathForProgressBar
{
    self.layerPath.frame = CGRectMake(0, 0, self.percentStep * self.lastSliderPercentValue + CGRectGetHeight(self.frame) / 4, CGRectGetHeight(self.progressBarView.frame));
    self.layerPath.cornerRadius = CGRectGetHeight(self.progressBarView.frame) / 2;
}

- (void)moveSliderViewTo:(CGPoint)location
{
    if (location.y > CGRectGetMaxY(self.sliderView.frame) || location.y < 0) {
        return;
    }
    if(location.x > (CGRectGetMinX(self.progressBarView.frame))  && location.x < (CGRectGetWidth(self.progressBarView.frame) - CGRectGetHeight(self.frame) / 4)) {
        CGFloat newPosition = location.x + CGRectGetHeight(self.frame) / 4;
        self.sliderView.center = CGPointMake(newPosition, CGRectGetMidY(self.sliderView.frame));
        self.lastSliderPercentValue = (location.x -  CGRectGetHeight(self.frame) / 4) / CGRectGetWidth(self.progressBarView.frame);
        [self setLayerPathWidth:location.x];
        [self moveBufferingLayer:newPosition];
    }
}

- (void)moveBufferingLayer:(CGFloat)newPosition
{
    if (newPosition >= CGRectGetMaxX(self.bufferingProgressBarLayer.frame)) {
        self.bufferingProgressBarLayer.frame = CGRectMake(0, 0, newPosition, CGRectGetHeight(self.bufferingProgressBarLayer.frame));
        self.lastBufferPercentValue = newPosition / CGRectGetWidth(self.progressBarView.frame);
    }
}

- (void)setLayerPathWidth:(CGFloat)width
{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
     self.layerPath.frame = CGRectMake(0, 0, width, CGRectGetHeight(self.layerPath.frame));
    [CATransaction commit];
}

- (void)getStepValue
{
    self.percentStep = CGRectGetWidth(self.progressBarView.frame);
}

@end
