//
//  ProgressBarView.h
//  PlayerMobileViewController
//
//  Created by Misha Gajdan on 29.06.15.
//  Copyright (c) 2015 Thinkmobiles. All rights reserved.
//

@protocol VideoProgressBarViewDelegate <NSObject>

@optional
- (void)didChangeProgressTo:(CGFloat)progress;

@end

IB_DESIGNABLE
@interface VideoProgressBarView : UIView

@property (weak, nonatomic) id <VideoProgressBarViewDelegate> delegate;

@property (strong, nonatomic) IBInspectable UIColor *progressBarBackgroundColor;
@property (strong, nonatomic) IBInspectable UIColor *progressBarBufferingColor;
@property (strong, nonatomic) IBInspectable UIColor *progressBarPlayedColor;
@property (strong, nonatomic) IBInspectable NSString *progressBarPointerImageName;

@property (assign, nonatomic) IBInspectable BOOL isTapEnabled;
@property (assign, nonatomic) IBInspectable BOOL isPanEnabled;

- (void)setBufferingProgresBar:(CGFloat)percent;
- (void)setSliderPosition:(CGFloat)percent;

- (void)setNeedsToDisplay;

@end
