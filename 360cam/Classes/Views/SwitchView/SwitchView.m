//
//  customSwitchView.m
//  Custom Slider
//
//  Created by Norbert Citrak on 6/24/15.
//  Copyright (c) 2015 Norbert Citrak. All rights reserved.
//

#import "SwitchView.h"
#import "UIColor+CrossingColor.h"

static CGFloat const AnimationDuration = 0.25f;

@interface SwitchView ()

@property (strong, nonatomic) UILabel *switcherLabel;
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;

@property (assign, nonatomic) CGPoint startPoint;
@property (strong, nonatomic) UIColor *futureColor;
@property (strong, nonatomic) UIColor *currentColor;

@end

@implementation SwitchView

#pragma mark - Lifecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self createLabel];
    [self setup];
}

#pragma mark - Public

- (void)setNeedsToDisplay
{
    CGFloat labelWidth = CGRectGetWidth(self.bounds) * .5;
    CGRect frame = CGRectMake(0.f, 0.f, labelWidth, CGRectGetHeight(self.bounds));
    self.switcherLabel.frame = frame;
    
    if (self.switchState) {
        [self moveSwitcher:self.switcherLabel toX:labelWidth / 2 animated:NO];
    } else {
        [self moveSwitcher:self.switcherLabel toX:labelWidth * 1.5f animated:NO];
    }
}

#pragma mark - Private

- (void)createLabel
{
    CGRect frame = CGRectMake(0.f, 0.f, CGRectGetWidth(self.bounds) * .5, CGRectGetHeight(self.bounds));
    self.switcherLabel = [[UILabel alloc] initWithFrame:frame];
    if (self.activeStateTitle.length) {
        self.switcherLabel.text = self.inactiveStateTitle;
    }
    self.switcherLabel.textAlignment = NSTextAlignmentCenter;
    self.switcherLabel.userInteractionEnabled = YES;
    self.switcherLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.switcherLabel];
}

- (void)setup
{
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchOnTap:)];
    [self.switcherLabel addGestureRecognizer:self.panGesture];
    [self addGestureRecognizer:self.tapGesture];
    
    self.switchState = NO;
    
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = self.borderColor.CGColor;
    
    self.switcherLabel.layer.backgroundColor = self.switcherColor.CGColor;
    self.switcherLabel.layer.borderWidth = 1.f;
    self.switcherLabel.layer.borderColor = self.borderColor.CGColor;
    
    self.backgroundColor = [self colorForCurrentState];
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    self.switcherLabel.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
}

- (void)moveSwitcher:(UIView *)view toX:(CGFloat)positionX animated:(BOOL)animated
{
    CGFloat animationDuration = AnimationDuration;
    
    CGFloat originalX = view.layer.position.x;
    view.layer.position = CGPointMake(positionX, view.layer.position.y);
    
    UIColor *backgroundColor = self.backgroundColor;
    self.backgroundColor = [self colorForCurrentState];
    
    if (!animated) {
        return;
    } else {
        CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        colorAnimation.duration = animationDuration;
        colorAnimation.fillMode = kCAFillModeForwards;
        colorAnimation.fromValue = backgroundColor;
        colorAnimation.toValue = self.backgroundColor;
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position.x"];
        animation.fromValue = @(originalX);
        animation.duration = animationDuration;
        
        [view.layer addAnimation:animation forKey:@"position"];
        [self.layer addAnimation:colorAnimation forKey:@"backgroundColor"];
    }
}

- (UIColor *)colorForCurrentState
{
    UIColor *switchColor;
    
    if ([self isOn]) {
        switchColor = self.onStateColor;
    } else {
        switchColor = self.offStateColor;
    }
    return switchColor;
}

- (void)setSwitcherText:(UILabel *)switcher
{
    BOOL state = [self isOn];
    if (self.activeStateTitle.length && self.inactiveStateTitle.length) {
        if (state) {
            switcher.text = self.activeStateTitle;
        } else {
            switcher.text = self.inactiveStateTitle;
        }
    }
}

- (BOOL)isOn
{
    return _switchState;
}

- (void)setOn:(BOOL)on animated:(BOOL)animated;
{
    CGFloat switcherWidth = CGRectGetWidth(self.switcherLabel.bounds);
    self.switchState = on;
    
    if (on) {
        [self moveSwitcher:self.switcherLabel toX:switcherWidth / 2 animated:animated];
    } else {
        [self moveSwitcher:self.switcherLabel toX:switcherWidth * 1.5f animated:animated];
    }
    
    [self setSwitcherText:self.switcherLabel];
}

- (void)switchOnTap:(UITapGestureRecognizer *)recognizer
{
    CGFloat switcherWidth = CGRectGetWidth(self.switcherLabel.bounds);
    
    if (self.switcherLabel.center.x <= switcherWidth) {
        self.switchState = NO;
        [self moveSwitcher:self.switcherLabel toX:switcherWidth * 1.5f animated:YES];
    } else {
        self.switchState = YES;
        [self moveSwitcher:self.switcherLabel toX:switcherWidth / 2 animated:YES];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(switchViewDidChangeState:)]) {
        [self.delegate switchViewDidChangeState:self.switchState];
    }
    
    [self setSwitcherText:self.switcherLabel];
}

#pragma mark - Gesture

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    UIView *switcherView = recognizer.view;
    CGPoint translation = [recognizer translationInView:switcherView];
    CGFloat switcherWidth = CGRectGetWidth(switcherView.bounds);
    
    if (switcherView.center.x + translation.x < switcherWidth / 2 || switcherView.center.x + translation.x > switcherWidth * 1.5f) {
        recognizer.view.center = CGPointMake(switcherView.center.x, switcherView.center.y);
    } else {
        recognizer.view.center = CGPointMake(switcherView.center.x + translation.x, switcherView.center.y);
    }
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            self.startPoint = [recognizer locationInView:self];
            if (![self isOn]) {
                self.futureColor = self.onStateColor;
                self.currentColor = self.offStateColor;
            } else {
                self.futureColor = self.offStateColor;
                self.currentColor = self.onStateColor;
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{
            CGPoint location = [recognizer locationInView:self];
            
            if (!CGRectContainsPoint(self.bounds, location)) {
                break;
            }
            
            CGFloat delta = (self.startPoint.x - location.x);
            CGFloat maxDelta = CGRectGetWidth(self.bounds) / 2;
            CGFloat progress = ABS(delta / maxDelta);
            
            if (progress > 1) {
                progress = 1;
            }
            
            UIColor *progressiveColor = [self.currentColor crossingToColor:self.futureColor percent:(progress * 100)];
            self.backgroundColor = progressiveColor;
            break;
        }
        case UIGestureRecognizerStateEnded: {
            if (recognizer.view.center.x <= switcherWidth) {
                self.switchState = YES;
                [self moveSwitcher:switcherView toX:switcherWidth / 2 animated:YES];
            } else {
                self.switchState = NO;
                [self moveSwitcher:switcherView toX:switcherWidth * 1.5f animated:YES];
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(switchViewDidChangeState:)]) {
                [self.delegate switchViewDidChangeState:self.switchState];
            }
            
            self.startPoint = CGPointZero;
            
            break;
        }
        default:
            break;
    }
    
    if (recognizer.view.center.x <= switcherWidth) {
        self.switchState = YES;
    } else {
        self.switchState = NO;
    }
    
    [self setSwitcherText:(UILabel *)switcherView];
    [recognizer setTranslation:CGPointZero inView:switcherView];
}

@end