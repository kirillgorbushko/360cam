//
//  customSwitchView.h
//  Custom Slider
//
//  Created by Norbert Citrak on 6/24/15.
//  Copyright (c) 2015 Norbert Citrak. All rights reserved.
//

@protocol SwitchViewDelegate <NSObject>

@optional
- (void)switchViewDidChangeState:(BOOL)switchState;

@end

IB_DESIGNABLE
@interface SwitchView : UIView

@property (strong, nonatomic) IBInspectable UIColor *borderColor;
@property (strong, nonatomic) IBInspectable UIColor *onStateColor;
@property (strong, nonatomic) IBInspectable UIColor *offStateColor;
@property (strong, nonatomic) IBInspectable UIColor *switcherColor;
@property (copy, nonatomic) IBInspectable NSString *activeStateTitle;
@property (copy, nonatomic) IBInspectable NSString *inactiveStateTitle;

@property (weak, nonatomic) id <SwitchViewDelegate> delegate;

@property(assign, nonatomic) BOOL switchState;

- (void)setOn:(BOOL)on animated:(BOOL)animated;
- (void)setNeedsToDisplay;

@end
