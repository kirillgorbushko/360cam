//
//  TouchView.h
//  360cam
//
//  Created by Kirill Gorbushko on 01.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

@interface TouchView : UIView

@property (strong, nonatomic) UIView *targetView;
@property (assign, nonatomic) CGRect activeRect;
@property (assign, nonatomic) __block BOOL blockAllTouch;
@property (assign, nonatomic) __block BOOL ignoreAllTouch;

@end
