//
//  TouchView.m
//  360cam
//
//  Created by Kirill Gorbushko on 01.07.15.
//  Copyright © 2015 Kirill Gorbushko. All rights reserved.
//

#import "TouchView.h"

@implementation TouchView

#pragma mark - Ovverride

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL hideTouch = YES;
    if (CGRectContainsPoint(self.activeRect, point)) {
        hideTouch = NO;
    }
    if (self.blockAllTouch) {
        hideTouch = YES;
    }
    if (self.ignoreAllTouch) {
        hideTouch = NO;
    }
    return hideTouch;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint pointForTargetView = [self.targetView convertPoint:point fromView:self];
    if (CGRectContainsPoint(self.targetView.bounds, pointForTargetView)) {
        return [self.targetView hitTest:pointForTargetView withEvent:event];
    }
    return [super hitTest:point withEvent:event];
}


@end
