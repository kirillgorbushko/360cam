//
//  BadgeLabel.h
//  BadgeLabel
//
//  Created by Victor on 10.07.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//

@interface BadgeLabel : UILabel

@property (strong, nonatomic) UIColor *badgeBackgroundColor;
@property (strong, nonatomic) UIColor *badgeBorderColor;
@property (strong, nonatomic) UIColor *badgeTextColor;

@property (assign, nonatomic) CGFloat badgeBorderWidth;
@property (assign ,readonly, nonatomic) NSUInteger badgeNumber;

- (void)addBadgeLabelForView:(UIView *)parentView;
- (void)addBadgeLabelForView:(UIView *)parentView withNumber:(NSUInteger)number;
- (void)setBadgeIconNumber:(NSUInteger)iconNumber;

@end
