//
//  BadgeLabel.m
//  BadgeLabel
//
//  Created by Victor on 10.07.15.
//  Copyright (c) 2015 thinkmobiles. All rights reserved.
//

#import "BadgeLabel.h"

static CGFloat const MaxTextHeight  = 14.f;

@interface BadgeLabel()

@property (strong, nonatomic) UIView *parentView;

@end

@implementation BadgeLabel

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setDefaultParameters];
    }
    return self;
}

#pragma mark - Public

- (void)addBadgeLabelForView:(UIView *)parentView
{
    _badgeNumber = 0;
    self.text = @"0";
    self.parentView = parentView;
    [self configureView];
}

- (void)addBadgeLabelForView:(UIView *)parentView withNumber:(NSUInteger)number
{
    self.parentView = parentView;
    _badgeNumber = number;

    self.text = [NSString stringWithFormat:@"%i", (int)number];
    [self configureView];
}

- (void)setBadgeIconNumber:(NSUInteger)iconNumber
{
    _badgeNumber = iconNumber;
    self.text = [NSString stringWithFormat:@"%i", (int)iconNumber];
    [self prepareBadgeFrame];
    [self hideBadgeIfNeeded];
}

#pragma mark - CustomAccessors

- (void)setBadgeBackgroundColor:(UIColor *)badgeBackgroundColor
{
    _badgeBackgroundColor = badgeBackgroundColor;
    self.backgroundColor = self.badgeBackgroundColor;
}

- (void)setBadgeBorderColor:(UIColor *)badgeBorderColor
{
    _badgeBorderColor = badgeBorderColor;
    self.layer.borderColor = self.badgeBorderColor.CGColor;
}

- (void)setBadgeTextColor:(UIColor *)badgeTextColor
{
    _badgeTextColor = badgeTextColor;
    self.textColor = self.badgeTextColor;
}

- (void)setBadgeBorderWidth:(CGFloat)badgeBorderWidth
{
    _badgeBorderWidth = badgeBorderWidth;
    self.layer.borderWidth = self.badgeBorderWidth;
}

#pragma mark - Private

- (void)configureView
{
    [self prepareBadgeFrame];
    [self.parentView addSubview:self];
    [self hideBadgeIfNeeded];
}

- (void)prepareBadgeFrame
{
    CGSize sizeForLabel = [self.text  sizeWithAttributes:@{NSFontAttributeName : self.font}];
    CGFloat maxSizeValue = MAX(sizeForLabel.width, sizeForLabel.height);
    CGRect frameForBadge = CGRectMake(0, 0, maxSizeValue + 2, maxSizeValue > MaxTextHeight ? MaxTextHeight : maxSizeValue + 2);
    self.frame = frameForBadge;
    self.layer.cornerRadius = self.frame.size.height / 2;
    CGPoint centerPoint = CGPointMake(CGRectGetMaxX(self.parentView.bounds), self.bounds.size.height / 2);
    self.center = centerPoint;
}

- (void)hideBadgeIfNeeded
{
    self.hidden = self.text.integerValue == 0;
}

- (void)setDefaultParameters
{
    self.textAlignment = NSTextAlignmentCenter;
    self.layer.masksToBounds = YES;
    self.font = [UIFont camFontZonaProRegularWithSize:12.f];
    self.backgroundColor = [UIColor redColor];
    self.textColor = [UIColor whiteColor];
}

@end
